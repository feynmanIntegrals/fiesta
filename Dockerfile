# syntax=docker/dockerfile:1

# Build: docker build -t fiesta-image .
# Run: docker run -d -p 3142:3142 --name fiesta-image-run apt-cacher
#
# and then you can run containers with:
#   docker run -t -i --rm -e http_proxy http://dockerhost:3142/ debian bash
FROM   ubuntu

# Setup timezone info
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y \
  g++ cmake zlib1g-dev \
  libmpfr-dev gfortran git mpich doxygen libgsl0-dev cppcheck \
  valgrind clang clang-tools \
  nvidia-cuda-toolkit nvidia-cuda-dev
