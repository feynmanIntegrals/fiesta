(*
    Copyright (C) Alexander Smirnov.
    The program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    The program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*)

If[Not[TrueQ[$VersionNumber>=8.0]],
    Print["This program only supports Mathematica 8 or higher"];
    Abort[];
];

If[Options[FIESTA] === {},
    Options[FIESTA] = {
        "NumberOfSubkernels" -> 1,
        "NumberOfLinks" -> 1,
        "Strategy" -> "S2",
        "SectorSymmetries" -> True,
        "ComplexMode" -> False,
        "d0" -> 4,
        "UsingC" -> True,
        "RegVar" -> None,
        "ExpandVar" -> t,
        "XVar" -> x,
        "EpVar" -> ep,
        "PMVar" -> pm,
        "Graph" -> None,
        "NegativeTermsHandling" -> Automatic,
        "PrimarySectorCoefficients" -> Automatic,
        "OnlyPrepare" -> False,
        "FixSectors" -> False,
        "MixSectors" -> 0,
        "SectorSplitting" -> False,
        "MinimizeContourTransformation" -> True,
        "ContourShiftShape" -> 1,
        "ContourShiftCoefficient" -> 1,
        "ContourShiftIgnoreFail" -> False,
        "FixedContourShift" -> 0,
        "LambdaIterations" -> 1000,
        "LambdaSplit" -> 4,
        "ChunkSize" -> 1,
        "OptimizeIntegrationStrings" -> False,
        "AnalyzeWorstPower" -> False,
        "ZeroCheckCount" -> 0,
        "ExpandResult" -> True,
        "FIESTAPath" -> DirectoryName[$InputFileName],
        "DataPath" -> Automatic,
        "BucketSize" -> 27,
        "NoDatabaseLock" -> False,
        "RemoveDatabases" -> True,
        "SeparateTerms" -> False,
        "BalanceSamplingPoints" -> False,
        "BalanceMode" -> Automatic,
        "BalancePower" -> Automatic,
        "ResolutionMode" -> "Taylor",
        "AnalyticIntegration" -> False,
        "OnlyPrepareRegions" -> False,
        "AsyLP" -> True,
        "PolesMultiplicity" -> False,
        "ExactIntegrationOrder" -> -Infinity,
        "ExactIntegrationTimeout" -> 10,
        "GPUIntegration" -> False,
        "NoAVX" -> False,
        "Precision" -> 6,
        "ReturnErrorWithBrackets" -> False,
        "Integrator" -> "vegasCuba",
        "IntegratorOptions" -> Automatic,
        "CIntegratePath" -> Automatic,
        "MPSmallX" -> Automatic,
        "MPThreshold" -> Automatic,
        "MPMin" -> Automatic,
        "MPPrecisionShift" -> Automatic,
        "MathematicaBinary" -> None,
        "QHullPath" -> "qhull",
        "DebugParallel" -> False,
        "DebugMemory" -> False,
        "DebugAllEntries" -> False,
        "DebugSector" -> 0
    };
];

FIESTA::usage = "
    FIESTA package offers the following functions:
    UF, SDEvaluate, SDExpand, SDExpandAsy, SDEvaluateG, SDExpandG, SDEvaluateDirect, SDExpandDirect, SDAnalyze, SDIntegrate, GenerateAnswer.
    Read information on each of them with ?name.

    The set of option for those functions is common (however some options ar applicable only for some of the function)
    and is set with Options[FIESTA] while FIESTA itself is not a name of a function.

    The options are explained below:
    NumberOfSubkernels: the number of parallel kernels used by the algebraic preparation stage, can be set to 0 to run in main kernel;
    NumberOfLinks: number of CIntegrateMP used at integration stage;
    Strategy: sector decomposition strategy that can be \"0\", \"A\", \"B\", \"S\", \"SS\", \"X\", \"KU0\", \"KU\", \"KUS\", \"KU2\";
    SectorSymmetries: indication whether to search symmetries between sectors,
    ComplexMode: whether we will need contour decomposition and complex numbers at integration stage;
    d0: the basic dimension, ep = (d0-d)/2;
    UsingC: whether the integration is performed with c++ and not Mathematica;
    RegVar: extra regularization variable if not None;
    ExpandVar: expansion variable that should be set for all Expand functions;
    XVar: the variables used for coordinates in alpha-representation;
    EpVar: the main expansion variable;
    PMVar: the variable for error estimate;
    Graph: the graph for Speer strategy, normally should not be set manually;
    NegativeTermsHandling: mode for dealing with negative terms, can be \"AdvancedSquares\", \"AdvancedSquares2\", \"AdvancedSquares3\", \"Squares\", False, Automatic;
    PrimarySectorCoefficients: if set can manually set weights to primary sectors;
    OnlyPrepare: perform no integration, just prepare a database, usefull if you need to change integration options later;
    FixSectors: turns off parallelization at variable substitution stage making a fixed numbering of sectors, usefull for debugging;
    MixSectors: the number of sectors joined into one expression;
    SectorSplitting: analyze x[i]->1 behaviour and try to split sectors and perform additional sector decomposition. set to True in case of bad convergence;
    MinimizeContourTransformation: use contour transformation only for those vars that seem to change sign of F. Set it to False in case of divergencies;
    ContourShiftShape: x*(1-x)*D[f,x] is estimated, then the coefficient is set to inverse of the maximum, then multiplied by ContourShiftShape, then lambda search is performed;
    ContourShiftCoefficient: final coefficient of all shifts after search is performed;
    FixedContourShift: if set to non-zero uses exactly that lambda in contour transformation formula, ignoring ContourShiftShape and detection;
    LambdaIterations: number of samples at all contour decomposition stages;
    LambdaSplit: number of parts of lambda at final search search for contour transformation;
    ChunkSize: for parallelization, number of terms passed in a chunk to a subkernel. Consider increasing it but so that ChunkSize*NumberOfSubkernels is a lot smaller that the number of terms;
    OptimizeIntegrationStrings: make Mathematica optimaze the generation of integration strings for c++. Might be slow, but should improve integration performance,
        if set to True, now be sure to have AnalyzeWorstPower equal to False and to use the SeparateTerms integration option;
    AnalyzeWorstPower: search for worst monom for better MPFR detection, old default was true, but I know no single example where it is needed;
    ZeroCheckCount: check whether the function is non-zero before sending it for integration, old default was 10, if 0, no check is performed, otherwise more is longer and safer;
    ExpandResult: whether to expand the final result by all small variables;
    FIESTAPath: path to the FIESTA5.m file, now detected automatically;
    DataPath: path to database files, if Automatic, then it is inside the FIESTA/temp folder with a process id; Default simply temp/db coinciding for different jobs; consider setting it to a local disk;
    BucketSize: database tuning option, normally should be left unless one gets 2^25 or more terms;
    NoDatabaseLock: should be set to True in case of problems with disk system on database open;
    RemoveDatabases: whether to remove databases after the result is produced;
    SeparateTerms: whether to integrate each term inside a sector separately, passed to CIntegratePool
    ResolutionMode: one of \"Taylor\", \"IBP0\", \"IBP1\", refer the papers for details;
    AnalyticIntegration: option for SDExpandAsy whether to try to partially integrate analytically;
    OnlyPrepareRegions: option for SDExpandAsy to only prepare initial regions;
    AsyLP: option for SDExpandAsy whether to use Lee-Pomeransky representation for faster region search;
    PolesMultiplicity: option for SDAnalyze whether to return multiplicity of located poles;
    ExactIntegrationOrder: maximal order for Mathematica to try analytic integration in case UsingC is False;
    ExactIntegrationTimeout: maximal time to spend on a term trying analytic integration in case UsingC is False;
    GPUIntegration: whether to request integration on a GPU;
    Precision: the number of digits in Mathematica results;
    ReturnErrorWithBrackets: whether to return error estimate variable as pm[_];
    Integrator: integrator passed to CIntegratePool;
    IntegratorOptions: integrator options passed to CIntegratePool;
    CIntegratePath: if set, provides path to CIntegrate manually,
    MPSmallX: tuning of MPFR, evaluation should be possible at {MPSmallX,...,MPSmallX};
    MPThreshold: tuning of MPFR, going to high precision if the Monom is less than MPThreshhold;
    MPMin: tuning of MPFR, see the papers for details;
    MPPrecisionShift: tuning of MPFR, additional bits in multiprecision;
    MathematicaBinary: None by default; if set, path to the Mathematica binary for CIntegrate to evaluate Polygammas and such; if set to Automatic, uses value from ./configure;
    QHullPath: path to the qhull binary;
    DebugParallel: print extra information related to parallelization and memory;
    DebugMemory -> False,
    DebugAllEntries: whether to print all entries at algebraic stages;
    DebugSector: can be set to a particular sector number to work only with one sector.
";

Off[General::compat];
BeginPackage["FIESTA`", {"Combinatorica`", "Asy`"}];
On[General::compat];

SDEvaluate::usage = "SDEvaluate[{U, F, loops}, indices, order, options] evaluates a Feynman integral;
    U and F are two function of x (or other variable in case of options);
    loops is the number of loops;
    indices is the set of indices;
    order is the requested expansion order by ep (or other variable in case of options);
    {U, F, loops} can be produced by the UF function";
SDEvaluateG::usage = "SDEvaluateG[{graph, external}, {U, F, loops}, indices, order, options] is the same with SDEvaluate,
    but {graph, external} is the most convenient syntax to provede a graph for the Speer strategy;
    here graph is the number of pairs of vertices (numbers) and external is the list of external vertices;
    the order of lines should coincide with the order of indices";
SDExpand::usage = "SDExpand[{U, F, loops}, indices, order, expand_degree, options] expands a Feynman integral;
    The syntax is the same with SDEvaluate, but there is also a parameter with the order of the expansion variable
    Default expansion variable is t, but this can be changed by options.
    SDExpand can work only in case F depends linearly on t";
SDExpandG::usage = "SDExpandG[{graph, external}, {U, F, loops}, indices, order, expand_degree, options] is a mixture of SDExpand and SDEvaluateG
    it expands an integral with the help of Speer sectors.";
SDExpandAsy::usage = "SDExpandAsy[{U, F, loops}, indices, order, expand_degree, options] expands the integral with the regions approach;
    The syntax completely coincides with the syntax of SDExpand";
SDEvaluateDirect::usage = "SDEvaluateDirect[functions, degrees, order, deltas(optional), options] evaluates a non-loop integral;
    degrees is the list of function degrees;
    functions is the list of functions, length should coincide with degrees;
    order is the expansion order;
    deltas, if provided, is the list of attached delta-functions;"
SDExpandDirect::usage = "SDExpandDirect[functions, degrees, order, expand_degree, deltas(optional), options] expands a non-loop integral;
    the syntax is the same as in SDEvaluateDirect, but the expansion variable degree is added as an argument;";
SDIntegrate::usage = "SDIntegrate[options] - integrate using prepared database (for example, after a run with OnlyPrepare->True";
SDAnalyze::usage = "SDAnalyze[{U, F, h}, degrees, dmin, dmax, options] searches for d values resulting in possible ep-poles
    U and F are two function of x (or other variable in case of options);
    h is the number of loops;
    degrees is the set of indices;
    dmin and dmax define the d search range";
GenerateAnswer::usage = "GenerateAnswer[options] - generate answer with the use of results from the database (see examples.nb)";
UF::usage="UF[LoopMomenta,Propagators,subst] generates the functions U and F by the loop momenta, list of propagators and list of replacements";

Begin["`Private`"];

FIESTA = Global`FIESTA;
Get["mm/MBexpGam.m"];

If[Not[ValueQ[PMCounter]],
    (* that is a counter for error estimate variables *)
    PMCounter=1;
];

MBVar; (* used for introducing an MB-integration in SDExpand *)

ImaginaryShiftVariable; (* introduced in complex mode in sectors with a negative free term, so there is a chance of appearing of Log[-1]; replaced to 0 before integration *)

RemoveFIESTAName::usage = "RemoveFIESTAName[xx] removes the package name from the strings before saving to database";
RemoveFIESTAName[xx_]:=StringReplace[xx,{"FIESTA`"->"","Private`"->""}];

ActualDataPath::usage = "ActualDataPath[options] provides a real data path depending on the options (automatic or manual)";
ActualDataPath[options:OptionsPattern[FIESTA]] := Module[{temp},
    If[OptionValue["DataPath"] === Automatic,
        Return[OptionValue["FIESTAPath"] <> "/temp/db" <> ToString[$ProcessID, InputForm]];
    ];
    If[OptionValue["DataPath"] === Default,
        Return[OptionValue["FIESTAPath"] <> "/temp/db"];
    ];
    Return[OptionValue["DataPath"]];
];

DefinedFor::usage = "DefinedFor[x] is the list of keys x is defined for";
DefinedFor[x_] := (ReleaseHold[Apply[List, ##[[1]], 1]]) & /@ DownValues[x]

QPutWrapper::usage = "QPutWrapper[TaskPrefix, dbase, key, none] gets an entry from dbase with prefix and key";
QPutWrapper[TaskPrefix_, dbase_, key_, value_]:=QPut[RemoveFIESTAName[dbase<>"/"],TaskPrefix<>"-"<>key,value]

QGetWrapper::usage = "QGetWrapper[TaskPrefix, dbase, key] stored an entry to dbase with prefix and key";
QGetWrapper[TaskPrefix_, dbase_, key_]:=QGet[RemoveFIESTAName[dbase<>"/"],TaskPrefix<>"-"<>key]

QSafeGetWrapper::usage = "QSafeGetWrapper[TaskPrefix, dbase, key, none] checks whether dbase has an entry with prefix and key, if it is there returns it, none otherwise";
QSafeGetWrapper[TaskPrefix_, dbase_, key_, none_]:=Module[{temp},
    temp=QCheck[RemoveFIESTAName[dbase<>"/"],TaskPrefix<>"-"<>key];
    If[temp,QGet[RemoveFIESTAName[dbase<>"/"],TaskPrefix<>"-"<>key],none]
]

MyWorstPower::usage = "MyWorstPower[x, z, options] finds the largest negative power of expression x by each of the variables in list z";
MyWorstPower[xx_, z_, options:OptionsPattern[FIESTA]] := Module[{temp},
    If[Head[xx] === Power,
        Return[If[Head[xx[[1]]] === OptionValue["XVar"], Min[##, 0] & /@ Exponent[xx, z], Abs[xx[[2]]]*MyWorstPower[xx[[1]], z, options]]];
    ];
    If[Head[xx] === Plus,
        Return[Apply[Min, Transpose[MyWorstPower[##, z, options] & /@ List @@ xx], {1}]];
    ];
    If[Head[xx] === Times,
        Return[Apply[Plus, Transpose[MyWorstPower[##, z, options] & /@ List @@ xx], {1}]];
    ];
    Min[##, 0] & /@ Exponent[xx, z]
]

ZeroCheck::usage = "ZeroCheck[x, options] runs checks whether the argument is a complete zero and returns true if ZeroCheckCount checks confirmed it. In case ZeroCheckCount = 0 returns False";
ZeroCheck[x_,options:OptionsPattern[FIESTA]] := Module[{vars, i, temp},
    If[OptionValue["ZeroCheckCount"] === 0,
        Return[False];
    ];
    vars = Variables[x];
    For[i = 1, i <= OptionValue["ZeroCheckCount"], i++,
        temp = Expand[x /. (Rule[##, 1/RandomInteger[{2, 10}]] & /@ vars)];
        If[temp =!= 0, Return[False]];
    ];
    True
]

ToStringInputForm::usage = "ToStringInputForm[x] is ToString[x, InputForm]";
ToStringInputForm[x__]:=ToString[x,InputForm]

MyMemoryInUse::usage = "MyMemoryInUse[options] is a joint memory information from main kernel and subkernels if they are working";
MyMemoryInUse[options:OptionsPattern[FIESTA]]:=
    "Memory usage: {" <>
    ToString[N[MaxMemoryUsed[]/(1024^3),4]] <>
    " Gb, " <>
    If[OptionValue["NumberOfSubkernels"] ===0, "0", ToString[N[Plus@@(ParallelEvaluate[MaxMemoryUsed[]])/(1024^3),4]]] <>
    " Gb}";

CommandOptions::usage = "CommandOptions[options] generated option for running CIntegratePool";
CommandOptions[options:OptionsPattern[FIESTA]]:=Module[{temp="",i,file},
    If[OptionValue["CIntegratePath"] =!= Automatic,
        temp=temp<>" --cIntegratePath "<>OptionValue["CIntegratePath"];
    ];
    If[OptionValue["Integrator"] =!= Automatic,
        temp=temp<>" --Integrator "<>OptionValue["Integrator"];
    ];
    If[OptionValue["MPSmallX"] =!= Automatic,
        temp=temp<>" --MPSmallX "<>ToString[OptionValue["MPSmallX"],CForm];
    ];
    If[OptionValue["MPThreshold"] =!= Automatic,
        temp=temp<>" --MPThreshold "<>ToString[OptionValue["MPThreshold"],CForm];
    ];
    If[OptionValue["MPMin"] =!= Automatic,
        temp=temp<>" --MPMin "<>ToString[OptionValue["MPMin"],CForm];
    ];
    If[OptionValue["MPPrecisionShift"] =!= Automatic,
        temp=temp<>" --MPPrecisionShift "<>ToString[OptionValue["MPPrecisionShift"],CForm];
    ];
    If[OptionValue["IntegratorOptions"] =!= Automatic,
        For[i=1,i<=Length[OptionValue["IntegratorOptions"]],i++,
            temp=temp<>" --IntegratorOption "<>OptionValue["IntegratorOptions"][[i]][[1]]<>":"<>OptionValue["IntegratorOptions"][[i]][[2]];
        ]
    ];
    If[OptionValue["MathematicaBinary"] =!= None,
        If[OptionValue["MathematicaBinary"] === Automatic,
            file = ReadString[OptionValue["FIESTAPath"] <> "paths.inc"];
            file = StringSplit[file,"\n"][[1]];
            If[StringTake[file,5] =!= "MATH=",
                Print["No MATH specification in paths.inc at first line"];
                Abort[];
            ];
            file = StringDrop[file,5];
            If[file === "",
                Print["Empty MATH specification in paths.inc at first line"];
                Abort[];
            ];
            temp=temp<>" --Math "<>file;
        ,
            temp=temp<>" --Math "<>OptionValue["MathematicaBinary"];
        ];
    ];
    If[OptionValue["ComplexMode"],
        temp=temp<>" --complex";
    ];
    If[OptionValue["Precision"] =!= 6,
        temp=temp<>" --precision " <> ToString[OptionValue["Precision"], InputForm];
    ];
    If[OptionValue["GPUIntegration"],
        temp=temp<>" --gpu";
    ];
    If[OptionValue["NoAVX"],
        temp=temp<>" --NoAVX";
    ];
    If[OptionValue["SeparateTerms"],
        temp=temp<>" --separateTerms";
    ];
    If[OptionValue["BalanceSamplingPoints"],
        temp=temp<>" --balanceSamplingPoints";
    ];
    If[OptionValue["BalanceMode"] =!= Automatic,
        temp=temp<>" --balanceMode "<>OptionValue["BalanceMode"];
    ];
    If[OptionValue["BalancePower"] =!= Automatic,
        temp=temp<>" --balancePower "<>OptionValue["BalancePower"];
    ];
    temp
]

InitializeQLink::usage = "InitializeQLink[Databases, options] starts QLink and opens listed databases; QLink variable is set to prevent future initializations";
InitializeQLink[Databases_,options:OptionsPattern[FIESTA]]:=Module[{temp, compileVersion, nowVersion},
    If[Head[QLink]===Symbol,
        QLink=Install[OptionValue["FIESTAPath"]<>"/bin/KLink"];
        If[Not[Head[QLink]===LinkObject],
            Print["Could not start QLink"];
            Abort[];
        ];
        compileVersion = QBuildVersion[];
        nowVersion = TrueQ[$VersionNumber>=10.0];
        If[nowVersion =!= compileVersion,
            Print["Mathlib protocol version does not match the one at build time."];
            If[nowVersion,
                Print["Current version is 4 (Mathematica version 10+)"];
            ,
                Print["Current version is 3 (Mathematica version 8-9)"];
            ];
            If[compileVersion,
                Print["Compile version is 4 (Mathematica version 10+)"];
            ,
                Print["Compile version is 3 (Mathematica version 8-9)"];
            ];
            Abort[];
        ];
        If[Not[TrueQ[QSetCompressionOn[]]],
            Print["Could not set database compression"];
            Abort[];
        ];
        (*
        If[Not[TrueQ[QSetAutoBucketOn[]]],
            Print["Could not set automatic bucket"];
            Abort[];
        ];
        *)
        If[Not[TrueQ[QSetBucketSize[OptionValue["BucketSize"]]]],
            Print["Could not set initial bucket"];
            Abort[];
        ];
        If[OptionValue["NoDatabaseLock"],
            If[Not[TrueQ[QSetNoLock[]]],
                Print["Could not turn database lock off"];
                Abort[];
            ];
        ];
    ];
    (
        If[Not[QOpenWithShortName[ActualDataPath[options]<>##,##]],
            Print["Could not open database ",##];
            Abort[];
        ];
    )&/@Databases;
]

TestIntegration::usage = "TestIntegration[options] tests CIntegratePool with given options before starting sector decomposition to prevent waste of time";
TestIntegration[options:OptionsPattern[FIESTA]]:=Module[{temp, command, res},
    If[OptionValue["UsingC"],
        command=OptionValue["FIESTAPath"]<>"/bin/CIntegratePool --Test"<>CommandOptions[options];
        res=ReadList["!"<>command,String];
        If[Not[And[Length[res]===2,res[[1]]==="Ok"]],
            Print["CIntegratePool test failed"];
            Print[command];
            Print/@res;
            Abort[];
        ];
        RawPrintLn["Using integrator settings: ",res[[2]]];
        RawPrintLn["Integration test passed"];
    ];
]

StartSubkernels::usage = "StartSubkernels[restart, options] closes saubkernels in case of restart, then starts them";
StartSubkernels[restart_,options:OptionsPattern[FIESTA]]:=Module[{temp},
    If[Or[Not[IntegerQ[OptionValue["NumberOfSubkernels"]]],OptionValue["NumberOfSubkernels"]<0],
        Print["Incorrect number of Subkernels"];
        Abort[];
    ];
    If[And[Length[Kernels[]]>0,Nor[restart]],
        Return[];
    ];

    If[Length[Kernels[]]>0,
        ParallelEvaluate[Abort[]];
        Parallel`Developer`ClearKernels[];
        Parallel`Developer`ResetQueues[];
        CloseKernels[];
    ];

    RawPrintLn["Starting subkernels"];
    LaunchKernels[Max[OptionValue["NumberOfSubkernels"],1]];
    DistributeDefinitions["FIESTA`", "Combinatorica`"];
    If[OptionValue["NumberOfSubkernels"]==0,
        RawPrintLn["Subkernel will be used for launching external programs, all evaluations go on main kernel."];
    ];

    If[TrueQ[$VersionNumber>=9.0],
        SetSystemOptions[CacheOptions -> Symbolic -> Cache -> False];
        SetSystemOptions[CacheOptions -> Numeric -> Cache -> False];
        SetSystemOptions[CacheOptions -> Developer -> Cache -> False];
        SetSystemOptions[CacheOptions -> ParametricFunction -> Cache -> False];
        SetSystemOptions[CacheOptions -> Quantity -> Cache -> False];
    ,
        SetSystemOptions[CacheOptions -> Symbolic -> False];
        SetSystemOptions[CacheOptions -> Numeric -> False];
        SetSystemOptions[CacheOptions -> Constants -> False];
        SetSystemOptions[CacheOptions -> CacheKeyMaxBytes -> 1];
        SetSystemOptions[CacheOptions -> CacheResultMaxBytes -> 1];
    ];

    ParallelEvaluate[
        If[TrueQ[$VersionNumber>=9.0],
            SetSystemOptions[CacheOptions -> Symbolic -> Cache -> False];
            SetSystemOptions[CacheOptions -> Numeric -> Cache -> False];
            SetSystemOptions[CacheOptions -> Developer -> Cache -> False];
            SetSystemOptions[CacheOptions -> ParametricFunction -> Cache -> False];
            SetSystemOptions[CacheOptions -> Quantity -> Cache -> False];
        ,
            SetSystemOptions[CacheOptions -> Symbolic -> False];
            SetSystemOptions[CacheOptions -> Numeric -> False];
            SetSystemOptions[CacheOptions -> Constants -> False];
            SetSystemOptions[CacheOptions -> CacheKeyMaxBytes -> 1];
            SetSystemOptions[CacheOptions -> CacheResultMaxBytes -> 1];
        ];
    ];
]

InitializeAllLinks::usage = "InitializeAllLinks[options] deletes databases if needed, runs codeinfo, integration test and subkernels start";
InitializeAllLinks[options:OptionsPattern[FIESTA]]:=Module[{temp,i,j},
    If[FileExistsQ[ActualDataPath[options]<>"in.kch"],
        DeleteFile[ActualDataPath[options]<>"in.kch"]
    ];
    If[FileExistsQ[ActualDataPath[options]<>"out.kch"],
        DeleteFile[ActualDataPath[options]<>"out.kch"]
    ];
    If[FileExistsQ[ActualDataPath[options]<>"1.kch"],
        DeleteFile[ActualDataPath[options]<>"1.kch"]
    ];
    If[FileExistsQ[ActualDataPath[options]<>"2.kch"],
        DeleteFile[ActualDataPath[options]<>"2.kch"]
    ];

    SetOptions[Asy`QHull,
        "Executable" -> OptionValue["QHullPath"],
        "InFile" -> ActualDataPath[options]<>"_qhi.tmp",
        "Mode" -> "Fv",
        "OutFile" -> ActualDataPath[options]<>"_qho.tmp"
    ];

    CodeInfo[options];
    TestIntegration[options];
    StartSubkernels[True,options];
]

UF[xx_, yy_, z_, options:OptionsPattern[FIESTA]] := Module[{degree, coeff, i, t2, t1, t0, zz},
    zz = Map[Rationalize[##,0]&, z, {0, Infinity}];
    degree = -Sum[yy[[i]]*OptionValue["XVar"][i], {i, 1, Length[yy]}];
    coeff = 1;
    For[i = 1, i <= Length[xx], i++,
        t2 = Coefficient[degree, xx[[i]], 2];
        t1 = Coefficient[degree, xx[[i]], 1];
        t0 = Coefficient[degree, xx[[i]], 0];
        coeff = coeff*t2;
        degree = Together[t0 - ((t1^2)/(4 t2))];
    ];
    degree = Together[-coeff*degree] //. zz;
    coeff = Together[coeff] //. zz;
    {coeff, Expand[degree], Length[xx]}
]

CodeInfo::usage = "CodeInfo[options] prints version and options information";
CodeInfo[options:OptionsPattern[FIESTA]]:=Module[{temp, VersionString},
    VersionString:="FIESTA 5.0";
    temp = PageWidth /. Options[$Output];
    SetOptions[$Output,PageWidth->Infinity];
    Print["Running "<>VersionString<>" with options: "<>ToString[Rule[##, OptionValue[##]] & /@ First /@ Options[FIESTA],InputForm]];
    SetOptions[$Output,PageWidth->temp];
]

KillSmall::usage = "KillSmall[x, options] replaces expression with zero if it is small absolutely or compared to the error estimate (the variables in the expression are assumed to be error estimates)";
KillSmall[x_,options:OptionsPattern[FIESTA]] := Module[{temp, main, add},
    temp = Variables[x];
    main = x /. ((## -> 0) & /@ temp);
    add = (x - main) /. ((## -> 1) & /@ temp);
    If[TrueQ[Or[
            And[Abs[main] < 3 Abs[add], Abs[main] < 300 * 10^-OptionValue["Precision"]],
            Abs[main] < 3 * 10^-OptionValue["Precision"]]
        ],
        0,
        x
    ]
]

CutExtraDigits::usage = "CutExtraDigits[xx, options] remove extra digits of a number leaving non-numbers untouched";
CutExtraDigits[xx_,options:OptionsPattern[FIESTA]] := Module[{temp},
    Map[
        If[And[
                NumberQ[##],
                Not[IntegerQ[##]],
                Not[Head[Abs[##]] === Rational]
            ],
            Chop[
                N[Round[##, 10^-OptionValue["Precision"]]],
                10^-OptionValue["Precision"]
            ]
        ,
            ##
        ]&,
        xx,
        {0, Infinity}
    ]
]

DoubleCutExtraDigits::usage = "DoubleCutExtraDigits[xx, options] calls CutExtraDigits for the number and for the error estimate in case the expression is of a+b*pm form";
DoubleCutExtraDigits[xx_,options:OptionsPattern[FIESTA]]:=Module[{temp},
    If[xx===Indeterminate,Return[Indeterminate]];
    temp=Variables[xx];
    If[Length[temp]>1,Return[xx]];
    If[Length[temp]==0,Return[CutExtraDigits[xx,options]]];
    CutExtraDigits[xx/.(temp[[1]]->0),options]+CutExtraDigits[(xx-(xx/.(temp[[1]]->0)))/.(temp[[1]]->1),options]*temp[[1]]
]

PMForm::usage = "PMForm[xx, options] prepares a printable form with +- error estimate";
PMForm[xx_]:=Module[{temp,PMSymbol},
    PMSymbol=If[$FrontEnd===Null,"+-","±"]
    If[xx===Indeterminate,Return["INDETERMINATE"]];
    temp=Variables[xx];
    If[Length[temp]>1,Return[ToString[xx,InputForm]]];
    If[Length[temp]==0,Return[ToString[xx,InputForm]]];
    ToString[xx/.(temp[[1]]->0),InputForm]<>" "<>PMSymbol<>" "<>ToString[(xx-(xx/.(temp[[1]]->0)))/.(temp[[1]]->1),InputForm]
]

RawPrint::usage = "RawPrint[x] prints to output channel";
RawPrint[x__]:=WriteString[$Output,x];

RawPrintLn::usage = "RawPrintLn[x] prints to output channel and ends a line";
RawPrintLn[x__]:=WriteString[$Output,x,"\n"];
RawPrintLn[]:=WriteString[$Output,"\n"];

MyTimingForm::usage = "MyTimingForm[x] is a pretty version of time string";
MyTimingForm[xx_]:=ToString[Chop[N[Round[xx,10^-4]],10^-4],InputForm]

FactorMonom::usage = "FactorMonom[xxx, x] splits function into a product of variables x[_] and the remaining part";
FactorMonom[xxx_, x_]:=Module[{xx,vars,i,monom,temp,var},
    xx=xxx;
    If[xx===0,Return[{0,0}]];
    vars=Union[Cases[xx, x[_], {0, Infinity}]];
    If[Head[xx]===Times,xx=List@@xx,xx={xx}];
    temp=-(var=##;Plus@@(
        If[Head[##] === Power,
            Exponent[##[[1]],var^-1]*##[[2]],
            Exponent[##,var^-1]
        ]&/@xx
    ))&/@vars;
    monom=Times@@(vars^temp);
    {monom,Expand[Times@@xx/(monom)]}
]

FactorLogMonom::usage = "FactorLogMonom[xxx, x] splits function into a product of logarithms of variables x[_] and the remaining part";
FactorLogMonom[xxx_, x_]:=Module[{xx,vars,i,monom,temp},
    xx=xxx;
    If[xx===0,Return[{0,0}]];
    vars=Sort[Cases[Variables[xx],Log[x[_]]]];
    temp=-Exponent[xx,##^-1]&/@vars;
    monom=Times@@(vars^temp);
    {monom,Expand[xx/(monom)]}
]

FactorMonomNew::usage = "FactorMonomNew[xxx, vr] splits function into a product of variables and the remaining part; this is a new version used in additional sector decomposition, to be safe the old one is also left in the code";
FactorMonomNew[xxx_,varname_]:=Module[{xx,vars,i,monom,temp,var,j},
    xx=xxx;
    If[xx===0,Return[{0,0}]];
    vars=Union[Cases[xx, varname[_], {0, Infinity}]];
    If[Head[xx]===Times,xx=List@@xx,xx={xx}];
    monom = 1;
    For[i=1, i<= Length[vars], ++i,
        var = vars[[i]];
        For[j=1, j<=Length[xx], ++j,
            If[Head[xx[[j]]] === Power,
                temp = Exponent[xx[[j,1]], var^-1];
                monom*= var^(-temp*xx[[j,2]]);
                xx[[j,1]] = Expand[xx[[j,1]] * var^temp];
            ,
                temp = Exponent[xx[[j]], var^-1];
                monom*= var^-temp;
                xx[[j]] = Expand[xx[[j]]*var^temp];
            ]
        ];
    ];
    {monom,Times@@xx}
]

ConstructTerm::usage = "ConstructTerm[xx, extraVar, options] combines an expression after epsilon expansion from an internal structure; If extraVar, then last of the indices are expansion var powers";
ConstructTerm[xx_, extraVar_, options:OptionsPattern[FIESTA]]:=Module[{temp,i},
    temp = xx;
    If[(OptionValue["RegVar"] =!= None),
        temp[[1]] = If[##>0,2,0]&/@temp[[1]]
    ]; (*strange fix???????*)
    If[extraVar,
        temp[[1]] = Drop[temp[[1]],-1];
        temp[[2]] = Drop[temp[[2]],-1];
        temp[[3]] = Drop[temp[[3]],-1]
    ];  (*powers of ex var *)
    temp = {
        Flatten[Position[Table[If[FreeQ[temp[[2]][[i]],z],temp[[1]][[i]],0],{i,Length[temp[[1]]]}],2]],
        Inner[(#1^#2)&,Array[OptionValue["XVar"],Length[temp[[1]]]],temp[[2]],Times] * Inner[(Log[#1]^#2)&,Array[OptionValue["XVar"],Length[temp[[1]]]],temp[[3]],Times] * xx[[4]],
        If[extraVar,{Last[xx[[2]]],Last[xx[[3]]]},{0,0}],
        xx[[5]]
    };
    temp
]

MyExpandDenominator::usage = "MyExpandDenominator[x, ep] makes all denominators in the expression expanded which is essensial for pole search. ep used not to expand ep pattern";
MyExpandDenominator[x_, ep_] := Module[{part1, part2,temp},
    If[Head[x] === Power,
        temp={x};
    ];
    If[Head[x] === Times,
        temp=List@@x;
    ];
    If[Head[temp]=!=List,
        Return[x];
    ];
    part1 = Select[temp, (And[Head[##] === Power, ##[[2]] < 0]) &];
    part2 = Select[temp, Not[TrueQ[(And[Head[##] === Power, ##[[2]] < 0])]] &];
    part2=Times@@part2;
    part1=ExpandAll[##,ep]&/@part1;
    part1=Times@@part1;
    Return[part1*part2];
]

ZPosExpand::usage = "ZPosExpand[x, options] performs a Tailor expansion by the regularization variable";
ZPosExpand[x_, zz_]:=Module[{xx, temp, result, shift, i, j, der, z},
    xx = x /. zz->z;
    Check[
        shift=0;
        temp={xx[[1]], xx[[2]], xx[[3]],  xx[[4]]/.MBshiftRules[z], xx[[5]], xx[[6]]};

        While[Quiet[Check[z=0;Evaluate[temp[[4]]],False]===False],
            Clear[z];
            If[shift < -20,
            Print["big shift in ZPosExpand"];
            Print[xx];
            Abort[];
            ];
            shift--;
            temp[[4]] = Expand[temp[[4]]*z,z];
        ];
        shift=-shift;
        Clear[z];
        result = Reap[
            der={temp};
            j=0;
            While[j<=shift,
            Sow[Append[##,j-shift]]&/@der;
            If[j==shift,Break[]];
            der=Flatten[(
                DeleteCases[
                Append[
                    Table[
                        If[D[##[[2]][[i]],z]===0,
                        {},
                        {##[[1]],##[[2]],ReplacePart[##[[3]],i->(##[[3]][[i]]+1)],##[[4]]*D[##[[2]][[i]],z],##[[5]],##[[6]]}
                        ]
                    ,
                    {i,Length[##[[1]]]}]
                ,
                    {##[[1]],##[[2]],##[[3]],D[##[[4]],z],##[[5]],##[[6]]} (*differentiating the expression*)
                ]
                ,{}] (*variable has no power*)
            )&/@der,1];
            j++;
            der={##[[1]],##[[2]],##[[3]],##[[4]]/j,##[[5]],##[[6]]}&/@der;
            ];
            ][[2]][[1]];
            z = 0;
            result = result;
            result=DeleteCases[result,{_,_,_,0,_,_,_}];
            Clear[z];
        ,
        Print["ZPosExpand error"];
        Print[xx];
        Abort[];
    ];
    result={Append[##[[1]],0],Append[##[[2]],##[[7]]],Append[##[[3]],0],##[[4]],##[[5]],##[[6]]}&/@result;
    If[Or@@((##[[4]]===ComplexInfinity)&/@result),
        Print["ZPosExpand error resulted in ComplexInfinity"];
        Print[xx];
        Abort[];
    ];
    (* now putting external var power to ind powers*)
    result
]

EpPosExpand::usage = "EpPosExpand[xx, extra, inshift, order, options] expands xx in ep up to order order assuming a order shift by insift and carrying extra through all the terms";
EpPosExpand[xx_, extra_, inshift_, order_, options:OptionsPattern[FIESTA]] := Module[{temp, shift, i, der, result, ep},
    Check[shift = 0; temp = MyExpandDenominator[xx/. OptionValue["EpVar"]->ep, ep] /. MBshiftRules[ep];
        While[Quiet[Check[ep=0;Evaluate[temp],False]===False],
            Clear[ep];
            temp = Expand[temp*ep,ep];
            shift++;
            If[shift > 20,Print["big shift"]; Print[xx];
                Print[extra];
                Print[inshift];
                Print[order];
                Abort[];
            ];
        ];
        Clear[ep];
        If[inshift-shift>order,Return[{}]];
        result = Reap[
            Sow[{-shift + inshift, {temp, extra /. OptionValue["EpVar"] -> ep}}];
            der = {{temp, extra /. OptionValue["EpVar"] -> ep}};
            For[i = 1, i <= order -inshift + shift, i++,
                der = Flatten[If[
                    D[##[[2]][[1]], ep] =!= 0,
                    {{D[##[[1]], ep], ##[[2]]}, {##[[1]]*D[##[[2]][[1]], ep], {##[[2]][[1]], ##[[2]][[2]] + 1}}},
                    {{D[##[[1]], ep], ##[[2]]}}
                ] & /@ der, 1];
                Sow[{-shift + i + inshift, {##[[1]]/Factorial[i], ##[[2]]}}] & /@ der
            ]
        ][[2]][[1]];
        ep = 0;
        result = result;
        result=DeleteCases[result,{_,{0,_}}];
        Clear[ep];
    ,
        Print["ep pos expand"]; Print[xx];Print[extra];Print[inshift]; Print[order]; Abort[]
    ];
    result /. ep->OptionValue["EpVar"]
]

CountVars::usage = "CountVars[xx, var] finds the largest number for var appearing in xx";
CountVars[xx_, var_]:=Module[{vars},
    vars = Union[Cases[xx,var[_],{0,Infinity}]];
    {xx,Max@@((##[[1]])&/@vars)}
]

AdvancedFirstVars::usage = "AdvancedFirstVars[[{yyy, xx, zz, uu}, second, var] changes the variables in expression to var[i] starting from 1. There might be a nontrivial order change";
AdvancedFirstVars[{yyy_,xx_,zz_,uu_},second_,var_]:=Module[{yy=var/@yyy,vars,rules,y},
    vars = Union[Cases[{xx,second},var[_],{0,Infinity}]];
    vars=Sort[vars,(If[TrueQ[MemberQ[yy,#1]],If[TrueQ[MemberQ[yy,#2]],#1[[1]]<#2[[1]],True],False])&];
    rules=Apply[Rule,Transpose[{vars,Array[y,Length[vars]]}],{1}];
    {xx/.rules,zz,uu,second//.rules} /. y->var
]

StringForIntegration::usage = "StringForIntegration[xx] prepares integration string for the database from an expression";
StringForIntegration[xx_]:=StringReplace[ToString[xx//.{Power->p,Log->l,Pi->P,EulerGamma->G,ImaginaryShiftVariable->0},InputForm],{"\\"->"","\""->""}];

StringForIntegrationOptimized::usage = "StringForIntegrationOptimized[xx] prepares integration string for the database from an expression using Mathematica optimized expression";
StringForIntegrationOptimized[xx_]:=Module[{oexp, locals, code, a, b, c, seq, vars, f},
    oexp = Experimental`OptimizeExpression[xx];
    {locals, code} =ReleaseHold[(Hold @@ oexp) /. Verbatim[Block][vars_, seq_] :> {vars, Hold[seq]}];
    code = code /. Hold[CompoundExpression[seq__]] :> Hold[{seq}];
    code = First[code //. Hold[{a___Hold, b_, c___}] /; Head[Unevaluated[b]] =!= Hold :> Hold[{a, Hold[b], c}]];
    code = Append[Apply[Rule, Drop[code, -1], {2}], {Rule[Null, ReleaseHold[Last[code]]]}];
    code = First /@ code;
    locals = Rule @@@ Transpose[{locals, Array[f, Length[locals]]}];
    code = Last/@(code /. locals);
    code = (StringForIntegration[##] <> ";\n")& /@code;
    code = StringDrop[StringJoin@@code, -2];
    locals = First /@ locals;
    locals = Remove /@ locals;
    Remove /@ Drop[Names["Compile`$*"], 1];
    ToString[Length[locals],InputForm] <> ";\n" <> code
]

DoIntegrate::usage = "DoIntegrate[xx, tryexact, options] performa an integration with Mathematica methods, trying first analytical integration in case of tryexact";
DoIntegrate[xx_,tryexact_,options:OptionsPattern[FIESTA]] := Module[{temp2,vars, temp,i,rules,met,res,$Timeout},
    If[NumberQ[xx],Return[{True,xx}]];
    vars = Union[Cases[xx,OptionValue["XVar"][_],{0,Infinity}],Cases[xx,OptionValue["XVar"][_],{0,Infinity}]];
    If[Length[vars]===0,Return[{True,Plus@@xx}]];
    If[Plus@@xx===0,Return[{True,0}]];

    temp= Join[{Plus@@(xx)},{##, 0,1} & /@ vars];

    If[tryexact,
        res=Quiet[TimeConstrained[Integrate@@temp,OptionValue["ExactIntegrationTimeout"],$Timeout]];
        If[And[Head[res]=!=Integrate,res=!=$Timeout],
            Return[{True,res}];
        ]
    ];
    temp=Join[temp,
        {
            Method -> Global`AdaptiveQuasiMonteCarlo,
            Compiled->True,
            MinRecursion -> 100,
            MaxRecursion -> 10000,
            MaxPoints -> 100000,
            PrecisionGoal -> 4
        }
    ];
    res=Apply[NIntegrate, temp, {0}];
    {False,res}
]

ZNegExpand::usage = "ZNegExpand[xx, z, options] performs a pole term substraction for an expansion or regularization variable";
ZNegExpand[xx_, z_, options:OptionsPattern[FIESTA]]:=Module[{i,j,deg,ders,der,result,ExpandMode,ep},
    ep = OptionValue["EpVar"];
    ExpandMode = (xx[[7,3]] =!= None);
    Check[
        i=1;
        If[ExpandMode,
            For[j=1,j<=Length[xx[[1]]],j++,
                If[And[
                        xx[[1]][[j]]===1,
                        Coefficient[xx[[2]][[j]],z]<0,
                        (xx[[2]][[j]]/.{ep->0,z->0})<=0,
                        -xx[[7]][[3]]+(xx[[2]][[j]]/.{ep->0,z->0} )<0
                    ]
                ,
                    i=j
                ]
            ]
        ];
        For[Null,i<=Length[xx[[1]]],i++,
            If[Or[xx[[1]][[i]]=!=1,Coefficient[xx[[2]][[i]],z]===0],Continue[]];
            If[Or[xx[[1]][[i]]=!=1,And[ExpandMode,Coefficient[xx[[2]][[i]],z]>0]],Continue[]];
            If[ExpandMode,
                deg=-xx[[7]][[3]]+(xx[[2]][[i]]/.{ep->0,z->0} ),
                deg=1+(xx[[2]][[i]]//.Flatten[{ep->0,z->-0.2}]);
            ];
            If[(OptionValue["RegVar"] =!= None),
                deg=0.1+(xx[[2]][[i]]//.{ep->Indeterminate,z->0})
            ];
            If[deg<0,
                Return[
                    Flatten[
                        ZNegExpand[##, z, options]&/@Reap[
                            If[xx[[2]][[i]]-deg==0,Print["Impossible degree (z)"];Abort[]];
                            ders={};(*vvv={};*)
                            For[j=0,j+deg<0,j++,
                                If[j===0,der=xx[[4]],der=D[der,OptionValue["XVar"][i]]/j];
                                (*AppendTo[vvv,(xx[[2]][[i]]+j+1)];*)
                                AppendTo[ders,der/.OptionValue["XVar"][i]->0];
                                If[Expand[ders[[j+1]]]=!=0,
                                    Sow[{ReplacePart[xx[[1]],0,i],
                                        ReplacePart[xx[[2]],0,i],
                                        xx[[3]],
                                        ders[[j+1]]/(xx[[2]][[i]]+j+1),
                                        xx[[5]],
                                        xx[[6]]/.OptionValue["XVar"][i]->0,
                                        {xx[[7]][[1]],Append[xx[[7]][[2]],(xx[[2]][[i]]+j+1)],xx[[7]][[3]]-If[Length[Position[xx[[2]],xx[[2]][[i]]]]>0,0,j]}
                                    }];
                                ];
                            ];
                            If[Expand[xx[[4]]-(ders.Table[OptionValue["XVar"][i]^(j-1),{j,Length[ders]}])]=!=0,
                                Sow[{ReplacePart[xx[[1]],2,i],
                                    xx[[2]],
                                    xx[[3]],
                                    xx[[4]]-(ders.Table[OptionValue["XVar"][i]^(j-1),{j,Length[ders]}]),
                                    xx[[5]],
                                    xx[[6]],
                                    xx[[7]]
                                }];
                            ]
                        ][[2]][[1]]
                    ,
                        1
                    ]
                ];
            ];
        ];
    ,
        Print[xx];
        Abort[]
    ];
    {xx}
];

EpNegExpand::usage = "EpNegExpand[xxx, options] performs a pole term substraction for an expansion by ep";
EpNegExpand[xxx_, options:OptionsPattern[FIESTA]]:=Module[{i,j,deg,ders,der,HasDelta,temp,xx,DeltaPower,ep},
    ep = OptionValue["EpVar"];
    xx=Take[xxx,6];
    For[i=1,i<=Length[xx[[1]]],i++,
        If[xx[[2]][[i]]===0,Continue[]];
        HasDelta=MemberQ[Variables[xx[[2]][[i]]],delta[_]];
        If[HasDelta,
            DeltaPower=Cases[Variables[xx[[2]][[i]]],delta[_]][[1]][[1]]
        ];
        temp=xx[[2]];
        temp[[i]]=temp[[i]]/.delta[_]->0;
        xx[[2]]=temp;
        deg=(xx[[2]][[i]]/.Flatten[{ep->0,z->-0.2}]);
        If[And[xx[[1]][[i]]===1,deg<=0],
            Return[
                Flatten[EpNegExpand[##,options]&/@Reap[
                    If[And[xx[[3]][[i]]===0,OptionValue["ResolutionMode"]==="IBP0"],
                        If[HasDelta,
                            Print["Resolution mode does not work with delta"];
                            Abort[]
                        ];
                        der=xx[[4]];
                        For[j=0,j+deg<0,j++,
                            der=der/(j+1+xx[[2]][[i]]);
                            Sow[{
                                ReplacePart[xx[[1]],0,i],
                                ReplacePart[xx[[2]],0,i],
                                ReplacePart[xx[[3]],0,i],
                                der/.(OptionValue["XVar"][i]->0),
                                xx[[5]],
                                xx[[6]]/.(OptionValue["XVar"][i]->0)
                            }];
                            der=-D[der,OptionValue["XVar"][i]];
                            Sow[{
                                ReplacePart[xx[[1]],2,i],
                                ReplacePart[xx[[2]],0,i],
                                ReplacePart[xx[[3]],0,i],
                                -der,
                                xx[[5]],
                                xx[[6]]/.(OptionValue["XVar"][i]->0)
                            }];
                        ];
                        Sow[{
                            ReplacePart[xx[[1]],2,i],
                            ReplacePart[xx[[2]],xx[[2]][[i]]+j,i],
                            xx[[3]],
                            der,
                            xx[[5]],
                            xx[[6]]
                        }];
                    ];
                    If[And[xx[[3]][[i]]===0,OptionValue["ResolutionMode"]==="IBP1"],
                        If[HasDelta,
                            Print["Resolution mode does not work with delta"];
                            Abort[]
                        ];
                        ders={};
                        der=xx[[4]];
                        For[j=0,j+deg<0,j++,
                            der=der/(j+1+xx[[2]][[i]]);
                            AppendTo[ders,der/.OptionValue["XVar"][i]->1];
                            Sow[{
                                ReplacePart[xx[[1]],0,i],
                                ReplacePart[xx[[2]],0,i],
                                ReplacePart[xx[[3]],0,i],
                                Last[ders],
                                xx[[5]],
                                xx[[6]]/.(OptionValue["XVar"][i]->1)
                            }];
                            der=D[der,OptionValue["XVar"][i]]/i;
                            der=-der*i;
                        ];
                        Sow[{
                            ReplacePart[xx[[1]],2,i],
                            ReplacePart[xx[[2]],xx[[2]][[i]]+j,i],
                            xx[[3]],
                            der,
                            xx[[5]],
                            xx[[6]]
                        }];
                    ];
                    If[Or[xx[[3]][[i]]=!=0,OptionValue["ResolutionMode"]==="Taylor"],
                        ders={};
                        For[j=0,If[HasDelta,j<DeltaPower,j+deg<0],j++,
                            If[j===0,der=xx[[4]],der=D[der,OptionValue["XVar"][i]]/j];
                            AppendTo[ders,der/.OptionValue["XVar"][i]->0];
                            If[Not[HasDelta],
                                Sow[{
                                    ReplacePart[xx[[1]],0,i],
                                    ReplacePart[xx[[2]],0,i],
                                    ReplacePart[xx[[3]],0,i],
                                    If[((xx[[2]][[i]]+j+1)/.ep->0)===0,
                                        ders[[j+1]] * ((-1)^xx[[3]][[i]]) * (xx[[3]][[i]]!)/(((Coefficient[xx[[2]][[i]]+j+1,ep])^(xx[[3]][[i]]+1)))
                                    ,
                                        ders[[j+1]] * ((-1)^xx[[3]][[i]]) * (xx[[3]][[i]]!)/(((xx[[2]][[i]]+j+1)^(xx[[3]][[i]]+1)))
                                    ],
                                    If[((xx[[2]][[i]]+j+1)/.ep->0)===0,
                                        xx[[5]]-xx[[3]][[i]]-1
                                    ,
                                        xx[[5]]
                                    ],
                                    xx[[6]]/.(OptionValue["XVar"][i]->0)
                                }];
                            ];
                        ];
                        Sow[{
                            ReplacePart[xx[[1]],2,i],
                            xx[[2]],
                            xx[[3]],
                            xx[[4]]-(ders.Table[OptionValue["XVar"][i]^(j-1),{j,Length[ders]}]),
                            xx[[5]],
                            xx[[6]]
                        }];
                    ];
                ][[2]][[1]],1]
            ];
        ];
    ];
    {xx}
];


PMSimplify::usage = "PMSimplify[xx, options] combines different error estimates into one by mean-square norm";
PMSimplify[xx_, options:OptionsPattern[FIESTA]] := Module[{temp},
    If[Length[Variables[xx]]===0,Return[xx]];
    temp = Sort[Variables[Expand[xx]]];
    If[MemberQ[temp,Indeterminate],Return[Indeterminate]];
    (xx /. (Rule[##, 0] & /@ temp)) +
        ((Plus @@ (Abs[Coefficient[xx, ##]]^2 & /@ temp))^(1/2))*
        If[OptionValue["ReturnErrorWithBrackets"],OptionValue["PMVar"][PMCounter++],ToExpression[ToString[OptionValue["PMVar"]] <> ToString[PMCounter++]]]
]

VarDiffReplace::usage = "VarDiffReplace[xx, yy] performs a replacement in expression xx by a VarDiffRule[yy]";
VarDiffReplace[xx_, yy_] := Module[{temp},
    temp = xx /. VarDiffRule[yy];
    Expand[temp]
]

VarDiffRule::usage = "VarDiffRule[xx] is a replacement adding half of the last index listed to the other ones and dividing the last one by two";
VarDiffRule[xx_] := Module[{temp, i, j},
    temp = Table[
        If[i === j,
            If[i < Length[xx], 1, 2],
            If[j === Length[xx], -1, 0]
        ]
    ,
        {i, 1, Length[xx]}, {j, 1, Length[xx]}
    ];
    temp = Inverse[temp];
    Apply[Rule, Transpose[{xx, temp.xx}], {1}]
]

AdvancedVarDiffReplace::usage = "AdvancedVarDiffReplace[xx, yy, options] searches for the best was to perform a replacement similar to VarDiffRule[yy] to get rid of most negative terms and runs it on xx";
AdvancedVarDiffReplace[xx_, yy_, options:OptionsPattern[FIESTA]] := Module[{temp, table,rule,y},
    Check[
        temp = If[Head[xx]===List,xx[[3]][[2]],xx];
        temp = NegTerms[temp, options];
        temp = Variables[temp];
        temp = If[Head[xx]===List,xx[[3]][[2]],xx] /. {OptionValue["XVar"][i_] :> If[MemberQ[temp, OptionValue["XVar"][i]], OptionValue["XVar"][i], 0]};
        temp = {Coefficient[Coefficient[temp, OptionValue["XVar"][yy[[1]]]], OptionValue["XVar"][yy[[2]]]],
            Coefficient[temp, OptionValue["XVar"][yy[[1]]], 2] /. {OptionValue["XVar"][yy[[1]]] -> 0,
            OptionValue["XVar"][yy[[2]]] -> 0},
            Coefficient[temp, OptionValue["XVar"][yy[[2]]], 2] /. {OptionValue["XVar"][yy[[1]]] -> 0,
            OptionValue["XVar"][yy[[2]]] -> 0}
        };
        temp=Expand/@temp;
        If[And[temp[[3]]=!=0,temp[[2]]=!=0],
            If[NumberQ[Together[temp[[1]]^2/(temp[[2]]*temp[[3]])]],
                temp = Sqrt[Together[temp[[3]]/temp[[2]]]];
                If[Or[Head[temp] === Rational, Head[temp] === Integer],
                    temp = {{1, temp/2}, {0, 1/2}};
                    rule=Apply[Rule, Transpose[{(OptionValue["XVar"][##] & /@ yy), temp.(y[##] & /@ yy)}], {1}];
                    temp = xx /. rule;
                    Return[Expand[temp /. y -> OptionValue["XVar"]]];
                ,
                    RawPrintLn["STRANGE SQUARE ROOT!"];
                ]
            ]
        ];
        Return[VarDiffReplace[xx, OptionValue["XVar"]/@yy]];
    ,
        Print[xx];
        Print[yy];
        Abort[];
    ]
]

BlockMatrix::usage = "BlockMatrix[MatrixList, pos, n] enlarges each of the MatrixList matrices to size n using non-zero at indices pos and adding 1 at diagonal outside the block";
BlockMatrix[MatrixList_, pos_, n_] := Module[{UU,VV},
    Normal[SparseArray[
        Join[Rule[{##,##},1]&/@Complement[Range[n],pos], Flatten[Map[Rule[##[[1]][[1]], ##[[2]][[1]]] &, Map[UU, Outer[List, pos, pos], {2}] + Map[VV,##,{2}], {2}], 1]], {n,n}
    ]] &/@MatrixList
]

ClearDatabase::usage = "ClearDatabase[index, reopen, options] closes a database, removes the files and opens it again in case of reopen";
ClearDatabase[index_,reopen_,options:OptionsPattern[FIESTA]]:=Module[{name},
    name=RemoveFIESTAName[ActualDataPath[options]<>ToString[index]];
    QClose[name];
    DeleteFile[name<>".kch"];
    If[reopen,QOpenWithShortName[name,index]];
]

MyParallelize::usage = "MyParallelize[x, options] evaluates expression either in parallel or not depending on options";
MyParallelize[x_,options:OptionsPattern[FIESTA]]:=If[OptionValue["NumberOfSubkernels"]>0,Parallelize[x,DistributedContexts -> {"Asy`", "FIESTA`", "Combinatorica`"} (*,Method->"FinestGrained"*)],x];
SetAttributes[MyParallelize,HoldFirst];

SectorDecomposition::usage = "SectorDecomposition[zs, U, intvars, n, j, Z, options] performs a sector decomposition of expression U (list) with variables zs set to 1, number of variables n, active variables having 1 at theis places in intvar, using j for printing and carrying an external part Z";
SectorDecomposition[zs_,U_,intvars_,n_,j_,Z_,options:OptionsPattern[FIESTA]]:=Module[{res,pols,pol,active,SDDone,i,jj,k,temp,degs,localActive,rules,CurrentGraph},
    rules=Rule[OptionValue["XVar"][##],1]&/@(zs);
    If[OptionValue["Strategy"] === "0",
        res={IdentityMatrix[n]};
        Goto[SDDone];
    ];
    If[OptionValue["Strategy"] === "SS",
        pol=Expand[Times@@U]/.rules;
        active=OptionValue["XVar"][##]&/@Range[Length[intvars]];
        CurrentGraph = OptionValue["Graph"][[1]];
        res=FindSDWithExtraInformation[{MyDegrees[pol,active]},MyDeleteEdges[CurrentGraph,zs], options];
        active=##[[1]]&/@active;
        res=BlockMatrix[res,active,n];
        Goto[SDDone];
    ];
    If[Or[OptionValue["Strategy"] === "X", OptionValue["Strategy"] === "S2"],
        pol=Expand[##/.rules]&/@U;
        active=(Union@@(Sort[Cases[Variables[##],OptionValue["XVar"][_]]]&/@pol));
        If[active === {},
            res = {IdentityMatrix[n]};
        ,
            res=FindSD[MyDegrees[##,active]&/@pol, options];
            active=##[[1]]&/@active;
            res=BlockMatrix[res,active,n];
        ];
        Goto[SDDone];
    ];
    If[OptionValue["Strategy"] === "S",
        pol={Expand[Times@@U/.rules]};
        active=(Union@@(Sort[Cases[Variables[##],OptionValue["XVar"][_]]]&/@pol));
        res=FindSD[MyDegrees[##,active]&/@pol, options];
        active=##[[1]]&/@active;
        res=BlockMatrix[res,active,n];
        Goto[SDDone];
    ];
    If[Or[OptionValue["Strategy"] === "KU0",OptionValue["Strategy"] === "KU",OptionValue["Strategy"] === "KUS",OptionValue["Strategy"] === "KU2"],
        If[OptionValue["Strategy"] === "KUS",
            pols=Expand[##/.rules]&/@U;
        ,
            pols={Expand[Times@@U]/.rules};
        ];
        active=(Union@@(Sort[Cases[Variables[##],OptionValue["XVar"][_]]]&/@pols));
        pols = {{MyDegrees[##,active],IdentityMatrix[Length[active]]}}&/@pols;
        For[i=1,i<=Length[pols],i++,
            For[jj=1,jj<=Length[pols[[i]]],jj++,
                degs=pols[[i,jj,1]];
                localActive = Max@@@Transpose[degs];
                localActive = Complement[Range[Length[localActive]],First/@Position[localActive,0]];
                If[localActive === {},
                    (* in this sector the next polynomial was already resolved*)
                    pols[[i, jj]] = {pols[[i, jj, 2]]};
                ,
                    degs = Extract[##,{##}&/@localActive]&/@degs;
                    res=Reap[
                        For[k=1,k<=Length[degs],k++,
                            temp=(##-degs[[k]])&/@degs;
                            temp=Join[temp,IdentityMatrix[Length[localActive]]];
                            temp=SimplexCones[temp, options];
                            Sow/@temp;
                        ];
                    ][[2]][[1]];
                    res=Map[(##/(GCD@@##))&,res,{2}];
                    res=(If[Det[##]>0,##,Join[{##[[2]],##[[1]]},Drop[##,2]]])&/@res;
                    res = BlockMatrix[res,localActive,Length[active]];
                    pols[[i, jj]] = (##.pols[[i, jj, 2]])&/@res; (* we make a step, this polynomial is already ok *)
                ];
            ];
            pols[[i]] = Flatten[pols[[i]], 1];
            If[i < Length[pols],
                pols[[i+1]] = {pols[[i+1,1,1]], ##} &/@ pols[[i]];
                For[jj=1,jj<=Length[pols[[i+1]]],jj++,
                    pols[[i+1,jj,1]] = (pols[[i+1,jj,2]].##)&/@pols[[i+1,jj,1]];
                    pols[[i+1,jj,1]] = OnlyLowPoints[NormShift[pols[[i+1,jj,1]]]];
                ]
            ];
        ];
        active=##[[1]]&/@active;
        res=BlockMatrix[Last[pols],active,n];
        Goto[SDDone];
    ];
    (* other strategies *)
    pol=Expand[Times@@U]/.rules;
    active=##[[1]]&/@Sort[Cases[Variables[pol],OptionValue["XVar"][_]]];
    res=FindSD[{MyDegrees[pol, Sort[Cases[Variables[pol],OptionValue["XVar"][_]]]]}, options];
    res=BlockMatrix[res,active,n];
    Label[SDDone];
    If[j > 0,
        RawPrintLn["Primary sector ",j," resulted in ",Length[res]," sectors."];
    ];
    {res,{zs,U,intvars,n,j,Z}}
]

RequiredResidues::usage = "RequiredResidues[gamm, vars, expandOrder, options] locates residues that have to be taken in expression gamm by variables vars (used in SDExpand due to MB)";
RequiredResidues[gamm_,var_,expandOrder_,options:OptionsPattern[FIESTA]]:=Module[{gammas,temp,temp1,temp2,gam,aa,bb,i,res,tt,Timess,ep},
    ep = OptionValue["EpVar"];
    Check[
        gam=gamm//. (Power[aa_, bb_] :> Timess @@ (Table[aa, {Abs[bb]}]));
        gammas = Cases[gam,  Gamma[aa__], Infinity];
        gammas = Select[gammas, (Length[Cases[##[[1]], MBVar, Infinity]] > 0) &];
        temp1=Flatten[(res = {};
            i=0;
            If[Coefficient[##[[1]], MBVar] < 0,
                While[i>=-expandOrder,
                    AppendTo[res,{((i-(##[[1]]/.{MBVar->0}))/Coefficient[##[[1]],MBVar]),Sign[Coefficient[##[[1]],MBVar]]}];i--;
                ];
            ];
            res
        ) & /@ gammas, 1];
        temp2=Flatten[(res = {};
            If[(Coefficient[##[[1]], MBVar] /. {ep->0})==0,Continue[]];
            i=0;
            AppendTo[res,{((i-(##[[1]]/.{MBVar->0}))/Coefficient[##[[1]],MBVar]),Sign[Coefficient[##[[1]],MBVar]]}];
            i--;
            res
        ) & /@ ({##}&/@var), 1];
        temp=Join[temp1,temp2];
        temp=ExpandAll[temp];
        If[Not[And@@(Reap[Sow[HHH[##[[2]]],##[[1]]]&/@temp,_,(Length[Union[#2]]===1)&][[2]])],
            Print["something wrong in RequiredResidues"];
            Print[gam];
            Print[var];
            Abort[];
        ];
        temp=##[[1]] & /@ Reap[Sow[##, ##[[1]]] & /@ temp][[2]];
        temp=Append[##,
            tt=##[[1]];
            Length[
                Join[
                    Select[gammas,And[IntegerQ[Expand[##[[1]]/.MBVar->tt]], Expand[##[[1]]/.MBVar->tt]<=0]&],
                    Select[var,(Expand[##/.MBVar->tt]===0)&]
                ]
            ]
        ]&/@temp;
    ,
        Print[gam];Print[var];Abort[];
    ];
    temp
]

MyResidue::usage = "MyResidue[vardegrees] returns a residue of expression xx by passing a MBGamma pole";
MyResidue[vardegrees_,xx_,x0_,maxorder_]:=Module[{temp,i,j,aa,bb,dx,terms},
    Check[
        terms={{vardegrees[[1]],vardegrees[[2]],xx}};
        terms=terms //. ((aa_:1) MBVar :> Expand[aa x0] + aa dx);
        terms=terms/. MBshiftRules[dx];

        terms={##[[1]],##[[2]],Distribute[(dx^maxorder)*##[[3]]]}&/@terms;

        terms = terms /. MBexpansionRules[dx, -1+maxorder];
        For[i=1,i<=-1+maxorder,i++,
            terms=Append[Table[{##[[1]],ReplacePart[##[[2]],#[[2]][[j]]+1,j],##[[3]]*D[##[[1]][[j]],dx]},{j,Length[##[[1]]]}],
                        {##[[1]],##[[2]],D[##[[3]],dx]}]&/@terms;
            terms=Flatten[terms,1];
            terms=DeleteCases[terms,{aa_,bb_,0}];
        ];

        terms=({##[[1]],##[[2]],Expand[##[[3]],dx]/(-1+maxorder)!}&/@terms) /. (dx -> 0);

        terms=DeleteCases[terms,{aa_,bb_,0}];
        temp=terms;
    ,
        Print[vardegrees];
        Print[xx];
        Print[x0];
        Print[maxorder];
        Abort[]
    ];
    Return[temp];
]

PerformStage::usage = "PerformStage[s, termss, d, prefix, flatten, options] runs an evaluation stage in parallel, using ReadFunction, EvalFunction and WriteFunction; s is source database, d is destanation";
PerformStage[s_,termss_,d_,prefix_,flatten_,options:OptionsPattern[FIESTA]]:=Module[{range,i,temp,result,ev,terms,totalLoad=0,gi,gl,gn,working,finished,runD,runS,tasks={}},
    RawPrint[MyTimingForm[AbsoluteTiming[
        terms=termss;
        If[terms===0,
            terms=Length[QList[s]]
        ];
        If[Not[d==="in"],ClearDatabase[d, True, options]];
        PrepareDots[terms];
        gn=terms;
        gl={};
        For[gi=1,gi<=10,gi++,
            AppendTo[gl,Quotient[gi*gn,10]]
        ];
        gi=0;
        For[runS=1;runD=1,runS<=terms,runS+=OptionValue["ChunkSize"],
            For[i=1,i!=OptionValue["ChunkSize"]+1,++i,
                gi++;
                While[
                    And[Length[gl]>0,gi>=gl[[1]]]
                ,
                    gl=Drop[gl,1];
                    RawPrint["."];
                    If[OptionValue["DebugParallel"],
                        RawPrint["(", N[totalLoad/runS,3],")"];
                    ];
                    result = True;
                ];
                If[Mod[gi,128]===0,
                    ClearSystemCache[]
                ]
            ];
            range = Range[runS, Min[runS + OptionValue["ChunkSize"] - 1, terms]];
            temp = First[{
                ReadFunction[s,prefix,##],
                While[Parallel`Developer`QueueRun[]],
                totalLoad += Length[Select[tasks,(Head[##[[4]]]===Parallel`Developer`running)&]]
            }] &/@ range;
            temp = DeleteCases[temp, ""];
            If[temp === {},
                Continue[];
            ];
            If[OptionValue["NumberOfSubkernels"]===0,
                result = EvalFunction[##, options] &/@ temp;
                If[flatten, result = Flatten[result, 1]];
                (WriteFunction[d,##,prefix,runD,options];runD++)&/@result;
            ,
                If[Length[tasks] >= 2*OptionValue["NumberOfSubkernels"],
                    finished={};
                    While[Parallel`Developer`QueueRun[]];
                    finished=Select[tasks,(Head[##[[4]]]===Parallel`Developer`finished)&];
                    working=Select[tasks,(Head[##[[4]]]=!=Parallel`Developer`finished)&];
                    If[Length[finished]===0,
                        {result,ev,tasks}=WaitNext[tasks];
                        result={result};
                    ,
                        result=WaitAll[finished];
                        tasks=working;
                    ];
                    AppendTo[tasks,ParallelSubmit[{temp}, EvalFunction[##, options] &/@ temp]];
                    result = Flatten[result, 1];
                    If[flatten,result=Flatten[result,1]];
                    If[## === $Aborted,Abort[]]&/@result;
                    (WriteFunction[d,##,prefix,runD,options];runD++)&/@result;
                ,
                    AppendTo[tasks,ParallelSubmit[{temp}, EvalFunction[##, options] &/@ temp]];
                ];
                While[Parallel`Developer`QueueRun[]];
            ]
        ];
        If[And[OptionValue["NumberOfSubkernels"]>0,Length[tasks]>0],
            While[Parallel`Developer`QueueRun[]];
            result=WaitAll[tasks];
            result = Flatten[result, 1];
            If[flatten,result=Flatten[result,1]];
            If[## === $Aborted,Abort[]]&/@result;
            (WriteFunction[d,##,prefix,runD,options];runD++)&/@result;
        ];
    ][[1]]]," seconds"];
    If[runD-1===0,
        RawPrintLn["."];
    ,
        RawPrintLn["; ",runD-1," terms."];
    ];
    If[OptionValue["DebugMemory"],RawPrintLn[MyMemoryInUse[options]]];
    Return[runD-1];
]

PrintAllEntries::usage = "PrintAllEntries[TaskPrefix, n, options] is used for debugging to print all entries after a stage";
PrintAllEntries[TaskPrefix_, n_,options:OptionsPattern[FIESTA]]:=Module[{temp, list},
    list=ToExpression[QGetWrapper[TaskPrefix, n,StringDrop[##, StringPosition[##, "-"][[1, 1]]]]]&/@Sort[QList[n],(ToExpression[#1]<ToExpression[#2])&];
    If[OptionValue["DebugSector"] > 0,
        list=Cases[list,{OptionValue["DebugSector"],_}]
    ];
    Print[InputForm[list]];
    Return[list];
]

SDIntegrate[options:OptionsPattern[FIESTA]]:=Module[{time,stagetime,temp,UsingCDatabase,SCounter,ForEvaluation,runorder,SHIFT,EXTERNAL,iii,command,i,j,res,result3,min,terms,terms0,value,nov,task,CurrentIntegralExact,RequestedOrders,CurrentOrder,CurrentVarDegree,EvC,TaskPrefixes,SCounterAll,ForEvaluationAll,minAll,runOrderAll,SHIFTALL,EXTERNALALL,TaskPrefix,estimateMode,fileName},
    time=MyTimingForm[AbsoluteTiming[
        InitializeQLink[{"in","out"},options];
        StartSubkernels[False,options];
        TaskPrefixes=QSafeGetWrapper["0", "in","","{}"];
        QPutWrapper["0", "out","",TaskPrefixes];
        RequestedOrders=ToExpression[QSafeGetWrapper["0", "in","RequestedOrders",ToStringInputForm[RequestedOrders]]];
        QPutWrapper["0", "out","RequestedOrders",ToStringInputForm[RequestedOrders]];
        TaskPrefixes=ToExpression[TaskPrefixes];

        Clear[terms,terms0,nov];
        For[iii=1,iii<=Length[TaskPrefixes],iii++,  (* preparation cycle on prefixes *)
            TaskPrefix=ToString[TaskPrefixes[[iii]]];

            Check[
                UsingCDatabase=ToExpression[QGetWrapper[TaskPrefix, "in","UsingC"]];
            ,
                Print["Database does not contain required info"];
                Abort[];
            ];
            If[UsingCDatabase=!=OptionValue["UsingC"],
                Print["The database was created with another UsingC setting"];
                Abort[];
            ];
            HasExact[TaskPrefix]=ToExpression[QGetWrapper[TaskPrefix, "in","Exact"]];
            QPutWrapper[TaskPrefix, "out","Exact",ToStringInputForm[HasExact[TaskPrefix]]];
            ExternalExponent[TaskPrefix]=ToExpression[QGetWrapper[TaskPrefix, "in","ExternalExponent"]];
            QPutWrapper[TaskPrefix, "out","ExternalExponent",ToStringInputForm[ExternalExponent[TaskPrefix]]];

            If[HasExact[TaskPrefix],
                ExactValue[TaskPrefix]=ToExpression[QGetWrapper[TaskPrefix, "in","ExactValue"]];
                QPutWrapper[TaskPrefix, "out","ExactValue",ToStringInputForm[ExactValue[TaskPrefix]]];
            ,
                SCounterAll[TaskPrefix]=ToExpression[QGetWrapper[TaskPrefix, "in","SCounter"]];
                ForEvaluationAll[TaskPrefix]=ToExpression[QGetWrapper[TaskPrefix, "in","ForEvaluation"]];
                minAll[TaskPrefix]=Min@@((##[[1]])&/@ForEvaluationAll[TaskPrefix]);
                runorderAll[TaskPrefix]=ToExpression[QGetWrapper[TaskPrefix, "in","runorder"]];
                SHIFTAll[TaskPrefix]=ToExpression[QGetWrapper[TaskPrefix, "in","SHIFT"]];
                EXTERNALAll[TaskPrefix]=ToExpression[QGetWrapper[TaskPrefix, "in","EXTERNAL"]];
                QPutWrapper[TaskPrefix, "out","ForEvaluation",ToStringInputForm[ForEvaluationAll[TaskPrefix]]];
                QPutWrapper[TaskPrefix, "out","runorder",ToStringInputForm[runorderAll[TaskPrefix]]];
                QPutWrapper[TaskPrefix, "out","SHIFT",ToStringInputForm[SHIFTAll[TaskPrefix]]];
                QPutWrapper[TaskPrefix, "out","EXTERNAL",ToStringInputForm[EXTERNALAll[TaskPrefix]]];
                For[i=-SHIFTAll[TaskPrefix],i<=runorderAll[TaskPrefix]-SHIFTAll[TaskPrefix]-minAll[TaskPrefix],i++,
                    QPutWrapper[TaskPrefix, "out","EXTERNAL-"<>ToString[i]<>"E",QGetWrapper[TaskPrefix, "in","EXTERNAL-"<>ToString[i]<>"E"]];
                    QPutWrapper[TaskPrefix, "out","EXTERNAL-"<>ToString[i],QGetWrapper[TaskPrefix, "in","EXTERNAL-"<>ToString[i]]];
                ];
                If[OptionValue["UsingC"],
                    For[EvC=1,EvC<=Length[ForEvaluationAll[TaskPrefix]],EvC++,
                        CurrentOrder=ForEvaluationAll[TaskPrefix][[EvC]][[1]];
                        CurrentVarDegree=ForEvaluationAll[TaskPrefix][[EvC]][[2]];
                        For[i=0,i<10,i++,
                            If[FileExistsQ[ActualDataPath[options]<>"out.kch-"<>TaskPrefix<>"-"<>ToString[CurrentOrder]<>"-"<>StringReplace[ToString[CurrentVarDegree,InputForm], "/" -> "|"]<>"-"<>ToString[i]],
                                DeleteFile[ActualDataPath[options]<>"out.kch-"<>TaskPrefix<>"-"<>ToString[CurrentOrder]<>"-"<>StringReplace[ToString[CurrentVarDegree,InputForm], "/" -> "|"]<>"-"<>ToString[i]]
                            ]
                        ];
                        If[FileExistsQ[ActualDataPath[options]<>"out.kch-"<>TaskPrefix<>"-"<>ToString[CurrentOrder]<>"-"<>StringReplace[ToString[CurrentVarDegree,InputForm], "/" -> "|"]<>"-R"],
                            DeleteFile[ActualDataPath[options]<>"out.kch-"<>TaskPrefix<>"-"<>ToString[CurrentOrder]<>"-"<>StringReplace[ToString[CurrentVarDegree,InputForm], "/" -> "|"]<>"-R"]
                        ];
                        terms[TaskPrefix,CurrentOrder,CurrentVarDegree]=ToExpression[QGetWrapper[TaskPrefix, "in",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-T"]];
                        terms0[TaskPrefix,CurrentOrder,CurrentVarDegree]=ToExpression[QSafeGetWrapper[TaskPrefix, "in",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-T0",terms[TaskPrefix,CurrentOrder,CurrentVarDegree]]];
                        nov[TaskPrefix,CurrentOrder,CurrentVarDegree]=ToExpression[QGetWrapper[TaskPrefix, "in",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-N"]];
                    ];
                ];
            ]
        ];  (* preparation cycle on prefixes *)
        If[OptionValue["UsingC"],
            QClose[ActualDataPath[options]<>"in"];
            command=OptionValue["FIESTAPath"]<>"/bin/CIntegratePool --in "<>ActualDataPath[options]<>"in --fromMathematica --threads "<>ToString[OptionValue["NumberOfLinks"]]<>CommandOptions[options];
            If[OptionValue["NumberOfSubkernels"] == 0,
                ReadList["!"<>command,String];
                task = {0,0,0,0};
            ,
                DistributeDefinitions[command];
                task=ParallelSubmit[(ReadList["!"<>command,String])];
                Parallel`Developer`QueueRun[];
            ];
        ];  (* in case of C we started the program and closed the input database *)

        For[iii=1,iii<=Length[TaskPrefixes],iii++,
            TaskPrefix=ToString[TaskPrefixes[[iii]]];
            If[Or[Length[TaskPrefixes]>1,Not[ExternalExponent[TaskPrefix]===0]],
                If[ExternalExponent[TaskPrefix]===0,
                    RawPrintLn["Integrating task ",TaskPrefix];
                ,
                    RawPrintLn["Integrating task ",TaskPrefix," (multiplied by ",ToString[OptionValue["ExpandVar"]],"^(",ToString[ExternalExponent[TaskPrefix],InputForm],"))"];
                ];
            ];
            If[HasExact[TaskPrefix],
                RawPrintLn["We have an exact answer: ",ToString[ExactValue[TaskPrefix],InputForm]];
                Continue[];
            ];
            stagetime=MyTimingForm[AbsoluteTiming[
                SCounter=SCounterAll[TaskPrefix];
                ForEvaluation=ForEvaluationAll[TaskPrefix];
                min=minAll[TaskPrefix];
                runorder=runorderAll[TaskPrefix];
                SHIFT=SHIFTAll[TaskPrefix];
                EXTERNAL=EXTERNALAll[TaskPrefix];
                If[OptionValue["UsingC"],
                    For[EvC=1,EvC<=Length[ForEvaluation],EvC++,
                        CurrentOrder=ForEvaluation[[EvC]][[1]];
                        CurrentVarDegree=ForEvaluation[[EvC]][[2]];
                        If[Union[(##[[2]])&/@ForEvaluation]=!={{0,0}},
                            RawPrint["Terms of "<>ToString[OptionValue["EpVar"]]<>" order ",CurrentOrder," and ",
                                If[OptionValue["RegVar"] =!= None, OptionValue["RegVar"], OptionValue["ExpandVar"]]," order ",ToString[CurrentVarDegree,InputForm],": "],
                            RawPrint["Terms of order ",CurrentOrder,": "]
                        ];
                        If[OptionValue["SeparateTerms"],
                            RawPrint[terms[TaskPrefix,CurrentOrder,CurrentVarDegree]]
                        ,
                            RawPrint[terms0[TaskPrefix,CurrentOrder,CurrentVarDegree]]
                        ];
                        RawPrintLn[", max vars: ",nov[TaskPrefix,CurrentOrder,CurrentVarDegree]];
                        estimateMode = OptionValue["BalanceSamplingPoints"];
                        While[True,
                            If[estimateMode,
                                RawPrint["Estimating"];
                            ,
                                RawPrint["Integrating"];
                            ];
                            RawPrintLn[MyTimingForm[AbsoluteTiming[
                                i=0;
                                While[i<10,
                                    While[And[i<10,FileExistsQ[ActualDataPath[options]<>"out.kch-"<>TaskPrefix<>"-"<>ToString[CurrentOrder]<>"-"<>StringReplace[ToString[CurrentVarDegree,InputForm], "/" -> "|"]<>"-"<>If[estimateMode,"e",""]<>ToString[i]]],
                                        DeleteFile[ActualDataPath[options]<>"out.kch-"<>TaskPrefix<>"-"<>ToString[CurrentOrder]<>"-"<>StringReplace[ToString[CurrentVarDegree,InputForm], "/" -> "|"]<>"-"<>If[estimateMode,"e",""]<>ToString[i]];
                                        RawPrint["."];
                                        i++;
                                    ];
                                    If[i==10,Break[]];
                                    Parallel`Developer`QueueRun[];
                                    If[Head[task[[4]]]===Parallel`Developer`finished,
                                        Break[];
                                    ];
                                    Pause[1];
                                ];
                            ][[1]]]," seconds."];
                            fileName = ActualDataPath[options]<>"out.kch-"<>TaskPrefix<>"-"<>ToString[CurrentOrder]<>"-"<>StringReplace[ToString[CurrentVarDegree,InputForm], "/" -> "|"]<>"-"<>If[estimateMode,"e",""]<>"R";
                            While[True,
                                If[FileExistsQ[fileName],
                                    Break[];
                                ];
                                Parallel`Developer`QueueRun[];
                                If[Head[task[[4]]]===Parallel`Developer`finished,
                                    Break[];
                                ];
                                Pause[1];
                            ];
                            If[Head[task[[4]]]===Parallel`Developer`finished,
                                If[task[[4]][[1]]=!={},
                                    Print["CIntegratePool produced output. It means that an error was encountered. The received output follows:"];
                                    Print[task[[4]][[1]]];
                                    Abort[];
                                ];
                            ];
                            temp=ReadList[fileName, String];
                            DeleteFile[fileName];
                            If[estimateMode,
                                estimateMode = False;
                            ,
                                Break[];
                            ];
                        ];
                        If[Length[temp]=!=1,
                            Print["Incorrect result!"];
                            Abort[];
                        ];
                        temp=temp[[1]];
                        temp=StringReplace[temp,{"e"->"*10^"}];
                        temp=ToExpression[temp];
                        result3=temp;
                        temp=DoubleCutExtraDigits[result3,options];
                        RawPrintLn["Returned answer: ",ToString[temp[[1]]+I*temp[[3]],InputForm]," + "<>ToString[OptionValue["PMVar"]]<>" * ",ToString[temp[[2]]+I*temp[[4]],InputForm]];
                        QPutWrapper[TaskPrefix, "out",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-R",ToStringInputForm[result3]];
                        QPutWrapper[TaskPrefix, "out",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-E",ToStringInputForm[False]];
                        QPutWrapper[TaskPrefix, "out","MaxEvaluatedOrder",ToStringInputForm[CurrentOrder]];
                        result3=InternalGenerateAnswer[TaskPrefix, False, options];
                        For[j=1,j<=Length[result3],j++,
                            RawPrint["(",ToString[result3[[j]][[1]],InputForm],")*",ToString[result3[[j]][[2]],InputForm]];
                            If[j<Length[result3],RawPrint["+"],RawPrintLn[""]];
                        ];
                    ]; (*for EvC*)
                ,   (*UsingC - not UsingC*)
                    EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp,result,iii},
                        temp=value;
                        If[temp==={},Return[{0,0,True}]];
                        temp=IntegrateHere[{temp},CurrentIntegralExact,options];
                        If[temp[[3]]>0,CurrentIntegralExact=False];
                        temp
                    ];
                    WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp},
                        If[value[[3]]>0,CurrentIntegralExact=False];
                        result3+={value[[1]],Power[value[[2]],2]};
                    ];
                    ReadFunction[s_,prefix_,runS_]:=Module[{temp,i,res},
                        res={};
                        temp=ToExpression[QGetWrapper[prefix, s,ToString[runS]]];
                        For[i=1,i<=temp,i++,
                            AppendTo[res,ToExpression[QGetWrapper[prefix, s,ToString[runS]<>"-"<>ToString[i]]]]
                        ];
                        res
                    ];
                    If[OptionValue["NumberOfSubkernels"]>0,
                        DistributeDefinitions[EvalFunction,CurrentIntegralExact];
                    ];
                    For[EvC=1,EvC<=Length[ForEvaluation],EvC++,
                        CurrentOrder=ForEvaluation[[EvC]][[1]];
                        CurrentVarDegree=ForEvaluation[[EvC]][[2]];
                        If[Union[(##[[2]])&/@ForEvaluation]=!={{0,0}},
                            RawPrint["Terms of "<>ToString[OptionValue["EpVar"]]<>" order ",CurrentOrder," and ",
                                If[OptionValue["RegVar"] =!= None, OptionValue["RegVar"], OptionValue["ExpandVar"]]," order ",ToString[CurrentVarDegree,InputForm],": "],
                            RawPrint["Terms of order ",CurrentOrder,": "]
                        ];
                        terms=ToExpression[QGetWrapper[TaskPrefix, "in",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-T"]];
                        terms0=ToExpression[QSafeGetWrapper[TaskPrefix, "in",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-T0",terms]];
                        nov=ToExpression[QGetWrapper[TaskPrefix, "in",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-N"]];
                        RawPrintLn[terms,", max vars: ",nov];
                        RawPrint["Integrating"];
                        CurrentIntegralExact=(CurrentOrder<=OptionValue["ExactIntegrationOrder"]);
                        DistributeDefinitions[CurrentIntegralExact];
                        result3=0;
                        PerformStage["in",SCounter,"in",TaskPrefix<>"-"<>ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm],False,options];
                            (* we do not really write in that database, but in is here to avoid reoping*)
                        result3={result3[[1]],Power[result3[[2]],1/2],0,0};
                        If[CurrentIntegralExact,
                            temp=FullSimplify[result3];
                        ,
                            temp=DoubleCutExtraDigits[result3,options];
                        ];
                        RawPrintLn["Returned answer: ",ToString[temp[[1]],InputForm]," + "<>ToString[OptionValue["PMVar"]]<>" * ",ToString[temp[[2]],InputForm]];
                        QPutWrapper[TaskPrefix, "out",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-R",ToStringInputForm[result3]];
                        QPutWrapper[TaskPrefix, "out",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-E",ToStringInputForm[CurrentIntegralExact]];
                        QPutWrapper[TaskPrefix, "out","MaxEvaluatedOrder",ToStringInputForm[CurrentOrder]];
                        result3=InternalGenerateAnswer[TaskPrefix, False, options];
                        For[j=1,j<=Length[result3],j++,
                            RawPrint["(",ToString[result3[[j]][[1]],InputForm],")*",ToString[result3[[j]][[2]],InputForm]];
                            If[j<Length[result3],RawPrint["+"],RawPrintLn[""]];
                        ];
                    ]; (*for EvC*)
                ]; (*not using C*)
            ][[1]]];
            If[Length[TaskPrefixes]>1,RawPrintLn["Task integration time: ",stagetime]];
        ]; (*big for*)

        If[OptionValue["UsingC"],
            If[OptionValue["NumberOfSubkernels"] =!= 0,res=WaitAll[task], res = {}];
            If[Not[res==={}],
                Print["CIntegratePool evaluation failed"];
                Print/@res;
                Abort[];
            ];
        ,
            QClose[ActualDataPath[options]<>"in"];
        ];
        result3=(InternalGenerateAnswer[ToString[##], True, options])&/@TaskPrefixes;
        QClose[ActualDataPath[options]<>"out"];
        result3=Apply[Times,result3,{2}]; (*multiplying coeff and ext var deg*)
        result3=Apply[Plus,result3,{1}];  (*summing all up*)
    ][[1]]];
    RawPrintLn["Total integration time: ",time];
    result3
];

SDPrepareAndIntegrate::usage = "SDPrepareAndIntegrate[TaskPrefix, intvars, ZZ, {runorder, expandOrder}, deltas, SHIFT, EXTERNAL, ExternalExponent, options] is the main preparation and integration routine called in the end by any SDEvaluate family function. Calls SDPrepare and then SDIntegrate";
SDPrepareAndIntegrate[TaskPrefix_, intvars_,ZZ_,{runorder_Integer,expandOrder_},deltas_,SHIFT_,EXTERNAL_,ExternalExponent_,options:OptionsPattern[FIESTA]]:=Module[{temp,timecounter,command,ep},
    ep = OptionValue["EpVar"];
    (* the databases and kernels were opened earlier by InitializeLinks[]*)
    temp=Series[EXTERNAL,{ep,0,runorder-SHIFT}];
    If[Not[TrueQ[And[NumberQ[Normal[temp]],SHIFT==0]]],
        If[Not[temp[[4]]===-SHIFT],
            Print["Incorrect order shift"];
            Print[EXTERNAL];
            Print[temp];
            Print[Normal[temp]];
            Print[temp[[4]]];
            Print[-SHIFT];
            Abort[];
        ];
    ];

    timecounter=AbsoluteTime[];
    temp=SDPrepare[TaskPrefix,intvars,ZZ,{runorder,expandOrder},deltas,SHIFT,EXTERNAL,ExternalExponent,options];

    If[Head[expandOrder] === List,Return[{temp}]]; (*DMode {temp} because SDPrepareAndIntegrate returns first *)

    RawPrintLn["Database ready for integration."];
    (* both databases are closed, database out is empty *)
    If[OptionValue["OnlyPrepare"] =!= False,
        If[OptionValue["OnlyPrepare"],
            If[OptionValue["UsingC"],
                command=
                    OptionValue["FIESTAPath"]<>"/bin/CIntegratePool --in "<>
                    ActualDataPath[options]<>
                    "in --threads "<>
                    ToString[OptionValue["NumberOfLinks"]]<>
                    CommandOptions[options];
                If[OptionValue["RegVar"] =!= None,
                    command = command <>" --expandVar "<>ToString[OptionValue["RegVar"],InputForm];
                ];
                If[NumberQ[expandOrder],
                    command = command <>" --expandVar "<>ToString[OptionValue["ExpandVar"],InputForm];
                ];
                RawPrintLn[command];
            ];
        ];
        RawPrintLn["Total time used: ",CutExtraDigits[AbsoluteTime[]-timecounter,options]," seconds."];
        Return[{"Prepared"}];
    ,
        temp=SDIntegrate[options];  (* it will open and close databases in and out*)
    ];
    RawPrintLn["Total time used: ",CutExtraDigits[AbsoluteTime[]-timecounter,options]," seconds."];
    If[OptionValue["RemoveDatabases"],
        DeleteFile[ActualDataPath[options]<>"in.kch"];
        DeleteFile[ActualDataPath[options]<>"out.kch"];
    ];
    temp
]

FindMaxLa::usage = "FindMaxLa[f, shifts, n, options] finds maximal possible lambda for contour deformation for f and shift coefficients according to the rule that the cubit terms should not dominate linear";
FindMaxLa[f_,shifts_,n_, options:OptionsPattern[FIESTA]] := Module[{la, la2, ff, dff, df, dfv, newvarsmult, newvars, rule, rules, coeffs, temp, i, min, b, id, values},
    min = OptionValue["ContourShiftShape"];
    df = D[f, OptionValue["XVar"][##]] &/@ Range[n];
    newvarsmult = (1 - I*la*shifts[[##]]*(1 - OptionValue["XVar"][##])*df[[##]]) & /@ Range[n];
    newvars = newvarsmult*Array[OptionValue["XVar"],n];
    rules = Apply[Rule, Transpose[{Array[OptionValue["XVar"], n], newvars}], {1}];
    ff = f /. rules;
    ff = {Coefficient[ff, la, 1], Coefficient[ff, la, 3]};
    ff = Compile[Evaluate[Array[OptionValue["XVar"],n]], Evaluate[##]] &/@ ff;
    For[i = 1, i <= OptionValue["LambdaIterations"], i++,
        values = Table[Rationalize[RandomReal[1]], {n}];
        temp = (## @@ values) &/@ ff;
        temp = Chop[temp];
        coeffs=Norm /@ (temp//. {Complex[a_, b_] -> b});
        If[coeffs[[2]]<=0.00001,
            coeffs=Infinity,
            coeffs=Sqrt[coeffs[[1]]/coeffs[[2]]]
        ];
        min=Min[min,coeffs];
    ];
    Rationalize[min,0.00001]
]

FindBestLa::usage = "FindBestLa[f, shifts, n, valuemax, options] while already having a maximal lambda shift splits it and tries to find maximal lambda so that the imaginary part of F remains negative and the norm of f does not decrease to much";
FindBestLa[f_,shifts_,n_,valuemax_, options:OptionsPattern[FIESTA]] := Module[{values, df, dfv, ff, la, newvarsmult, newvars, rule, temp, i, mins, curva, coeffs, max, temp2, la2, la3, real, im, temprealV, tempimV, ff2, rules, j},
    mins=Table[Infinity,{OptionValue["LambdaSplit"]}];
    df = D[f, OptionValue["XVar"][##]] &/@ Range[n];
    newvarsmult = (1 - I*la*shifts[[##]]*(1 - OptionValue["XVar"][##])*df[[##]]) & /@ Range[n];
    newvars = newvarsmult*Array[OptionValue["XVar"],n];
    rules = Apply[Rule, Transpose[{Array[OptionValue["XVar"], n], newvars}], {1}];
    ff = f /. rules;
    ff = Compile[Evaluate[Append[Array[OptionValue["XVar"],n],la]], Evaluate[ff]];
    For[j=1,j<=OptionValue["LambdaIterations"],j++,
        values = Table[Rationalize[RandomReal[1]], {n}];
        For[i=1,i<=OptionValue["LambdaSplit"],i++,
            If[mins[[i]]===0,Continue[]];
            curva=valuemax*i/OptionValue["LambdaSplit"];
            temp2 = ff @@ Append[values, curva];
            {temprealV,tempimV} = {Re[temp2], Im[temp2]};
            If[TrueQ[tempimV<0],
                mins[[i]]=Min[mins[[i]],tempimV^2+temprealV^2];
            ,
                mins[[i]]=0;
            ];
        ];
        If[Max@@mins ===0,
            Break[];
        ];
    ];

    max=Max@@mins;

    If[max===0,
        Print["Can't find lambda (contour transformation)!"];
        If[OptionValue["ContourShiftIgnoreFail"],
            Return[0];
        ,
            Print[InputForm[f]];
            Print[shifts];
            Print[n];
            Print[valuemax];
            Abort[];
        ];
    ];

    Return[Position[mins,max][[1]][[1]]*valuemax/OptionValue["LambdaSplit"]];
]

FindMaxVariableShifts::usage = "FindMaxVariableShifts[shifts, n, options] with shifts being additions to variables for contour transformation estimates maximal value of those shifts in order to balance contour transformation for different variables";
FindMaxVariableShifts[shifts_,n_, options:OptionsPattern[FIESTA]]:=Module[{temp,max,i,j},
    max=Table[0,{n}];
    For[j=1,j<=OptionValue["LambdaIterations"],j++,
        temp=shifts/. Apply[Rule, Transpose[{Array[OptionValue["XVar"], n], Table[Rationalize[RandomReal[1]], {n}]}], {1}];
        For[i=1,i<=n,i++,
            max[[i]]=Max[max[[i]],Abs[temp[[i]]]];
        ]
    ];
    Return[max];
];

TransformContour::usage = "TransformContour[expr, options] performs contour transormation in physical region trying to find best lambda shift";
TransformContour[expr2_, options:OptionsPattern[FIESTA]] := Module[{expr=expr2,sign,temp, values, vars, pow, min, n, m, pos, F, F2, temp2, newvarsmult, newvars, rules, la,lavalue,lavaluemax,shifts, function, i, F0, F1, F00, F11, notNeeded, j, k, bads, y},
    n = Length[expr[[1]]];
    F = expr[[6]];

    If[TrueQ[NumberQ[F]],
        If[F<0,
            Print["WARNING: F is completely negative! Consider global sign change! Answers can be incorrect!"];
        ];
        Return[expr]
    ];
    shifts=((1 - OptionValue["XVar"][##])*OptionValue["XVar"][##]*D[F, OptionValue["XVar"][##]]) & /@ Range[n];
    shifts=FindMaxVariableShifts[shifts,n,options];
    shifts=Rationalize[##,0.00001]&/@shifts;
    If[OptionValue["MinimizeContourTransformation"],
        vars = Cases[Variables[F], OptionValue["XVar"][_]];
        vars = Sort[vars];
        m = Length[vars];
        values = (F /. Rule @@@ Transpose[{vars, ##}]) & /@ Tuples[{0, 1}, m];
        bads = {};
        For[j = m, j >= 1, --j,
            notNeeded = True;
            For[i = 1, i <= Length[values], ++i,
                pow = Power[2, m - j];
                If[Mod[i - 1, 2*pow] < pow,
                    F00 = values[[i]];
                    F11 = values[[i + pow]];
                    If[Sign[F00] * Sign[F11] == -1,
                        notNeeded = False;
                        Break[];
                    ];
                ];
            ];
            If[notNeeded,
                shifts[[vars[[j,1]]]] = 0;
            ];
        ];
    ];

    (* inserting ImaginaryShiftVariable so that Mathematica does not turn Log[-1] to Pi*I since we need -Pi*I *)
    function = PowerExpand[expr[[4]]];
    If[Head[function] === Times,
        function = List @@ function
    ,
        function = {function}
    ];
    function = If[And[Head[##] === Power, Length[NegTerms[##[[1]], options]] > 0],
        sign = Sign[##[[1]] /. OptionValue["XVar"][_]->0];
        If[sign === -1,
            Power[##[[1]] - ImaginaryShiftVariable, ##[[2]]],
            ##
        ],
        ##
    ]&/@function;
    function = Times@@function;

    If[Plus@@shifts === 0,
        Return[ReplacePart[expr, 4->function]];
    ];
    (*min = Min @@ (DeleteCases[shifts,0]);*)
    shifts=If[##===0,
        0,
        1/##
    ]&/@shifts;
    shifts = Times@@@ Transpose[{shifts, expr[[1]]}];
    newvarsmult = (1 - I * la * shifts[[##]] * (1 - OptionValue["XVar"][##])*D[F, OptionValue["XVar"][##]]) & /@ Range[n];
    newvars= newvarsmult*Array[OptionValue["XVar"],n];
    rules = Apply[Rule, Transpose[{Array[y, n], newvars}], {1}];
    F2=Replace[F, OptionValue["XVar"] -> y, {0, Infinity}, Heads -> True]//.rules;
    If[OptionValue["FixedContourShift"] =!= 0,
        lavalue = OptionValue["FixedContourShift"];
    ,
        lavaluemax=FindMaxLa[F,shifts,n,options];
        If[lavaluemax == 0,
            Print["Can't find max la (contour transformation)"];
            If[OptionValue["ContourShiftIgnoreFail"],
                lavalue = 0;
            ,
                Print[F];
                Print[shifts];
                Print[n];
                Abort[];
            ];
        ,
            lavalue=FindBestLa[F,shifts,n,lavaluemax,options];
            lavalue = lavalue * OptionValue["ContourShiftCoefficient"];
        ];
    ];
    temp={
        expr[[1]],
        expr[[2]],
        expr[[3]],
        (Replace[function, OptionValue["XVar"] -> y, {0, Infinity}, Heads -> True] //. rules)*Det[Outer[D[#1, #2] &, newvars, Array[OptionValue["XVar"], n]]]*Inner[Power,newvarsmult,expr[[2]],Times],
        expr[[5]],
        F2,
        Replace[expr[[7]], OptionValue["XVar"] -> y, {0, Infinity}, Heads -> True] //.rules
    };
    temp = temp /. la -> lavalue;
    temp
]

TakeResidues::usage = "TakeResidues[xx,options] is used in SDExpand calling RequiredResudues and then taking them all";
TakeResidues[xx_,options:OptionsPattern[FIESTA]]:=Module[{temp,num,temp1,temp0,temp2,ReqR,res1,garg,garg1,garg2},
    temp=xx;
    ReqR=RequiredResidues[temp[[4,1]],temp[[4,2]],temp[[4,3]],options];

    temp2=Flatten[
        MyResidue[
            {temp[[1]],temp[[2]]},
            ##[[2]]*temp[[3]],
            ##[[1]],
            ##[[3]]
        ]&/@ReqR
    ,
        1
    ];

    temp2=Select[temp2,(##[[3]]=!=0)&];

    temp2=temp2/. Gamma[garg_] :> Gamma[Expand[garg]];
    temp2=temp2/. Power[garg1_,garg2_] :> Power[Expand[garg1],Expand[garg2]];
    temp2
]

LocateBadEnds::usage = "LocateBadEnds[F, var] finds variables responsible for turning F to 0 when they are set to 1";
LocateBadEnds[F_, var_] := Module[{temp, vars, m, values, bads, j, pow, F00, F11},
    vars = Cases[Variables[F], var[_]];
    vars = Sort[vars];
    m = Length[vars];
    values = (F /. Rule @@@ Transpose[{vars, ##}]) & /@ Tuples[{0, 1}, m];
    bads = {};
    For[j = m, j >= 1, --j,
        For[i = 1, i <= Length[values], ++i,
            pow = Power[2, m - j];
            If[Mod[i - 1, 2*pow] < pow,
                F00 = values[[i]];
                F11 = values[[i + pow]];
                If[And[F11 == 0, F00 =!= 0],
                    AppendTo[bads, vars[[j]]];
                    Break[];
                ];
            ];
        ];
    ];
    Sort[bads]
]

PolynomialOrderings::usage = "PolynomialOrderings[pn, vs_List, n_Integer] finds n first orderings sending the polynomial pn into its canonical form";
PolynomialOrderings[pn_, vs_List:{}, n_Integer:-1] := Module[
    {vt, crs, gcd, cmx, cns, cas, cps, cvs, ord, max, ca},
    (* check: variables *)
    vt = vs;
    If[vt === {}, vt = Variables[pn]];
    (* -- (1) -- *)
    (* polynomial -> coefficient rules *)
    crs = CoefficientRules[pn, vt];
    (* possible common factor *)
    If[crs === {},
        gcd = 1;
    ,
        gcd = PolynomialGCD @@ (Last /@ crs);
    ];
    (* rules -> matrix of exponents, coefficients *)
    cmx = Append[First[#], Simplify[Last[#]/gcd]] & /@ crs;
    (* operate on the transposed *)
    If[cmx === {},
        Return[{Range[Length[vs]]}];
    ];
    cmx = Transpose[Sort[cmx]];
    (* -- (2) -- *)
    (* initialize list of column numbers, permutations *)
    cns = Range[Length[vt]];
    cas = {{}};
    (* iterate until all variables ordered *)
    While[
        Length[First[cas]] < Length[vt],
        (* -- (3) -- *)
        (* extended permutations *)
        cps = Join @@ (Function[ca, Append[ca, #] & /@ Complement[cns, ca]] /@ cas);
        (* -- (4) -- *)
        (* candidate vectors *)
        cvs = (cmx[[Prepend[#, -1]]]  (* coefficients, swap rows *)
            // Transpose           (* -> columns *)
            // Sort                (* sort rows *)
            // Transpose           (* -> columns *)
            // Last) & /@ cps;     (* extract vector *)
        (* -- (5) -- *)
        (* lexicographical ordering *)
        ord = Ordering[cvs];
        (* maximum vector *)
        max = cvs[[Last[ord]]];
        (* -- (6) -- *)
        (* select (maximum number of) candidate permutations *)
        cas = Part[cps, Select[ord, cvs[[#]] === max & ]];
        cas = If[n >= 0 && n < Length[cas], Take[cas, n], cas]
    ];
    (* -- (7) -- *)
    (* result: canonical orderings *)
    cas
];

BisectSector::usage = "BisectSector[value, var] uses the form of value after sector decomposition and bisects the variables that are responsible for getting F=0 when set to 1";
BisectSector[value_, var_]:=Module[{temp, num, bads, bad, bisectionPoint, rule1, rule2, bisectionCount = 1},
    ClearSystemCache[];
    SeedRandom[0];
    temp=value;
    Check[
        bads = LocateBadEnds[temp[[6]], var];
        bads = First/@bads;
        bads = Union[bads];
        temp = {temp}; (* starting bisection *)
        While[bads != {},
            bad = bads[[1]];
            ++bisectionCount;
            While[Not[PrimeQ[bisectionCount]], ++bisectionCount];
            (*bisectionPoint = 1/bisectionCount;*)
            bisectionPoint = (RandomInteger[512] + 256)/1024;
            (*bisectionPoint = (RandomInteger[64] + 32)/128;*)
            bads = Drop[bads,1];
            rule1 = {var[bad] -> var[bad] * bisectionPoint};
            rule2 = {var[bad] -> 1 - var[bad] * (1 - bisectionPoint)};
            If[##[[3,bad]] =!= 0,
                Print["Cannot split sector with log indices"];
                Abort[];
            ]& temp;
            temp = {
                {
                    ##[[1]],
                    ##[[2]],
                    ##[[3]],
                    bisectionPoint * (bisectionPoint)^##[[2,bad]] * (##[[4]])/.rule1,
                    ##[[5]],
                    ##[[6]]/.rule1,
                    ##[[7]]
                }
            ,
                {
                    ##[[1]],
                    ReplacePart[##[[2]], bad->0],
                    ##[[3]],
                    (1 - bisectionPoint) * (var[bad]^##[[2,bad]] * ##[[4]])/.rule2,
                    ##[[5]],
                    ##[[6]]/.rule2,
                    ##[[7]]
                }
            }&/@temp;
            temp = Flatten[temp, 1];
        ];
    ,
        Print[InputForm[temp]];
        Abort[];
    ];
    temp
];

AdditionalSectorDecomposition::usage = "AdditionalSectorDecomposition[value, options] uses the form of value after sector decomposition and performs a sector decomposition again after the sector was bisected since the singularities from x[i]->1 are moved to 0 and might be unresolved";
AdditionalSectorDecomposition[value_,options:OptionsPattern[FIESTA]] := Module[{temp=value, num, sd, reps, rule, Jac, n, func, pre, y},
    ClearSystemCache[];
    n = Length[Position[temp[[1]],1]];
    If[NumberQ[temp[[6]]],
        Return[{temp}];
    ];
    sd = SectorDecomposition[{}, {Expand[temp[[6]]]}, Cases[temp[[1]], 1], Length[temp[[1]]], 0, 0, options][[1]];
    If[Length[sd] <= 1,
        Return[{temp}];
    ];
    temp = (
        reps=Inner[Power[#2, #1] &, Transpose[##], Array[y,Length[temp[[1]]]],Times];
        rule=Apply[Rule,Transpose[{Array[OptionValue["XVar"],Length[temp[[1]]]],reps}],{1}];
        Jac=Det[Outer[D,reps,Array[y,Length[temp[[1]]]]]];
        func = (Jac * temp[[4]]) //. rule;
        func = FactorMonomNew[PowerExpand[func], y];
        pre = Inner[Power, OptionValue["XVar"] /@ First/@Position[temp[[1]], 1], Delete[temp[[2]], Position[temp[[1]], 0]],Times] /. rule;
        {
            Join[Table[1, {n}], Table[0, {Length[temp[[1]]] - n}]],
            Expand[(Exponent[pre,##]&/@Array[y,Length[temp[[2]]]]) + (Exponent[func[[1]], ##]&/@Array[y,Length[temp[[2]]]])],
            temp[[3]],
            func[[2]] /. {y->OptionValue["XVar"]},
            temp[[5]],
            FactorMonomNew[PowerExpand[temp[[6]]/.rule], y][[2]] /. {y->OptionValue["XVar"]},
            temp[[7]]
        }
    ) &/@ sd;
    temp
];

SDPrepare::usage = "SDPrepare[TaskPrefix, intvars, ZZ, {runorder, expandOrder}, deltas, SHIFT, EXTERNAL, ExternalExponent, options] is the main preparation and integration routine called in the end by any SDEvaluate function. Goes through sll the algebraic stages starting from sector decomposition";
SDPrepare[TaskPrefix_,intvars_,ZZ_,{runorder_Integer,expandOrder_},deltas_,SHIFT_,EXTERNAL_,ExternalExponent_,options:OptionsPattern[FIESTA]]:=Module[{Z,U,F,SD,f,forsd,ii,i,vars,n,m,j,l,rule,Z3,Z2,U2,F2,coeff,k,Jac,res,result,md,result2,pol,active,zsets,SDCoeffs,timecounter,HasExtra,ForEvaluation,temp,BisectionPointsHere,afterSubst,can,op,groups,dbin,dbout,dbtemp,expr,min,sector,ser,terms,terms0,runS,run1,s,prefix,CurrentOrder,CurrentVarDegree,CurrentTaskPrefix,TaskPrefixes,OldTaskPrefix,Terms,TermsCount,OldTermsCount,SCounter,MixSectorCounter,PSector,d,EvC,value,NumberOfVariables,ep,y,serC},
    ep = OptionValue["EpVar"];
    (* expandOrder can be None for Evaluate modes, some number for SDExpand (not Asy) or a pair of numbers for SDAnalyze *)

    If[Length[intvars]==0,
        (*temp=Plus @@ ((##[[1]]) & /@ (Flatten[(##[[1]]) & /@ ZZ, 1]));*)
        temp = Transpose[{##[[1]], Table[##[[2]], {Length[##[[1]]]}]}] & /@ ZZ;
        temp = Flatten[temp, 1];
        temp = Apply[Append, temp , {1}];
        temp = {##[[1]], Inner[Power, ##[[3]], ##[[2]], Times]} & /@ temp;
        temp = Apply[Times, temp, {1}];
        temp = Plus @@ temp;
        temp*=EXTERNAL;
        InitializeQLink[{"in"},options];
        TaskPrefixes=ToExpression[QSafeGetWrapper["0", "in","","{}"]];
        QPutWrapper["0", "in","",ToString[Append[TaskPrefixes,TaskPrefix]]];
        QPutWrapper[TaskPrefix, "in","Exact",ToStringInputForm[True]];
        QPutWrapper[TaskPrefix, "in","ExactValue",ToStringInputForm[temp]];
        QPutWrapper[TaskPrefix, "in","UsingC",ToStringInputForm[OptionValue["UsingC"]]];
        QPutWrapper[TaskPrefix, "in","ExternalExponent",ToStringInputForm[ExternalExponent]];
        QClose[ActualDataPath[options]<>"in"];
        Return[];
    ];

    InitializeQLink[{"in","1","2"},options];
    TaskPrefixes=ToExpression[QSafeGetWrapper["0", "in","","{}"]];
    If[MemberQ[TaskPrefixes,TaskPrefix],
        Print["Task results with this prefix already exist in database!"];
        Abort[];
    ];
    QPutWrapper["0", "in","RequestedOrders",ToStringInputForm[{runorder-SHIFT,expandOrder}]];
    QClose[ActualDataPath[options]<>"in"];

    MixSectorCounter=-1;

    n=Length[intvars];

    If[OptionValue["NumberOfSubkernels"]>0,
        DistributeDefinitions[OptionValue["XVar"]];
    ];
    Terms=0;

    ClearDatabase["1", True, options];
    run1=1;

    For[l=1,l<=Length[ZZ],l++,
        {Z,U}=ZZ[[l]];
        If[Times@@U===0,Continue[]];
        vars=Apply[OptionValue["XVar"], Position[intvars, 1], {1}];
        If[Length[vars]>0,
            zsets=Tuples[deltas];
            (* {deltas, U, intvars, n, primary number, Z} *)
            SD=Transpose[{zsets,Table[U,{Length[zsets]}],Table[intvars,{Length[zsets]}],Table[n,{Length[zsets]}],Range[Length[zsets]],Table[Z,{Length[zsets]}]}]/.Log[_]->1;
            If[OptionValue["PrimarySectorCoefficients"] =!= Automatic,
                SDCoeffs = OptionValue["PrimarySectorCoefficients"],
                SDCoeffs = Table[1,{Length[SD]}]
            ];

            SD=Delete[SD,Position[SDCoeffs,0]];
            If[And[OptionValue["SectorSymmetries"], OptionValue["PrimarySectorCoefficients"] === Automatic, Length[Z] === 1],
                temp = ((##[[2]] * Last[##][[1,1]])/. (Rule[OptionValue["XVar"][##],1] &/@ ##[[1]])) &/@SD;
                temp = (
                    op = PolynomialOrderings[Plus@@##,Array[OptionValue["XVar"],n],1];
                    (Times@@##) //. Rule @@@ Transpose[Append[Map[OptionValue["XVar"], op, {2}], Array[y, n]]] /. y -> OptionValue["XVar"]
                ) &/@ temp;
                groups = Last /@ Reap[Sow @@@ Transpose[{Range[Length[temp]], temp}], _, f][[2]];
                RawPrintLn["Using symmetries of primary sectors ", groups];
                If[Length[##] > 1,
                    temp = Length[##];
                    SD[[##[[1]]]] = ReplacePart[SD[[##[[1]]]], 6 -> ({##[[1]] * temp, ##[[2]]} &/@SD[[##[[1]], 6]])];
                    (SD[[##]] = Null) &/@ Drop[##, 1];
                ] &/@ groups;
                SD = DeleteCases[SD, Null];
            ];
            RawPrintLn["Sector decomposition - ",Length[SD]," sectors"];
            RawPrintLn["Totally: ",MyTimingForm[AbsoluteTiming[
                If[OptionValue["NumberOfSubkernels"]>0,
                    SetSharedFunction[RawPrintLn];
                ];
                SD = Join[##, {options}] &/@ SD;
                SD=MyParallelize[(SectorDecomposition@@##)&/@SD,options];
                If[OptionValue["NumberOfSubkernels"]>0,
                    UnsetShared[RawPrintLn];
                ];
                ][[1]]]," seconds; ",Total[Length/@First/@SD]," sectors."
            ];
            (* {matrices, {deltas, U, intvars, n, primary number, Z} }*)

            If[OptionValue["DebugAllEntries"],Print["Sector decomposition"];
                If[OptionValue["DebugSector"]>0,
                    Print[InputForm[Flatten[(temp = ##[[2]]; {##, temp} & /@ ##[[1]]) & /@ SD, 1][[OptionValue["DebugSector"]]]]],
                    Print[InputForm[SD]]
                ];
            ];

            RawPrint["Preparing database: "];
            RawPrintLn[MyTimingForm[AbsoluteTiming[
                For[i=1,i<=Length[SD],i++,
                    For[j=1,j<=Length[SD[[i]][[1]]],j++,
                        QPutWrapper[TaskPrefix, "1",ToString[run1,InputForm],ToStringInputForm[{SD[[i]][[1]][[j]],SDCoeffs[[SD[[i]][[2]][[5]]]],SD[[i]][[2]][[1]],SD[[i]][[2]][[6]],SD[[i]][[2]][[2]],SD[[i]][[2]][[3]],SD[[i]][[2]][[4]],deltas}]];run1++;
                    ];
                ];
            ][[1]]]," seconds. "];
        ,
            QPutWrapper[TaskPrefix, "1",ToString[run1,InputForm],ToStringInputForm[{{},{},{},Times@@U,0}]];run1++;
        ];

    ];
    Terms=run1-1;

    RawPrint["Variable substitution"];
    SCounter=1;

    dbin="1";
    dbout="2";
    EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp,ThisSD,rules,reps,rule,Jac,U2,Z2,Z3,U3,NewDegrees,power,powers,i,deltass,vars,op,aaa,y},
        (
        ClearSystemCache[];
        ThisSD=ToExpression[value];
        (*Print[ThisSD];*)
        (* 1=matrix, 2= coeff, 3 =zset, 4 = Z = coeff and powers, 5 = functions, 6 = allvars, 7 = n, 8 = deltas *)
        (* if we want SDEvaluateDirect to work properly with deltas,
            we have to add a transformation here
            calculate the power of variables in ThisSD[[3]] for each part of Z independently
            that is the sum of product of powers and degree of functions (we assume them to be homogeneous)
            + n (1 for delta, n-1 for jacobian)
            if this power is not equal to 0
            we introduce a new function equal to sum of variables in delta and -power
        *)
        If[Length[ThisSD]===5,Return[{ThisSD}]];
        n=ThisSD[[7]];
        If[ThisSD[[2]]===0,
            {}
        ,
            deltass = ThisSD[[8]];
            rules=Rule[OptionValue["XVar"][##],1]&/@ThisSD[[3]];
            reps=Inner[Power[#2, #1] &, Transpose[ThisSD[[1]]], Array[y,n],Times];
            rule=Apply[Rule,Transpose[{Array[OptionValue["XVar"],n],reps}],{1}];
            Jac=Det[Outer[D,reps,Array[y,n]]];
            power[a_,vars_]:=(
                temp = Expand[a];
                temp = If[Head[temp] === Plus, temp[[1]], temp];
                temp = temp /. (Rule@@@Transpose[{OptionValue["XVar"]/@(deltas[[i]]),Table[OptionValue["XVar"],{Length[deltas[[i]]]}]}]);
                Exponent[temp, OptionValue["XVar"]]
            );
            For[i=1,i<=Length[deltas],++i,
                vars=OptionValue["XVar"]/@(deltass[[i]]);
                powers = power[##,vars] & /@ ThisSD[[5]];
                powers = Expand[(Length[deltass[[i]]]+power[##[[1]],vars] + ##[[2]].powers)]&/@(ThisSD[[4]]);
               If[Or@@(Not[##===0]&/@powers),
                    (*Print["Introducing a new factor"];*)
                    AppendTo[ThisSD[[5]],Plus@@(OptionValue["XVar"]/@(deltass[[i]]))];
                    temp = Transpose[ThisSD[[4]]];
                    temp[[2]] = Transpose[temp[[2]]];
                    temp[[2]] = Append[temp[[2]],-powers];
                    temp[[2]] = Transpose[temp[[2]]];
                    ThisSD[[4]] = Transpose[temp];
                    (*Print[ThisSD];*)
                ];
            ];
            Z2=(ThisSD[[4]]/.rules)//.rule;
            U2=(ThisSD[[5]]/.rules)//.rule;
            (
                NewDegrees=Table[0,{n}];
                NewLogDegrees=Table[0,{n}];
                Z3=##;
                For[m=1,m<=Length[U2],m++,
                    temp=FactorMonom[U2[[m]] /. {0.->0},y];
                    U3[m]=temp[[2]];
                    NewDegrees=NewDegrees+Z3[[2]][[m]]*Table[Exponent[temp[[1]],y[i]],{i,n}];
                    temp=FactorLogMonom[U3[m] /. {0.->0},y];
                    U3[m]=temp[[2]];
                    NewLogDegrees=NewLogDegrees+Z3[[2]][[m]]*Table[Exponent[temp[[1]],Log[y[i]]],{i,n}];
                ];
                NewDegrees=NewDegrees+Table[Exponent[Jac,y[i]],{i,n}];
                Z3[[1]]=FactorMonom[PowerExpand[Z3[[1]]],y];
                NewDegrees=NewDegrees+Table[Exponent[Z3[[1]][[1]],y[i]],{i,n}];
                Z3[[1]]=Z3[[1]][[2]];
                If[Or[OptionValue["RegVar"] =!= None, And[expandOrder =!= None, Head[expandOrder] =!= List]],
                    temp=Exponent[Z3[[1]],If[OptionValue["RegVar"] =!= None, OptionValue["RegVar"], OptionValue["ExpandVar"]]] // Simplify ;
                    Z3[[1]]=Expand[Z3[[1]]/Power[If[OptionValue["RegVar"] =!= None, OptionValue["RegVar"], OptionValue["ExpandVar"]],temp]];
                ];
                f={
                    ReplacePart[ThisSD[[6]],0,List/@ThisSD[[3]]],
                    Expand[NewDegrees],
                    NewLogDegrees,
                    Z3[[1]]*ThisSD[[2]]*(Jac/.y[aaa_]->1)*Times@@Table[((U3[m]-If[And[m===2,OptionValue["ComplexMode"]===True],0*I/1000000,0])^(Z3[[2]][[m]])),{m,Length[U2]}],
                    0,
                    If[Length[U2]===0,0,U3[2]],
                    {Z3[[1]],{}, expandOrder}
                };
                If[NumberQ[f[[4]]],f[[4]]*=(1+1/1000000)^ep];
                If[And[expandOrder =!= None, Head[expandOrder] =!= List],
                    AppendTo[f[[1]],2];
                    AppendTo[f[[2]],temp];
                    AppendTo[f[[3]],0];
                ];
                f=f//.y->OptionValue["XVar"];
                If[OptionValue["SectorSymmetries"],
                    temp = f[[4]];
                    If[Head[temp] === Power, temp = {temp}];
                    op = PolynomialOrderings[Plus @@ First /@ Cases[temp, Power[_, _]], Array[OptionValue["XVar"], n], 1];
                    f = f //. Rule @@@ Transpose[Append[Map[OptionValue["XVar"], op, {2}], Array[y, n]]] /. y -> OptionValue["XVar"];
                    f[[1]] = Table[f[[1, op[[1, i]]]], {i, 1, n}];
                    f[[2]] = Table[f[[2, op[[1, i]]]], {i, 1, n}];
                    f[[3]] = Table[f[[3, op[[1, i]]]], {i, 1, n}];
                ];
                ToStringInputForm[f]
            )&/@Z2
        ]
        )
    ];

    WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=(
        MixSectorCounter++;
        If[MixSectorCounter>OptionValue["MixSectors"],SCounter++;MixSectorCounter=0];
        If[And[Not[OptionValue["SectorSymmetries"]], OptionValue["DebugSector"]>0, SCounter =!=OptionValue["DebugSector"]],
            QPutWrapper[prefix, d,ToString[sector,InputForm],""];
        ,
            QPutWrapper[prefix, d,ToString[sector,InputForm],StringJoin@@{"{",ToStringInputForm[SCounter],",",value,"}"}];
        ];
    );
    ReadFunction[s_,prefix_,runS_]:=QGetWrapper[prefix, s,ToString[runS]];
    DistributeDefinitions[EvalFunction];

    If[OptionValue["FixSectors"],
        Terms=PerformStage[dbin,Terms,dbout,TaskPrefix,True,options,"NumberOfSubkernels"->0];
    ,
        Terms=PerformStage[dbin,Terms,dbout,TaskPrefix,True,options];
    ];

    If[OptionValue["DebugAllEntries"],Print["Variable substitution"];PrintAllEntries[TaskPrefix, dbout,options]];

    If[OptionValue["SectorSymmetries"],
        RawPrint["Counting different sectors: "];
        RawPrint[MyTimingForm[AbsoluteTiming[
            afterSubst=ToExpression[QGetWrapper[TaskPrefix, dbout, StringDrop[##, StringPosition[##, "-"][[1, 1]]]]]&/@Sort[QList[dbout],(ToExpression[#1]<ToExpression[#2])&];
            can = (
                (Inner[Power, Array[OptionValue["XVar"], n], ##[[2, 2]], Times]*
                Inner[Power, Log /@ Array[OptionValue["XVar"], n], ##[[2, 3]], Times]*
                ##[[2, 4]])
            ) & /@ afterSubst;
            groups = Last /@ Reap[Sow @@@ Transpose[{First/@afterSubst, can}], _, f][[2]];
            If[Length[##] > 1,
                expr = ToExpression[QGetWrapper[TaskPrefix, dbout,ToString[##[[1]],InputForm]]];
                expr[[2,4]] = Length[##]*expr[[2,4]];
                QPutWrapper[TaskPrefix, dbout,ToString[##[[1]],InputForm], ToStringInputForm[expr]];
                (QPutWrapper[TaskPrefix, dbout,ToString[##,InputForm], ""]) &/@ Drop[##,1];
            ] &/@ groups;
        ][[1]]]," seconds"];
        RawPrintLn["; ",Length[groups]," terms."];
        If[OptionValue["DebugMemory"],RawPrintLn[MyMemoryInUse[options]]];
    ];
    dbtemp=dbin;dbin=dbout;dbout=dbtemp;
    (* {vars, indices, log degrees, expression, ep degree, F, {gammas,{},expandOrder}}   *)

    If[Head[expandOrder] === List,  (* DMode return possible d. Program stops here *)
        result=Flatten[ToExpression[QGetWrapper[TaskPrefix, dbin,ToString[##]]][[2]][[2]]&/@Range[Terms],1];
        result=Union[result];
        result = Join[result,First/@Cases[Numerator[EXTERNAL],Gamma[_]]];
        result=result/.{ep->(OptionValue["d0"]-d)/2};
        result=Expand/@result;
        result=Select[result,(Variables[##]=!={})&];
        result={##,Sort[{##/.d->expandOrder[[1]],##/.d->expandOrder[[2]]}]}&/@result;
        result={##[[1]],{Ceiling[##[[2]][[1]]],Min[Floor[##[[2]][[2]]],-1+SHIFT]}}&/@result;
        result=Select[result,(##[[2]][[1]]<=##[[2]][[2]])&];
        result = {##[[1]], Range @@ (##[[2]])} & /@ result;
        result={Table[##[[1]],{Length[##[[2]]]}],##[[2]]}&/@result;
        result=Transpose/@result;
        result=Flatten[result,1];
        result=((##[[2]]-(##[[1]]/.d->0))/Coefficient[##[[1]],d])&/@result;
        If[OptionValue["PolesMultiplicity"],
            result=Reap[Sow[##, ##] & /@ result, _, {#1, Length[#2]} &][[2]]
        ,
            result=Union[result];
        ];
        Return[result];
    ];

    (* {vars, indices, log degrees, expression, ep degree, F, {gammas,{},expandOrder}}   *)
    If[OptionValue["SectorSplitting"],

        OldTermsCount = 0;
        While[True,

            RawPrint["Searching for "<>ToString[OptionValue["XVar"]]<>"[i]->1 singularities"];

            EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp, num, bads, bad, bisectionPoint, rule1, rule2, bisectionCount = 1},
                temp=ToExpression[value];
                num=temp[[1]];
                If[And[OptionValue["DebugSector"]>0, num =!= OptionValue["DebugSector"]],
                    Return[{}];
                ];
                temp=temp[[2]];
                temp = BisectSector[temp, OptionValue["XVar"]];
                ToStringInputForm[{num, ##}] &/@ temp
            ];
            DistributeDefinitions[EvalFunction];
            WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=(QPutWrapper[prefix, d,ToString[sector,InputForm],value];);

            Terms=PerformStage[dbin,Terms,dbout,TaskPrefix,True,options];
            TermsCount = Terms;

            If[OptionValue["DebugAllEntries"],Print["Singularities search"];PrintAllEntries[TaskPrefix, dbout,options]];
            dbtemp=dbin;dbin=dbout;dbout=dbtemp;
            If[TermsCount === OldTermsCount,
                Break[];
            ];

            RawPrint["Additional sector decomposition"];
            (* now decomposing to get rid of singularities that came from x[i] -> 1 *)

            EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp, num, sd, reps, rule, Jac, n, func, pre},
                temp=ToExpression[value];
                num=temp[[1]];
                temp=temp[[2]];
                Check[
                    temp = AdditionalSectorDecomposition[temp, optionsEval];
                ,
                    Print[value];
                    Abort[];
                ];
                ToStringInputForm[{num, ##}] &/@ temp
            ];
            DistributeDefinitions[EvalFunction];
            WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=(QPutWrapper[prefix, d,ToString[sector,InputForm],value];);

            Terms=PerformStage[dbin,Terms,dbout,TaskPrefix,True,options];
            If[OptionValue["DebugAllEntries"],Print["Additional sector decomposition"];PrintAllEntries[TaskPrefix, dbout,options]];
            dbtemp=dbin;dbin=dbout;dbout=dbtemp;
            OldTermsCount = Terms;
        ];
    ];

    If[OptionValue["ComplexMode"],
        RawPrint["Performing contour transformation"];

        EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp,num,bad},
            ClearSystemCache[];
            SeedRandom[0];
            temp=ToExpression[value];
            num=temp[[1]];
            temp=temp[[2]];

            If[OptionValue["SectorSplitting"],
                bad=LocateBadEnds[temp[[6]], OptionValue["XVar"]];
                If[Length[bad]>0,
                    Print["Bad integration ends still found (for sector ",num,"): ",bad];
                    Throw[1];
                ];
            ];

            temp=TransformContour[temp, optionsEval];  (* here we use a formula:  x_i -> x_i - x_i (1-x_i) i la dF/dx_i             *)
            ToStringInputForm[{num,temp}]
        ];
        DistributeDefinitions[EvalFunction];
        WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=(QPutWrapper[prefix, d,ToString[sector,InputForm],value];);

        Terms=PerformStage[dbin,Terms,dbout,TaskPrefix,False,options];

        If[OptionValue["DebugAllEntries"],Print["Contour transformation"];PrintAllEntries[TaskPrefix, dbout,options]];

        dbtemp=dbin;dbin=dbout;dbout=dbtemp;

        (* {vars, indices, log degrees, expression, ep degree, F, {gammas,{},expandOrder}}   *)

    ];

    If[Or[(OptionValue["RegVar"] =!= None),expandOrder =!= None],

        RawPrint["Pole resolution"];

        EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp,num},
            ClearSystemCache[];
            temp=ToExpression[value];
            num=temp[[1]];
            temp=temp[[2]];
            If[OptionValue["RegVar"] =!= None,
                temp=ZNegExpand[temp, OptionValue["RegVar"], optionsEval];
            ,
                temp=ZNegExpand[temp, MBVar, optionsEval];
            ];
            ToStringInputForm[{num,##}]&/@temp
        ];
        DistributeDefinitions[EvalFunction];
        WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=(QPutWrapper[prefix, d,ToString[sector,InputForm],value];);

        Terms=PerformStage[dbin,Terms,dbout,TaskPrefix,True,options];

        If[OptionValue["DebugAllEntries"],Print["Pole resolution"];PrintAllEntries[TaskPrefix, dbout,options]];

        dbtemp=dbin;dbin=dbout;dbout=dbtemp;

        (* {vars, indices, log degrees, expression, ep degree, F, {gammas,{},expandOrder}}   *)
    ];

    If[Or[(OptionValue["RegVar"] =!= None),expandOrder =!= None],

        If[(OptionValue["RegVar"] =!= None),  (* normally used in SDExpandAsy *)

            RawPrint["Z expansion"];

            EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp,num},
                ClearSystemCache[];
                temp=ToExpression[value];
                num=temp[[1]];
                temp=temp[[2]];
                temp=ZPosExpand[temp, OptionValue["RegVar"]];
                ToStringInputForm[{num,##}]&/@temp
            ];
            DistributeDefinitions[EvalFunction];
            WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=(QPutWrapper[prefix, d,ToString[sector,InputForm],value];);

            Terms=PerformStage[dbin,Terms,dbout,TaskPrefix,True,options];

            If[OptionValue["DebugAllEntries"],Print["Z expansion"];PrintAllEntries[TaskPrefix, dbout,options]];

            dbtemp=dbin;dbin=dbout;dbout=dbtemp;

        ,
            (* variant of expandOrder =!= None and OptionValue["RegVar"] === None) *)
            (* normally used in SDExpand *)

            RawPrint["Taking residues"];

            EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp,num,temp1,temp0,temp2,ReqR,res1},
                ClearSystemCache[];
                temp=ToExpression[value];
                num=temp[[1]];
                temp=temp[[2]];
                temp2=TakeResidues[{temp[[2]],temp[[3]],temp[[4]],temp[[7]]},optionsEval];
                (*TakeResidues works with degrees, log degrees, expression and extra info *)
                (*vars and ep order are not changed*)
                ToStringInputForm[{num, {temp[[1]],ExpandAll[##[[1]]],ExpandAll[##[[2]]],##[[3]],temp[[5]],temp[[6]]}}]& /@ temp2
            ];
            DistributeDefinitions[EvalFunction];
            WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=(QPutWrapper[prefix, d,ToString[sector,InputForm],value];);

            Terms=PerformStage[dbin,Terms,dbout,TaskPrefix,True,options];

            If[OptionValue["DebugAllEntries"],Print["Taking residues"];PrintAllEntries[TaskPrefix, dbout,options]];

            dbtemp=dbin;dbin=dbout;dbout=dbtemp;

        ] (* variant of expandOrder =!= None and not (OptionValue["RegVar"] =!= None) ended*)
    ];  (*expandOrder =!= None or (OptionValue["RegVar"] =!= None)*)


    (* {vars, indices, log indices, expression, ep degree, F} - here the order of expandVariable wend to indices*)

    RawPrint["Pole resolution"];
    EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp,num},
        ClearSystemCache[];
        temp=ToExpression[value];
        num=temp[[1]];
        temp=temp[[2]];
        Check[
            temp=EpNegExpand[temp, optionsEval],
            Print["EpNegExpand error"];
            Print["Expr is: ",ToExpression[value][[2]]];
            Print["Num is: ", num];
            Abort[];
        ];
        temp=DeleteCases[temp,{_,_,_,0,_,_}];
        temp = AdvancedFirstVars[ConstructTerm[##, Or[expandOrder =!= None,(OptionValue["RegVar"] =!= None)], options],Last[##], OptionValue["XVar"]] &/@ temp;
        ToStringInputForm[{num,##}]&/@temp
    ];

    DistributeDefinitions[EvalFunction];
    WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=(QPutWrapper[prefix, d,ToString[sector,InputForm],value];);

    Terms=PerformStage[dbin,Terms,dbout,TaskPrefix,True,options];

    (* it was before, now we combine stages *)
    (* {vars, indices, log indices, expression, ep degree, F} *)
    (* for external variable vars, indices, log indices will be one place longer and contain its powers *)
    (* now *)
    (* {expression, {ExpandVar degree, ExpandLog degree}, ep degree, F} *)

    If[OptionValue["DebugAllEntries"],Print["Pole resolution"];PrintAllEntries[TaskPrefix, dbout,options];];

    dbtemp=dbin;dbin=dbout;dbout=dbtemp;

    RawPrint["Epsilon expansion"];

    EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp,num,FF},
        ClearSystemCache[];
        temp=ToExpression[value];
        num=temp[[1]];
        temp=temp[[2]];
        FF=temp[[4]]; (* no need to expand F here *)
        temp=EpPosExpand[temp[[1]],temp[[2]],temp[[3]],runorder,optionsEval];
        (*temp=EpPosExpand[temp[[1]] /. Power[-1, a_] :> E^(I*Pi*a),temp[[2]],temp[[3]],runorder,optionsEval];*)
        temp=ToStringInputForm[{num,{##[[2]][[1]],##[[1]],##[[2]][[2]],FF}}]&/@temp
    ];
    DistributeDefinitions[EvalFunction];
    WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=(QPutWrapper[prefix, d,ToString[sector,InputForm],value];);

    Terms=PerformStage[dbin,Terms,dbout,TaskPrefix,True,options];

    If[OptionValue["DebugAllEntries"],Print["Epsilon expansion"];PrintAllEntries[TaskPrefix, dbout,options];];

    dbtemp=dbin;dbin=dbout;dbout=dbtemp;

    (*  {expression, ep degree, ext var degrees, FF}         *)

    RawPrint["Preparing integration strings"];

    EvalFunction[value_,optionsEval:OptionsPattern[FIESTA]]:=Module[{temp,temp1,num,temp2,temp3,vars,FF,l},
        ClearSystemCache[];
        temp=ToExpression[value];
        num=temp[[1]];
        temp1=temp[[2]][[2]];temp3=temp[[2]][[3]]; (*degrees*)
        FF=temp[[2]][[4]];
        temp=temp[[2]][[1]];

        If[ZeroCheck[temp, options],
            {}
        ,
            vars=CountVars[Cases[Variables[Variables[temp]/.Log->List],OptionValue["XVar"][_]],OptionValue["XVar"]];
            If[OptionValue["AnalyzeWorstPower"],
                temp2=MyWorstPower[temp,vars[[1]], options];
                temp2=Ceiling[Abs[##]]&/@temp2;
            ,
                temp2 = 0&/@(vars[[1]]);
            ];
            {{
                num,
                If[vars[[2]]<=0, (*it can be -Infinity *)
                    If[OptionValue["UsingC"],
                        StringForIntegration[
                            Chop[
                                N[
                                    Round[
                                        temp (* to avoid Log[-1 + ImaginaryShiftVariable] -> I*Pi *)
                                        /.Log->l
                                        /.ImaginaryShiftVariable->0
                                        /.{l[a_] :> (-I*Pi + Log[-a]) /; And[Element[a, Reals], a < 0]}
                                        /.l->Log,
                                        10^-6
                                    ]
                                ],
                                10^-5
                            ]
                        ]<>";"
                    ,
                        ToStringInputForm[temp /. ImaginaryShiftVariable->0]
                    ]
                ,
                    If[OptionValue["UsingC"],
                        If[OptionValue["AnalyzeWorstPower"],
                            "("<>StringForIntegration[temp]<>")*("<>StringForIntegration[Inner[Power,vars[[1]],temp2,Times]]<>")*("<>StringForIntegration[Inner[Power,vars[[1]],-temp2,Times]]<>");"
                        ,
                            If[OptionValue["OptimizeIntegrationStrings"],
                                "$"<>StringForIntegrationOptimized[temp]<>";"
                            ,
                                StringForIntegration[temp]<>";"
                            ]
                        ]
                        ,
                        ToStringInputForm[temp /. ImaginaryShiftVariable ->0]
                    ]
                ],
                vars[[2]],
                temp1,
                temp3,
                If[vars[[2]]<=0, (*it can be -Infinity *)
                    If[OptionValue["UsingC"],StringForIntegration[Chop[N[Round[FF,10^-6]],10^-5]]<>";",ToStringInputForm[FF]]
                ,
                    If[OptionValue["UsingC"],StringForIntegration[FF]<>";",ToStringInputForm[temp]]
                ]
            }}
            (* num, value, number of vars, deg1, deg2,FF*)
        ]

    ];
    DistributeDefinitions[EvalFunction];

    WriteFunction[d_,value_,prefix_,sector_,optionsEval:OptionsPattern[FIESTA]]:=(
        If[Head[NumberOfVariables[value[[4]],value[[5]]]]===NumberOfVariables,NumberOfVariables[value[[4]],value[[5]]]=0;];
        If[Head[PSector[value[[4]],value[[5]],value[[1]]]]===PSector,PSector[value[[4]],value[[5]],value[[1]]]=0];
        NumberOfVariables[value[[4]],value[[5]]]=Max[NumberOfVariables[value[[4]],value[[5]]],value[[3]]];
        PSector[value[[4]],value[[5]],value[[1]]]++;
        QPutWrapper[
            prefix,
            d,
            ToString[value[[4]]]<>"-"<>ToString[value[[5]],InputForm]<>"-"<>ToString[value[[1]]]<>"-"<>ToString[PSector[value[[4]],value[[5]],value[[1]]]],
            StringReplace[RemoveFIESTAName[value[[2]]],ToString[OptionValue["XVar"]]->"x"]
        ];
        QPutWrapper[
            prefix,
            d,
            ToString[value[[4]]]<>"-"<>ToString[value[[5]],InputForm]<>"-"<>ToString[value[[1]]]<>"-"<>ToString[PSector[value[[4]],value[[5]],value[[1]]]]<>"-N",
            ToStringInputForm[value[[3]]]
        ]; (* number of variables for a particular term in a sector *)
        QPutWrapper[
            prefix,
            d,
            ToString[value[[4]]]<>"-"<>ToString[value[[5]],InputForm]<>"-"<>ToString[value[[1]]]<>"-"<>ToString[PSector[value[[4]],value[[5]],value[[1]]]]<>"F",
            StringReplace[RemoveFIESTAName[value[[6]]],ToString[OptionValue["XVar"]]->"x"]
        ];
    );

    InitializeQLink[{"in"},options];
    ClearDatabase[dbout, False, options];

    PerformStage[dbin,Terms,"in",TaskPrefix,True,options];

    If[OptionValue["DebugMemory"],RawPrintLn[MyMemoryInUse[options]]];


    ForEvaluation=Union[{##[[1]], ##[[2]]} & /@DefinedFor[PSector]];
    ForEvaluation=Select[ForEvaluation,(##[[1]]<=runorder)&];
    (*ForEvaluation=Sort[ForEvaluation, (#1[[1]] < #2[[1]]) &];*)

    QPutWrapper[TaskPrefix, "in","Exact",ToStringInputForm[False]];
    QPutWrapper[TaskPrefix, "in","SCounter",ToStringInputForm[SCounter]];
    QPutWrapper[TaskPrefix, "in","UsingC",ToStringInputForm[OptionValue["UsingC"]]];
    QPutWrapper[TaskPrefix, "in","ExternalExponent",ToStringInputForm[ExternalExponent]];
    QPutWrapper[TaskPrefix, "in","ForEvaluation",ToStringInputForm[ForEvaluation]];
    QPutWrapper[TaskPrefix, "in","runorder",ToStringInputForm[runorder]];
    QPutWrapper[TaskPrefix, "in","SHIFT",ToStringInputForm[SHIFT]];
    min=Min@@((##[[1]])&/@ForEvaluation);
    If[min=!=Infinity,
        ser=Series[EXTERNAL,{ep,0,runorder-SHIFT-min}];
        QPutWrapper[TaskPrefix, "in","EXTERNAL",ToStringInputForm[ser]];
        For[i=-SHIFT,i<=runorder-SHIFT-min,i++,
            serC = SeriesCoefficient[ser,i];
            If[
                And[
                    Head[serC] === SeriesCoefficient,
                    NumberQ[serC[[1]]],
                    IntegerQ[serC[[2]]],
                    serC[[2]] =!= 0
                ]
            ,
                serC = 0;
            ];
            QPutWrapper[TaskPrefix, "in","EXTERNAL-"<>ToString[i]<>"E",ToStringInputForm[serC]]; (*exact*)
            QPutWrapper[TaskPrefix, "in","EXTERNAL-"<>ToString[i],ToStringInputForm[CutExtraDigits[N[serC,OptionValue["Precision"]+32],options]]];
        ];
    ,
        QPutWrapper[TaskPrefix, "in","EXTERNAL",ToStringInputForm[0]];
    ];

    QPutWrapper[TaskPrefix, "in","ForEvaluationString",StringJoin@@((ToString[##[[1]]]<>"-"<>ToString[##[[2]],InputForm]<>"|")&/@ForEvaluation)];
    For[EvC=1,EvC<=Length[ForEvaluation],EvC++,
        CurrentOrder=ForEvaluation[[EvC]][[1]];
        CurrentVarDegree=ForEvaluation[[EvC]][[2]];
        terms=0;
        terms0=0;
        (If[Head[PSector[CurrentOrder,CurrentVarDegree,##]]===PSector,PSector[CurrentOrder,CurrentVarDegree,##]=0])&/@Range[SCounter];
        For[j=1,j<=SCounter,j++,
            QPutWrapper[TaskPrefix, "in",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-"<>ToString[j],ToStringInputForm[PSector[CurrentOrder,CurrentVarDegree,j]]];
            terms+=PSector[CurrentOrder,CurrentVarDegree,j];
            If[PSector[CurrentOrder,CurrentVarDegree,j]>0,terms0++];
        ];
        QPutWrapper[TaskPrefix, "in",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-T",ToStringInputForm[terms]];
        QPutWrapper[TaskPrefix, "in",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-T0",ToStringInputForm[terms0]];
        QPutWrapper[TaskPrefix, "in",ToString[CurrentOrder]<>"-"<>ToString[CurrentVarDegree,InputForm]<>"-N",ToStringInputForm[NumberOfVariables[CurrentOrder,CurrentVarDegree]]];
    ];

    TaskPrefixes=ToExpression[QSafeGetWrapper["0", "in","","{}"]];
    QPutWrapper["0", "in","",ToString[Append[TaskPrefixes,TaskPrefix]]];

    ClearDatabase[dbin, False, options];
    QClose[ActualDataPath[options]<>"in"];

    (* all databases are closed *)
    ClearAll[PSector, NumberOfVariables];

    If[OptionValue["DebugMemory"],RawPrintLn[MyMemoryInUse[options]]];
];

IntegrateHere::usage = "IntegrateHere[expression, tryexact, options] is a wrapper for Mathematica integration by DoIntegrate working with error messages and such";
IntegrateHere[expression_,tryexact_,options:OptionsPattern[FIESTA]]:=Module[{ff,result,result2,Localresult3,Good},
    If[expression=={},Return[{0,0,True}]];
    ff = expression;
    ff=DeleteCases[ff,{aa_,{}}];
    Localresult3 = (
        result2=Reap[Block[{$MessagePrePrint = Sow,$Messages = {}},
            Quiet[
                result=DoIntegrate[##,tryexact,options];
            ,
                {Power::infy, Power::indet, Infinity::indet,General::stop}
            ];
        ]][[2]];
        If[result[[1]],
            Localresult3={result[[2]],0,0}
        ,
            result=result[[2]];
            result2=DeleteCases[##, HoldForm[$MessageList]] & /@result2;
            result2=DeleteCases[##, HoldForm[OptionValue["XVar"]]] & /@result2;
            result2=DeleteCases[##, HoldForm[OptionValue["XVar"][_]]] & /@result2;
            result2=DeleteCases[result2,{}];
            Good=False;
            If[And[Length[$MessageList]>=1,Last[$MessageList]===HoldForm[NIntegrate::"maxp"],Length[result2]===1,
                ReleaseHold[result2[[1]][[2]]]===result],
                Good=True;
                Localresult3=Append[ReleaseHold/@Drop[result2[[1]],1],1];
            ];
            If[Length[result2]===0,
                Good=True;
                Localresult3={result,0,1};
            ];
            If[Not[Good],
                RawPrintLn[PMForm[result]];
                RawPrintLn["Something went wrong"];
                RawPrintLn[$MessageList];
                RawPrintLn[result2];
                Abort[];
            ];
        ];
        Localresult3
    )&/@ff;
    Plus@@Localresult3
]

MyDelta::usage = "MyDelta[xx,yy] is a table of length yy filled with zeros but 1 on index equal to xx";
MyDelta[xx_,yy_]:=Module[{iii},
    Table[If[iii===xx,1,0],{iii,yy}]
]

NegTerms::usage = "NegTerms[xx, options] is the list of terms of expansion of xx with negative coefficients";
NegTerms[0] := {}
NegTerms[xx_, options:OptionsPattern[FIESTA]] := Module[{temp},
    If[TrueQ[NumberQ[xx]],
        {}
    ,
        Select[List @@ Expand[xx], ((## /. {OptionValue["XVar"][ii_] -> 1}) < 0) &]
    ]
]

KillNegTerms::usage = "KillNegTerms[{Z,MM,UF}, options] is a preresolving function where Z has extra terms, UF = {U,F} and MM is a monomial";
KillNegTerms[ZZ_, options:OptionsPattern[FIESTA]]:=Module[{temp,vars,subsets,U,F,UF,Z,i,MM},
    {Z,MM,UF}=ZZ;
    temp=NegTerms[UF[[2]], options];
    If[temp==={},Return[{ZZ}]];
    RawPrintLn["Negative terms encountered: ",InputForm[temp]];
    vars=#[[1]]&/@Cases[Variables[temp],OptionValue["XVar"][_]];
    subsets=Subsets[vars,{2}];
    For[i=1,i<=Length[subsets],i++,
        If[And[Length[NegTerms[VarDiffReplace[UF[[2]],OptionValue["XVar"]/@subsets[[i]]], options]]<Length[temp],
               Length[NegTerms[VarDiffReplace[UF[[2]],OptionValue["XVar"]/@{subsets[[i]][[2]],subsets[[i]][[1]]}], options]]<Length[temp]]
                ,
            RawPrintLn["Decomposing into two sectors corresponding to ",subsets[[i]]];
            Z={#[[1]]/2,#[[2]]}&/@Z;
            Return[{VarDiffReplace[{Z,MM,UF},OptionValue["XVar"]/@subsets[[i]]],VarDiffReplace[{Z,MM,UF},OptionValue["XVar"]/@{subsets[[i]][[2]],subsets[[i]][[1]]}]}]
        ];
    ];
    Return[{ZZ}]
]

AdvancedKillNegTerms::usage = "AdvanceKillNegTerms[{Z,MM,UF}, options] is an advanced recurcive preresolving function where ZZ is the same as with KillNegTerms and level is the recursion level starting with 1";
AdvancedKillNegTerms[ZZ_,level_, options:OptionsPattern[FIESTA]]:=Module[{temp,vars,subsets,U,F,UF,Z,i,mins,rs,r1,r2,MM},
    {Z,MM,UF}=ZZ;
    temp=NegTerms[UF[[2]], options];
    If[temp==={},Return[{{ZZ},0}]];
    vars=#[[1]]&/@Cases[Variables[temp],OptionValue["XVar"][_]];
    subsets=Subsets[vars,{2}];
    mins=Table[Infinity,{Length[subsets]}];
    rs=Table[{},{Length[subsets]}];
    For[i=1,i<=Length[subsets],i++,
        If[And[Length[NegTerms[VarDiffReplace[UF[[2]],OptionValue["XVar"]/@subsets[[i]]], options]]<Length[temp],
               Length[NegTerms[VarDiffReplace[UF[[2]],OptionValue["XVar"]/@{subsets[[i]][[2]],subsets[[i]][[1]]}], options]]<Length[temp]]
                ,
            r1=AdvancedKillNegTerms[VarDiffReplace[{{#[[1]]/2,#[[2]]}&/@Z,MM,UF},OptionValue["XVar"]/@subsets[[i]]],level+1, options];
            r2=AdvancedKillNegTerms[VarDiffReplace[{{#[[1]]/2,#[[2]]}&/@Z,MM,UF},OptionValue["XVar"]/@{subsets[[i]][[2]],subsets[[i]][[1]]}],level+1, options];
            rs[[i]]=Join[r1[[1]],r2[[1]]];
            mins[[i]]=r1[[2]]+r2[[2]];
        ];
    ];

    i=Position[mins,Min[mins],{1}][[1]][[1]];
    If[mins[[i]]===Infinity,
        Return[{{ZZ},Length[temp]}],
        If[level===1,RawPrintLn["Total number of negative terms remaining in subexpressions: ",mins[[i]]]];
        If[level===1,RawPrintLn["Total number of subexpressions: ",Length[rs[[i]]]]];
        Return[{rs[[i]],mins[[i]]}];
    ];
]

AdvancedKillNegTerms2::usage = "AdvanceKillNegTerms2[{Z,MM,UF}, options] is an advanced recurcive preresolving function where ZZ is the same as with KillNegTerms and level is the recursion level starting with 1";
AdvancedKillNegTerms2[ZZ_,level_, options:OptionsPattern[FIESTA]]:=Module[{temp,vars,subsets,U,F,UF,Z,i,mins,rs,r1,r2,pairs,variants,MM,min},
    {Z,MM,UF}=ZZ;
    temp=NegTerms[UF[[2]], options];
    If[temp==={},Return[{{{ZZ},0}}]];
    vars=#[[1]]&/@Cases[Variables[temp],OptionValue["XVar"][_]];
    subsets=Subsets[vars,{2}];
    variants={};
    For[i=1,i<=Length[subsets],i++,
        If[And[Length[NegTerms[VarDiffReplace[UF[[2]],OptionValue["XVar"]/@subsets[[i]]], options]]<Length[temp],
               Length[NegTerms[VarDiffReplace[UF[[2]],OptionValue["XVar"]/@{subsets[[i]][[2]],subsets[[i]][[1]]}], options]]<Length[temp]]
                ,
            r1=AdvancedKillNegTerms2[VarDiffReplace[{{#[[1]]/2,#[[2]]}&/@Z,MM,UF},OptionValue["XVar"]/@subsets[[i]]],level+1, options];
            r2=AdvancedKillNegTerms2[VarDiffReplace[{{#[[1]]/2,#[[2]]}&/@Z,MM,UF},OptionValue["XVar"]/@{subsets[[i]][[2]],subsets[[i]][[1]]}],level+1, options];
            pairs=Tuples[{r1,r2}];
            variants=Join[variants,{Join[##[[1]][[1]],##[[2]][[1]]],##[[1]][[2]]+##[[2]][[2]]}&/@pairs];
        ];
    ];
    If[Length[variants]==0,Return[{{{ZZ},Length[temp]}}]];
    If[level===1,
        min=Min@@((##[[2]])&/@variants);
        i=Position[((##[[2]])&/@variants),min][[1]][[1]];
        RawPrintLn["Total number of negative terms remaining in subexpressions: ",variants[[i]][[2]]];
        RawPrintLn["Total number of subexpressions: ",Length[variants[[i]][[1]]]];
        Return[{variants[[i]]}];
    ,
        Return[variants];
    ];
]

AdvancedKillNegTerms3::usage = "AdvanceKillNegTerms3[{Z,MM,UF}, options] is an advanced recurcive preresolving function where ZZ is the same as with KillNegTerms and level is the recursion level starting with 1";
AdvancedKillNegTerms3[ZZ_,level_, options:OptionsPattern[FIESTA]]:=Module[{temp,vars,subsets,U,F,UF,Z,i,mins,rs,r1,r2,pairs,variants,MM,min},
    {Z,MM,UF}=ZZ;
    If[Length[UF]<2,Return[{{{ZZ},0}}]];
    temp=NegTerms[UF[[2]], options];
    If[temp==={},Return[{{{ZZ},0}}]];
    vars=#[[1]]&/@Cases[Variables[temp],OptionValue["XVar"][_]];
    subsets=Subsets[vars,{2}];
    variants={};
    For[i=1,i<=Length[subsets],i++,
        If[And[Length[NegTerms[AdvancedVarDiffReplace[UF[[2]],subsets[[i]], options], options]]<Length[temp],
               Length[NegTerms[AdvancedVarDiffReplace[UF[[2]],{subsets[[i]][[2]],subsets[[i]][[1]]}, options], options]]<Length[temp]]
                ,
            r1=AdvancedKillNegTerms3[AdvancedVarDiffReplace[{{#[[1]]/2,#[[2]]}&/@Z,MM,UF},subsets[[i]], options],level+1, options];
            r2=AdvancedKillNegTerms3[AdvancedVarDiffReplace[{{#[[1]]/2,#[[2]]}&/@Z,MM,UF},{subsets[[i]][[2]],subsets[[i]][[1]]}, options],level+1, options];
            pairs=Tuples[{r1,r2}];
            variants=Join[variants,{Join[##[[1]][[1]],##[[2]][[1]]],##[[1]][[2]]+##[[2]][[2]]}&/@pairs];
        ];
    ];
    If[Length[variants]==0,
        If[level == 1,
            RawPrintLn["Negative terms of total number ", Length[temp], " cannot be removed"];
        ];
        Return[{{{ZZ},Length[temp]}}]
    ];
    If[level===1,
        min=Min@@((##[[2]])&/@variants);
        i=Position[((##[[2]])&/@variants),min][[1]][[1]];
        RawPrintLn["Total number of negative terms remaining in subexpressions: ",variants[[i]][[2]]];
        RawPrintLn["Total number of subexpressions: ",Length[variants[[i]][[1]]]];
        Return[{variants[[i]]}];
    ,
        Return[variants];
    ];
]

DecomposeF::usage = "DecomposeF[{Z, MM, UF}, options] performs the MB decomposition of F by variable ExpandVar introducing integration variable MBVar";
DecomposeF[ZZ_, options:OptionsPattern[FIESTA]]:=Module[{temp,vars,subsets,U,F,UF,Z,i,shift,MM,temp1,temp2},
    {Z,MM,UF}=ZZ;
    temp=Expand[UF[[2]]];
    temp1=Coefficient[temp,OptionValue["ExpandVar"],1];
    temp1=Expand[temp1];
    temp2=Coefficient[temp,OptionValue["ExpandVar"],2];
    temp=If[Head[temp]===Plus,List@@temp,{temp}];
    temp1=If[Head[temp1]===Plus,List@@temp1,{temp1}];
    temp2=If[Head[temp2]===Plus,List@@temp2,{temp2}];
    If[And[Length[temp1]>0,Length[temp]>Length[temp1]],
        RawPrintLn["Expansion variable found"];
        shift=0;
        While[Expand[UF[[1]]/.(OptionValue["ExpandVar"]->0)]===0,
            shift+=1;
            UF[[1]]=Expand[UF[[1]]/OptionValue["ExpandVar"]];
        ];
        MM*=OptionValue["ExpandVar"]^shift;
        MM=MM*(OptionValue["ExpandVar"]^(MBVar));
        If[temp2==={0},
            UF=Join[{UF[[1]]/.(OptionValue["ExpandVar"]->0),Expand[UF[[2]]-(OptionValue["ExpandVar"]* Plus@@temp1)]//.(OptionValue["ExpandVar"]->0),Plus@@temp1},Drop[UF,2]];
            Z={##[[1]]*Gamma[-##[[2]][[2]]+MBVar]*Gamma[-MBVar]/Gamma[-##[[2]][[2]]], Join[{##[[2]][[1]], ##[[2]][[2]]-MBVar,MBVar},Drop[##[[2]],2]]} & /@Z;
        ,
            Print["Can't handle higher t powers with SDExpand, please try SDExpandAsy"];
            Abort[];
        ];
    ];
    {Z,MM,UF}
]

UseEpMonom::usage = "UseEpMonom[{Z,MM,UF}] takes exponent out of the free monomial and and it to functions if needed";
UseEpMonom[ZZ_]:=Module[{temp,vars,subsets,U,F,Z,MM,i,UF},
    {Z,MM,UF}=ZZ;
    MM=Expand[MM];
    If[And[Head[MM]===Power,Head[Expand[MM[[1]]]]===Plus],
        {{#[[1]],Prepend[#[[2]],MM[[2]]]}&/@Z,Prepend[UF,MM[[1]]]}
    ,
        {{#[[1]]*MM,#[[2]]}&/@Z,UF}
    ]
]

KillNegativeIndices::usage = "KillNegativeIndices[ZZ1, deltas1, degrees1, epdegrees1, options] get rid of negative non-ep part of indices (degrees1) by the diffirentiation of the expression.";
KillNegativeIndices[ZZ1_,deltas1_,degrees1_,epdegrees1_,options:OptionsPattern[FIESTA]]:=Module[{temp,i,j,var,ZZ,degrees,epdegrees,result,deltas,aaa1,aaa2},
    ZZ=ZZ1;
    deltas=deltas1;
    degrees=degrees1;
    epdegrees=epdegrees1;
    For[i=1,i<=Length[degrees],i++,var=OptionValue["XVar"][i];
        If[epdegrees[[i]]===0,
            While[degrees[[i]]<0,
                ZZ[[1]]=Flatten[
                    Append[
                        Table[
                            {-##[[1]]*D[ZZ[[2]][[j]],var]*##[[2]][[j]],##[[2]]-MyDelta[j,Length[ZZ[[2]]]]},
                            {j,1,Length[ZZ[[2]]]}
                        ],
                        {-D[##[[1]],var],##[[2]]}
                   ]&/@ZZ[[1]],1];

                degrees[[i]]=degrees[[i]]+1;
            ];
            If[degrees[[i]]===0,
                ZZ=Expand[ZZ/.var->0];
                deltas=DeleteCases[##,i]&/@deltas;
            ];
        ]
    ];
    ZZ[[1]]=Expand[ZZ[[1]]];
    ZZ[[1]]=DeleteCases[ZZ[[1]],{0,{aaa1_,aaa2_}}];
    Return[{ZZ,deltas,degrees,epdegrees}];
];

SDExpandAsy[{U_,F_,h_},degrees1_,order_,exo_,options:OptionsPattern[FIESTA]]:=Module[{i,shift,temp,j,ZZ,result={},l,n,deg,temp1,all,temp2,outside,ll,times,U1,F1,degrees2,min,var,pos,ii,found,jj,zeros,vars,a,b,temp0,temp3,ttt,go,result2,regions,command,TaskPrefix,ep,y},
    ep = OptionValue["EpVar"];
    (* Basic expansion function with ASY usage *)

    n=Length[degrees1];

    zeros = Flatten[Position[(## <= 0) & /@ degrees1, True]];
    vars = OptionValue["XVar"]/@ Complement[Range[n], zeros];

    {U1,F1}=Map[Rationalize[##,0]&, {U,F}, {0, Infinity}] /. (Rule[OptionValue["XVar"][##], 0] & /@ zeros);

    InitializeAllLinks[options];

    If[Length[zeros]>0, RawPrintLn["Active variables: ",vars]];

    If[OptionValue["AsyLP"],
        regions=Asy`PExpand[(U1+F1),vars,OptionValue["ExpandVar"],Asy`IntegralDim->Length[vars]];
    ,
        regions=Asy`PExpand[(U1*F1),vars,OptionValue["ExpandVar"],Asy`IntegralDim->Length[vars]-1];
    ];
    regions = {Rule @@@ Transpose[{vars, vars /. OptionValue["XVar"] -> y}],1,##}&/@regions;

    DeleteFile[Asy`InFile/.Options[Asy`QHull]];
    DeleteFile[Asy`OutFile/.Options[Asy`QHull]];

    regions=({##[[1]],##[[2]],##[[3]]-Min@@(##[[3]])})&/@regions;

    PutAtPositions[x_, y_, n_] := Normal[SparseArray[Apply[Rule, Transpose[{x, y}], {1}], {n}]];

    regions = {##[[1]],##[[2]],PutAtPositions[Complement[Range[n], zeros],##[[3]],n]}&/@regions;

    RawPrintLn["Regions: ",ToString[##[[3]]&/@regions,InputForm]];

    TaskPrefix = "0"; (* it is a counter *)

    If[Not[OptionValue["OnlyPrepareRegions"]],
        InitializeQLink[{"in"},options];
        QPutWrapper["0", "in","RequestedOrders",ToStringInputForm[{order,exo}]];
        QClose[ActualDataPath[options]<>"in"];
    ];

    If[Or@@(Not[IntegerQ[##]]&/@Flatten[##[[3]]&/@regions]),Print["Can't continue with fractional regions. Please consider changing the small variable"];Abort[]];

    (* below should use OnlyPrepare -> True *)

    For[j=1,j<=Length[regions],j++,
        RawPrintLn["Working in region ",ToString[regions[[j]][[3]],InputForm]];

        temp = Map[Rationalize[##,0]&, {U,F}, {0, Infinity}];

        (* created powers *)
        ZZ={{Gamma[Expand[Plus@@degrees1-h(OptionValue["d0"]/2-ep)]]/regions[[j]][[2]],{Plus@@degrees1-(h+1)(OptionValue["d0"]/2-ep),-(Plus@@degrees1-h(OptionValue["d0"]/2-ep))}}};

        (* degrees to start with *)
        degrees2=degrees1;

        ZZ={ZZ,temp};

        (*killing negative indices*)
        (* here we diffirentiate only for negative indices, they are indepentent from positive - with region replacements *)

        For[i=1,i<=Length[degrees2],i++,var=OptionValue["XVar"][i];
            While[TrueQ[degrees2[[i]]<0],
                ZZ[[1]]=Flatten[
                    Append[
                        Table[
                            {-##[[1]]*D[ZZ[[2]][[j]],var]*##[[2]][[j]],##[[2]]-MyDelta[j,Length[ZZ[[2]]]]},
                            {j,1,Length[ZZ[[2]]]}
                        ],
                        {-D[##[[1]],var],##[[2]]}
                   ]&/@ZZ[[1]],1];
                degrees2[[i]]=degrees2[[i]]+1;
            ];
            If[degrees2[[i]]===0,
                ZZ=Expand[ZZ/.var->0];
            ];
        ];

        temp=ZZ[[2]];
        ZZ=ZZ[[1]];

        (* region replacement for functions - PreResolve*)
        temp=temp //. regions[[j]][[1]];
        temp=ExpandAll[temp  //. {y[a_]:>OptionValue["XVar"][a]}];
        ZZ=ZZ //. regions[[j]][[1]];
        ZZ=ExpandAll[ZZ  //. {y[a_]:>OptionValue["XVar"][a]}];
        If[Or@@((##==0)&/@temp),Return[0]];

        (* region replacement for functions - exv multiplication*)
        temp=temp /. {OptionValue["XVar"][i_] :>  OptionValue["XVar"][i] OptionValue["ExpandVar"]^regions[[j]][[3]][[i]] } //. a__^b__ :> Factor[a]^Expand[b] // PowerExpand;
        ZZ=ZZ /. {OptionValue["XVar"][i_] :>  OptionValue["XVar"][i] OptionValue["ExpandVar"]^regions[[j]][[3]][[i]] } //. a__^b__ :> Factor[a]^Expand[b] // PowerExpand;
        ZZ=Append[##,0]&/@ZZ;
        (*factoring out power of exv from functions*)
        For[i=1,i<=Length[temp],i++,
            While[(temp[[i]]/.{OptionValue["ExpandVar"]->0})==0,
                temp[[i]]=Together[temp[[i]]/OptionValue["ExpandVar"]];
                ZZ={##[[1]],##[[2]],##[[3]]+##[[2]][[i]]}&/@ZZ;
            ]
        ];

        ZZ=ExpandAll[ZZ];
        ZZ=DeleteCases[ZZ,{0,_,_}];

        (*factoring out power of exv from ZZ*)
        ZZ=(temp2=##;While[(temp2[[1]]/.{OptionValue["ExpandVar"]->0})==0,temp2={Together[temp2[[1]]/OptionValue["ExpandVar"]],temp2[[2]],temp2[[3]]+1}];temp2)&/@ZZ;

        (* region replacement for degrees - exv multiplication*)
        (* OptionValue["XVar"][i] is in power degrees2[[i]]-1;
            however the Jacobian is the product of exv^regions[[j]][[3]][[i]]
            hence the power of exv appearing is Sum[regions[[j]][[3]][[i]]*(degrees2[[i]])];
        *)
        shift=Sum[regions[[j]][[3]][[i]]*(degrees2[[i]]), {i, 1, Length[degrees2]}];

        (*taking out min*)

        min=Min@@((##[[3]]/.{OptionValue["ExpandVar"]->0,ep->0,OptionValue["RegVar"]->0})&/@ZZ);
        pos=Position[((##[[3]]/.{OptionValue["ExpandVar"]->0,ep->0,OptionValue["RegVar"]->0})&/@ZZ),min][[1]][[1]];

        shift+=ZZ[[pos]][[3]];
        ZZ={OptionValue["ExpandVar"]^(##[[3]]-ZZ[[pos]][[3]])*##[[1]],##[[2]]}&/@ZZ;

        (*done cancelling*)

        n=Max@@((##[[1]])&/@Cases[Variables[Plus@@temp],OptionValue["XVar"][_]]);

        shift=Expand[shift];

        RawPrintLn["External degree: ",ToString[shift,InputForm]];
        If[(shift/.{ep->0,OptionValue["RegVar"]->0})<=exo,

            outside=(E^(h EulerGamma ep));
            i=0;
            While[(shift/.{ep->0,OptionValue["RegVar"]->0})+i<=exo,

                RawPrintLn["Evaluating at order ",ToString[(shift/.{ep->0,OptionValue["RegVar"]->0})+i,InputForm]];
                temp2={Expand[({##[[1]]/(Factorial[i]),##[[2]]}&/@ZZ)/.{OptionValue["ExpandVar"]->0}],temp/.{OptionValue["ExpandVar"]->0}};
                temp2[[1]]=DeleteCases[temp2[[1]],{0,_}];
                If[Length[temp2[[1]]]>0,
                    (* here we will work in this region, otherwise only continue to next order*)

                    If[OptionValue["OnlyPrepareRegions"],
                        temp3=temp2;
                        temp2={temp2[[1]],Table[temp2[[2]],{Length[temp2[[1]]]}]};
                        temp2=Transpose[temp2];
                        temp2={Append[##[[2]],##[[1]][[1]]],Append[##[[1]][[2]],1]}&/@temp2;
                        temp2=Transpose/@temp2;
                        temp2=Apply[Power,temp2,{2}];
                        temp2=Apply[Times,temp2,{1}];
                        temp2=Apply[Plus,temp2,{0}];
                        (*AppendTo[result,{regions[[j]][[3]],temp2*outside*exv^(shift+i)}];*)
                        RawPrintLn[ToString[Inner[(Power[#1,#2-1]/If[Not[TrueQ[#2<=0]],Gamma[#2],1])&,Array[OptionValue["XVar"],Length[degrees2]],degrees2,Times]*temp2*outside*OptionValue["ExpandVar"]^(shift+i),InputForm]];
                        temp2=temp3;
                    ];
                    deg=degrees2;
                    times=1;
                    For[l=1,l<=Length[deg],l++,
                        While[And@@(((##[[1]]/.{OptionValue["XVar"][l]->0})===0)&/@(temp2[[1]])),
                            temp2[[1]]={Together[##[[1]]/OptionValue["XVar"][l]]*(Gamma[deg[[l]]+1]/Gamma[deg[[l]]]),##[[2]]}&/@(temp2[[1]]);
                            deg[[l]]++;
                        ]
                    ];
                    (* here we start integrating, label used*)
                    Label[go];
                    n=Length[deg];
                    For[l=1,l<=n,l++,
                        If[deg[[l]]==0,Continue[]];
                        found=False;
                        (*searching for single function depending on first power of OptionValue["XVar"][l]*)
                        For[ii=1,ii<=Length[temp2[[2]]],ii++,
                            If[
                                And[
                                    Cases[Variables[{Delete[temp2[[2]],ii]}],OptionValue["XVar"][l]]==={},
                                    Coefficient[temp2[[2]][[ii]],OptionValue["XVar"][l],2]===0
                                ]
                            ,
                                found=True;
                                Break[]
                            ]
                        ];
                        If[And[OptionValue["AnalyticIntegration"],found],
                            RawPrintLn["Integrating by "<>ToString[OptionValue["XVar"]]<>"[",l,"] and changing variables"];
                            (*transforming z - third member is the degree of deg[[l]]*)
                            temp2[[1]] = {CoefficientRules[##[[1]], OptionValue["XVar"][l]], ##[[2]]} & /@ (temp2[[1]]);
                            temp2[[1]] = {##[[1]][[2]], ##[[2]], ##[[1]][[1]][[1]]} & /@
                            Flatten[Transpose[{##[[1]], Table[##[[2]], {Length[##[[1]]]}]}] & /@ (temp2[[1]]), 1];
                            (*integrating*)
                            AppendTo[temp2[[2]],Together[(temp2[[2]][[ii]]-(temp2[[2]][[ii]]/.{OptionValue["XVar"][l]->0}))/OptionValue["XVar"][l]]];
                            temp2[[2]][[ii]]=temp2[[2]][[ii]]/.{OptionValue["XVar"][l]->0};
                            temp2[[1]]={(1/Gamma[deg[[l]]])*##[[1]]*Gamma[Expand[-##[[2]][[ii]]-(deg[[l]]+##[[3]])]]*Gamma[deg[[l]]+##[[3]]]/Gamma[-##[[2]][[ii]]],
                                Append[ReplacePart[##[[2]],ii->##[[2]][[ii]]+(deg[[l]]+##[[3]])],-(deg[[l]]+##[[3]])]
                            }&/@temp2[[1]];
                            deg[[l]]=0;

                            pos = Complement[Range[n],Flatten[Position[deg,0]]];

                            If[Length[pos]==1,
                            (*Break[];*)
                                RawPrintLn["Delta function used"];
                                temp2[[1]]={##[[1]]/Gamma[deg[[First[pos]]]],##[[2]]}&/@(temp2[[1]]);
                                temp2=temp2/.{OptionValue["XVar"][First[pos]]->1};
                                deg={};
                            ];

                            For[l=1,l<=Length[temp2[[2]]],l++,(*factoring out*)
                               If[Length[Union[(##[[2]][[l]])&/@(temp2[[1]])]]===1,
                                    For[ll=1,ll<=Length[deg],ll++,
                                        While[And[(temp2[[2]][[l]] /. {OptionValue["XVar"][ll] -> 0}) == 0, temp2[[2]][[l]] =!= 0],
                                            temp2[[2]][[l]]=Together[temp2[[2]][[l]]/OptionValue["XVar"][ll]];
                                            temp2[[1]]={##[[1]]*(Gamma[Expand[deg[[ll]]+temp2[[1]][[1]][[2]][[l]]]]/Gamma[deg[[ll]]]),##[[2]]}&/@(temp2[[1]]);
                                            deg[[ll]]+=temp2[[1]][[1]][[2]][[l]];
                                        ]
                                    ]
                                ];
                            ];


                            (*factoring functions*)
                            For[ii = 1, ii <= Length[temp2[[2]]], ii++,
                                ttt = FactorList[temp2[[2]][[ii]]];
                                ttt = DeleteCases[ttt, {1, 1}];
                                If[Length[ttt] === 0, Continue[]];
                                temp2[[2]] = Join[temp2[[2]], (##[[1]]) & /@ Drop[ttt, 1]];
                                temp2[[2]][[ii]] = ttt[[1]][[1]];
                                temp2[[1]] = {##[[1]], Join[##[[2]], ((##[[2]]) & /@ Drop[ttt, 1])*##[[2]][[ii]]]} & /@ (temp2[[1]]);
                                temp2[[1]] = {##[[1]],ReplacePart[##[[2]], ii -> ttt[[1]][[2]]*##[[2]][[ii]]]} & /@ (temp2[[1]]);
                            ];

                            (*factoring free term*)
                            For[ii = 1, ii <= Length[temp2[[1]]], ii++,
                                ttt = FactorList[temp2[[1]][[ii]][[1]]];
                                ttt = DeleteCases[ttt, {1, 1}];
                                If[Length[ttt] === 0, Continue[]];
                                jj = 1;
                                While[jj <= Length[ttt],
                                    If[MemberQ[temp2[[2]], ttt[[jj]][[1]]],
                                        pos = Position[temp2[[2]], ttt[[jj]][[1]], {1}][[1]][[1]];
                                        temp2[[1]][[ii]][[2]][[pos]] += ttt[[jj]][[2]];
                                        ttt = Delete[ttt, jj];
                                    ,
                                        jj++
                                    ];
                                ];
                                temp2[[1]][[ii]][[1]] = Times @@ Power @@@ ttt;
                            ];
                            (*joining equal*)
                            For[ii = 1, ii <= Length[temp2[[2]]], ii++,
                                jj = ii + 1;
                                While[jj <= Length[temp2[[2]]],
                                    If[temp2[[2]][[ii]] === temp2[[2]][[jj]],
                                        temp2[[1]] = {##[[1]], Delete[ReplacePart[##[[2]], ii -> ##[[2]][[ii]] + ##[[2]][[jj]]], jj]} & /@ (temp2[[1]]);
                                        temp2[[2]] = Delete[temp2[[2]], jj];
                                    ,
                                        jj++;
                                    ];
                                ];
                            ];

                            l=1;
                            While[l<=Length[temp2[[2]]],  (*removing ones*)
                                If[temp2[[2]][[l]]===1,
                                    temp2[[2]]=Delete[temp2[[2]],l];
                                    temp2[[1]]={##[[1]],Delete[##[[2]],l]}&/@(temp2[[1]]);
                                ,
                                    l++;
                                ];
                            ];

                            If[Length[temp2[[2]]]===1,   (*has to be at least length two*)
                                temp2[[2]]=Prepend[temp2[[2]],1];
                                temp2[[1]]={##[[1]],Prepend[##[[2]],1]}&/@(temp2[[1]]);
                            ];

                            times++;
                            If[Length[deg]===0,Break[]];
                            Goto[go];
                        ];  (*if analytic integration and found *)
                    ];  (* the cycle *)
                    TaskPrefix=ToString[ToExpression[TaskPrefix]+1];
                    temp0=SDEvaluate2[TaskPrefix,temp2,h,deg,{order,None},order,outside,shift+i,Sequence @@ DeleteCases[{options}, Global`OnlyPrepare -> _],Global`OnlyPrepare->Automatic];
                    If[OptionValue["OnlyPrepareRegions"],
                        temp2 = temp0[[2]];
                        temp2 = {##[[1]], Table[##[[2]], {Length[##[[1]]]}]} & /@ temp2;
                        temp2 = Transpose[##] & /@ temp2;
                        temp2 = Map[{Append[##[[2]], ##[[1]][[1]]], Append[##[[1]][[2]], 1]} &, temp2, {2}];
                        temp2 = Map[Transpose, temp2, {2}];
                        temp2 = Apply[Power, temp2, {3}];
                        temp2 = Apply[Times, temp2, {2}];
                        temp2 = Apply[Plus, temp2, {1}];
                        temp2 = Apply[Plus, temp2, {0}];
                        temp2 = Inner[Power[#1,If[#2===0,0,#2-1]]&, Array[OptionValue["XVar"], Length[temp0[[1]]]], temp0[[1]], Times] *temp2* temp0[[6]]* Times @@ (KronekerDelta[##,1]&/@ Apply[Plus, Map[OptionValue["XVar"], temp0[[4]], {2}], {1}]) * OptionValue["ExpandVar"]^temp0[[7]];
                        AppendTo[result,{regions[[j]][[3]],temp2}];
                    ];
                ];(*end of big if (integration needed for this order)*)
                If[(shift/.{ep->0,OptionValue["RegVar"]->0})+i<exo,
                    (*ZZ transformation *)
                    ZZ=Flatten[
                        Append[
                            Table[
                                {##[[1]]*D[temp[[j]],OptionValue["ExpandVar"]]*##[[2]][[j]],##[[2]]-MyDelta[j,Length[temp]]},
                                {j,1,Length[temp]}
                            ],
                            {D[##[[1]],OptionValue["ExpandVar"]],##[[2]]}
                        ]&/@ZZ,
                        1
                    ];
                    ZZ=DeleteCases[ZZ,{0,_}];
                    If[ZZ=={},
                        RawPrintLn["Nothing from this region"];
                        Break[];
                    ]
                ];
                i++;
            ]  (*big while *)
        ,
            RawPrintLn["Nothing from this region"];
        ];
    ]; (* for over regions *)

    (* above should use OnlyPrepare->True *)

    If[OptionValue["OnlyPrepareRegions"],
        Return[Reap[Sow[##[[2]], G[##[[1]]]] & /@ result, _, {{##}[[1]][[1]], {##}[[2]]} &][[2]]]
    ];

    If[OptionValue["OnlyPrepare"],
        If[OptionValue["UsingC"],
            command=OptionValue["FIESTAPath"]<>"/bin/CIntegratePool --in "<>ActualDataPath[options]<>"in --threads "<>ToString[OptionValue["NumberOfLinks"]]<>CommandOptions[options];
            If[OptionValue["RegVar"] =!= None,
                command = command <>" --expandVar "<>ToString[OptionValue["RegVar"],InputForm];
            ];
            RawPrintLn[command];
        ];
        Return[];
    ];

    If[TaskPrefix == "0",
        result2 = 0
    ,
        SDIntegrate[options];
        result2=GenerateAnswer[options];
    ];

    If[OptionValue["RemoveDatabases"],
        DeleteFile[ActualDataPath[options]<>"in.kch"];
        If[TaskPrefix =!= "0",
            DeleteFile[ActualDataPath[options]<>"out.kch"];
        ];
    ];
    result2
]

SDEvaluate[{U_,F_,h_},degrees1_,order_,options:OptionsPattern[FIESTA]]:=Module[{temp},
    (* basic evaluation *)
    SDEvaluate1[{U,F,h},degrees1,{order,None},options]
]

SDEvaluateG[{graph_,external_},{U_,F_,h_},degrees1_,order_,options:OptionsPattern[FIESTA]]:=Module[{temp,CurrentGraph,ExternalVertices,InfinityVertex},
    (* evaluation with a graph *)
    InfinityVertex=Apply[Max,graph,{0,Infinity}]+1;
    CurrentGraph=MyGraph[Join[graph,{##,InfinityVertex}&/@external]];
    ExternalVertices=external;
    SDEvaluate1[{U,F,h},degrees1,{order,None},options,Strategy->"SS",Graph->{CurrentGraph, ExternalVertices, InfinityVertex}]
]

SDExpandG[{graph_,external_},{U_,F_,h_},degrees1_,order_,exo_,options:OptionsPattern[FIESTA]]:=Module[{temp,CurrentGraph,ExternalVertices,InfinityVertex},
    (* Basic expansion function with a graph asyst *)
    InfinityVertex=Apply[Max,graph,{0,Infinity}]+1;
    CurrentGraph=MyGraph[Join[graph,{##,InfinityVertex}&/@external]];
    ExternalVertices=external;
    SDEvaluate1[{U,F,h},degrees1,{order,exo},options,Strategy->"SS",Graph->{CurrentGraph, ExternalVertices, InfinityVertex}]
]

SDExpand[{U_,F_,h_},degrees1_,order_,exo_,options:OptionsPattern[FIESTA]]:=Module[{temp},
    (* Basic expansion function *)
    SDEvaluate1[{U,F,h},degrees1,{order,exo},options,SectorSymmetries->False]
]

SDAnalyze[{U_,F_,h_},degrees1_,dmin_,dmax_,options:OptionsPattern[FIESTA]]:=Module[{temp},
    (* Basic analyze function *)
    SDEvaluate1[{U,F,h},degrees1,{0,{dmin,dmax}},options]
]

SDEvaluate1::usage = "SDEvaluate1[{U, F, h}, degrees1, {order, expandOrder}, diff, options] is the first stage of SDEvaluate/SDExpand/SDAnalyze (and G versions) creating external gamma. SDAnalyze used expandorder to pass {dmin, dmax}";
SDEvaluate1[{U_,F_,h_},degrees1_,{order_, expandOrder_}, options:OptionsPattern[FIESTA]]:=Module[{temp,ZZ,degrees,a1,outside,result,runorder,ep},
    ep = OptionValue["EpVar"];
    degrees=degrees1/.{ep->0};
    a1=Plus@@degrees1;
    outside=(E^(h EulerGamma ep));
    outside=outside*Gamma[a1-h(OptionValue["d0"]/2-ep)];
    runorder=order;
    If[And[((a1-(h)(OptionValue["d0"]/2-ep))/.ep->0)<=0,IntegerQ[((a1-(h)(OptionValue["d0"]/2-ep))/.ep->0)]],
        runorder++
    ];
    ZZ={{1,{a1-(h+1)(OptionValue["d0"]/2-ep),-(a1-h(OptionValue["d0"]/2-ep))}}};
    temp = {ZZ,{U,F}};
    InitializeAllLinks[options];
    result=SDEvaluate2["1",temp,h,degrees1,{order,expandOrder},runorder,outside,0,options][[1]];
    result
]

SDEvaluate2::usage = "SDEvaluate2[TaskPrefix, ZZ0, h, degrees1, {order, expandOrder}, runorder2, outside, ExternalExponent, options] is the second stage of most functions; we deal with negative indices, create runorder and deltas. SDExpandAsy also joins here";
SDEvaluate2[TaskPrefix_, ZZ0_,h_,degrees1_,{order_,expandOrder_},runorder2_,outside_,ExternalExponent_,options:OptionsPattern[FIESTA]]:=Module[{ZZ,n,degrees,epdegrees,a1,deltas,runorder,j,i,CurrentGraph,ExternalVertices,InfinityVertex,ep},
    ep = OptionValue["EpVar"];
    runorder=runorder2;

    degrees=degrees1/.{ep->0};
    epdegrees=Expand[degrees1-(degrees1/.{ep->0})]/ep;
    a1=Plus@@degrees1;

    ZZ=ZZ0;

    n=Length[degrees1];
    If[OptionValue["Graph"] =!= None,
        {CurrentGraph, ExternalVertices, InfinityVertex} = OptionValue["Graph"];
        For[i = 1, i<= Length[degrees], ++i,
            If[degrees[[i]] === 0,
                CurrentGraph=ContractEdge[CurrentGraph,i];
                If[CurrentGraph===False,
                    RawPrintLn["WARNING: loop contracted, possible zero in the answer"];
                    Abort[];
                ];
            ];
        ];
    ];

    If[And[Not[OptionValue["NegativeTermsHandling"] === False],Length[degrees]>0],
        If[Length[NegTerms[ZZ[[2]], options]]==Length[Expand[ZZ[[2]]]],
            RawPrintLn["All terms in F are negative. Please consider to change the sign of all propagators"];
        ,
            If[Length[NegTerms[ZZ[[2]], options]]>Length[Expand[ZZ[[2]]]]/2,
                RawPrintLn["There are more negative terms in F than positive ones. Please consider to change the sign of all propagators"]
            ]
        ];
    ];

    For[j=1,j<=Length[degrees],j++,
        If[And[degrees[[j]]<=0,Not[epdegrees[[j]]==0]],runorder--]
    ];

    deltas={Range[n]};

    {ZZ,deltas,degrees,epdegrees}=KillNegativeIndices[ZZ,deltas,degrees,epdegrees, options];


    If[OptionValue["Graph"] =!= None,
        SDEvaluate3[TaskPrefix,ZZ,degrees,epdegrees,runorder,{order,expandOrder},outside,deltas,ExternalExponent,options,Graph->{CurrentGraph, ExternalVertices, InfinityVertex}]
    ,
        SDEvaluate3[TaskPrefix,ZZ,degrees,epdegrees,runorder,{order,expandOrder},outside,deltas,ExternalExponent,options]
    ]
]

SDEvaluateDirect[functions_, degrees_, order_,deltas_List:{}, options:OptionsPattern[FIESTA]] :=  Module[{temp, n, ZZ, result, intvars,i,VariablesHere},
    (* direct evaluation without automatic delta and gamma build *)
    VariablesHere = Sort[Union[Cases[functions, OptionValue["XVar"][_], {0, Infinity}]]];
    If[Length[VariablesHere]===0,
        n=0,
        n = Last[VariablesHere][[1]]
    ];
    temp = Transpose[{functions, degrees}];
    ZZ = {
        {{Times @@ ((##[[1]]) & /@ Select[temp, (##[[2]] === 1) &]), DeleteCases[degrees, 1]}},
        Rationalize[(##[[1]]) & /@ Select[temp, (##[[2]] =!= 1) &]]
    }/.var->OptionValue["XVar"];

    InitializeAllLinks[options];

    SDEvaluate3["1",ZZ,Table[1,{n}],Table[0,{n}],order,{order,None},1,deltas,0,options][[1]]
]

SDExpandDirect[functions_, degrees_, order_, exo_, deltas_List:{}, options:OptionsPattern[FIESTA]] :=  Module[{temp, VVariables, n, ZZ, result, intvars,i,VariablesHere},
    (* direct expansion without automatic delta and gamma build *)

    VariablesHere = Sort[Union[Cases[functions, OptionValue["XVar"][_], {0, Infinity}]]];
    If[Length[VariablesHere]===0,
        n=0,
        n = Last[VariablesHere][[1]]
    ];
    temp = Transpose[{functions, degrees}];
    ZZ = {
        {{Times @@ ((##[[1]]) & /@ Select[temp, (##[[2]] === 1) &]), DeleteCases[degrees, 1]}},
        Rationalize[(##[[1]]) & /@ Select[temp, (##[[2]] =!= 1) &]]/.var->OptionValue["XVar"]
    };

    InitializeAllLinks[options];

    SDEvaluate3["1",ZZ,Table[1,{n}],Table[0,{n}],order,{order,exo},1,deltas,0,options,SectorSymmetries->False][[1]]
]

SDEvaluate3::usage = "SDEvaluate3[TaskPrefix, ZZ1, degrees, epdegrees, runorder, {order, expandOrder}, outside, deltas, ExternalExponent, options] is the stage where the SDExpandDirect/SDEvaluateDirect functions also join the evaluation process. Here function simplifications performed dealing with negative indices and negative terms";
SDEvaluate3[TaskPrefix_, ZZ1_,degrees_,epdegrees_,runorder_,{order_,expandOrder_},outside_,deltas_,ExternalExponent_,options:OptionsPattern[FIESTA]]:=Module[{temp,i,j,var,ZZ,aaa,denom,ZZOld,intvars,n,ep},
    ep = OptionValue["EpVar"];
    If[And[(OptionValue["RegVar"] =!= None),expandOrder =!= None],
        Print["Can't continue together with SDExpand and RegVar"];
        Abort[];
    ];

    If[And[OptionValue["Strategy"] === "SS",OptionValue["Graph"] === None],
        RawPrintLn["Error: Strategy -> \"SS\" chosen, SDEvaluateG or SDExpandG modes accepted only"];
        Abort[];
    ];
    (* here ZZ is a pair; first argument is a list of pairs - monomial and list of powers, second - list of functions *)

    ZZ=ZZ1;
    If[Length[ZZ[[1]]]===0,Return[{0}]];
    n=Length[degrees];
    intvars=Table[(If[Or[Variables[degrees[[i]]]=!={},degrees[[i]]>0,Not[epdegrees[[i]]===0]],1,0]),{i,1,n}];
    ZZ[[1]]={
        #[[1]]*(Times@@Table[OptionValue["XVar"][i]^(If[TrueQ[degrees[[i]]>0],degrees[[i]]-1,0]),{i,1,n}])
            /(Times@@Table[If[TrueQ[MemberQ[Variables[degrees[[i]]],OptionValue["RegVar"]]],Gamma[degrees[[i]]+ep*epdegrees[[i]]],1],{i,Length[degrees]}])
        ,
        #[[2]]
    }&/@ZZ[[1]];
    ZZ={{ZZ[[1]],(Times@@Table[OptionValue["XVar"][i]^(If[intvars[[i]]===1,epdegrees[[i]]*ep+If[Not[TrueQ[degrees[[i]]>0]],degrees[[i]]-1,0],0]),{i,1,n}]),ZZ[[2]]}};

    (* we used indices, inserted a middle argument (weird monomial), put everything in a list *)
    If[Length[degrees]>0,
        If[OptionValue["NegativeTermsHandling"]==="Squares",
            While[True,
                ZZOld=ZZ;
                ZZ=Flatten[KillNegTerms[##, options]/@ZZ,1];
                If[ZZ===ZZOld,Break[]];
            ];
        ];
        If[OptionValue["NegativeTermsHandling"]==="AdvancedSquares",
            ZZ=Flatten[AdvancedKillNegTerms[##, 1, options][[1]]&/@ZZ,1]
        ];
        If[OptionValue["NegativeTermsHandling"]==="AdvancedSquares2",
            ZZ=Flatten[AdvancedKillNegTerms2[##, 1, options][[1]][[1]]&/@ZZ,1]
        ];
        If[Or[
            OptionValue["NegativeTermsHandling"]==="AdvancedSquares3",
            And[
                OptionValue["NegativeTermsHandling"]===Automatic,
                Not[OptionValue["ComplexMode"]]
                ]
            ]
        ,
            ZZ=Flatten[AdvancedKillNegTerms3[##, 1, options][[1]][[1]]&/@ZZ,1]
        ];
    ];
    If[And[expandOrder =!= None, Head[expandOrder] =!= List],
        ZZ=DecomposeF[##, options]&/@ZZ
        (* here MBVar can appear in case of SDExpand *)
    ];
    ZZ=UseEpMonom/@ZZ;

    (* middle argument is gone *)
    denom=Times@@Table[
        If[
            TrueQ[Or[
                And[degrees[[i]]<=0,epdegrees[[i]]==0],
                MemberQ[Variables[degrees[[i]]],MBVar],
                MemberQ[Variables[degrees[[i]]],OptionValue["RegVar"]]
                ]
            ],
            1,
            Gamma[degrees[[i]]+ep*epdegrees[[i]]]
        ],
        {i,Length[degrees]}];
    RawPrintLn["Integration has to be performed up to order ",runorder];

    If[OptionValue["OnlyPrepareRegions"],
        (* this is for asy, we fo not need expandOrder *)
        Return[{intvars,ZZ,runorder,deltas,runorder-order,outside/denom,ExternalExponent}];
    ,
        Return[SDPrepareAndIntegrate[TaskPrefix,intvars,ZZ,{runorder,expandOrder},deltas,runorder-order,outside/denom,ExternalExponent,options]];
    ];
]

IntegrationCommand[options:OptionsPattern[FIESTA]]:=Module[{result3},
    InitializeQLink[{"out"},options];
    result3=QSafeGetWrapper["0", "out","IntegrationCommand","Missing integration command"];
    QClose[ActualDataPath[options]<>"out"];
    result3
]

GenerateAnswer[options:OptionsPattern[FIESTA]]:=Module[{result3,temp, TaskPrefix, ep},
    ep = OptionValue["EpVar"];
    (* answer generation from the database *)
    InitializeQLink[{"out"},options];

    TaskPrefixes=ToExpression[QGetWrapper["0", "out",""]]; (*do not add my*)
    RequestedOrders=ToExpression[QSafeGetWrapper["0", "out","RequestedOrders",ToStringInputForm[RequestedOrders]]];

    result3=(TaskPrefix=ToString[##];
        temp=InternalGenerateAnswer[TaskPrefix, True, options];
        temp=Append[##,Power[OptionValue["ExpandVar"],ToExpression[QGetWrapper[TaskPrefix, "out","ExternalExponent"]]]]&/@temp;
        temp
    )&/@TaskPrefixes;

    result3=Apply[Times,result3,{2}]; (*multiplying coeff and ext var deg*)
    result3=Apply[Plus,result3,{1}];  (*summing all up*)

    QClose[ActualDataPath[options]<>"out"];

    If[OptionValue["ExpandResult"],
        result3=Collect[
            Normal[Series[Series[Plus @@ result3, {OptionValue["RegVar"], 0, 0}], {ep, 0, RequestedOrders[[1]]}]],
            {ep, OptionValue["ExpandVar"], Log[OptionValue["ExpandVar"]],OptionValue["RegVar"]},
            DoubleCutExtraDigits[KillSmall[PMSimplify[##,options],options],options]&
        ];
        If[Head[RequestedOrders]===List,
            temp=Exponent[result3,ep];
            If[And[temp=!=RequestedOrders[[1]],temp=!=-Infinity],
                Print["WARNING: requested ep order is ",RequestedOrders[[1]],", but result is up to ",temp];
            ];
        ];
        result3
    ,
        result3
    ]
]

InternalGenerateAnswer::usage = "InternalGenerateAnswer[TaskPrefix, Final, options] is called by GenerateAnswer in a cycle in case multiple prefixes are used";
InternalGenerateAnswer[TaskPrefix_, Final_, options:OptionsPattern[FIESTA]]:=Module[{temp,i,j,min,degs,result,SHIFT,EXTERNAL,ForEvaluation,order,lastexact,HasExact,runorder,ep,runorderRequested},
    ep = OptionValue["EpVar"];
    (*returns a list of pairs - second term is extvar degree*)

    HasExact=ToExpression[QGetWrapper[TaskPrefix, "out","Exact"]];
    If[HasExact,
        Return[{{ToExpression[QGetWrapper[TaskPrefix, "out","ExactValue"]]}}];
    ];

    ForEvaluation=ToExpression[QGetWrapper[TaskPrefix, "out","ForEvaluation"]];
    runorder=ToExpression[QSafeGetWrapper[TaskPrefix, "out","MaxEvaluatedOrder",ToStringInputForm[-Infinity]]];
    If[Final,
        runorderRequested=ToExpression[QSafeGetWrapper[TaskPrefix, "out","runorder",ToStringInputForm[-Infinity]]];
    ,
        runorderRequested = runorder;
    ];
    SHIFT=ToExpression[QGetWrapper[TaskPrefix, "out","SHIFT"]];
    EXTERNAL=ToExpression[QGetWrapper[TaskPrefix, "out","EXTERNAL"]];
    degs=Union[##[[2]]&/@ForEvaluation];
    result=Flatten[(
        min=Min@@((##[[1]])&/@Cases[ForEvaluation,{_,##}]);
        (*min<=runorder, that's for sure*)
            (*temp=Series[EXTERNAL,{ep,0,runorder-min}];   -> it was till MaxEvaluatedOrder - min, that can be maximum runorder-min*)
            (*runorder is maximally order + SHIFT*)
            (*series coefficients are from -SHIFT to runorder-SHIFT-min  =  order-min*)
            (*but it should be checked that the pole of EXTERNAL is -SHIFT *)

        lastexact=min;
        While[lastexact<=runorder,
            If[Not[ToExpression[QSafeGetWrapper[TaskPrefix, "out",ToString[lastexact]<>"-"<>ToString[##,InputForm]<>"-E",False]]],Break[]];
            lastexact++;
        ];
        lastexact--;
        temp=Table[{
                If[j+SHIFT<=lastexact,
                    FullSimplify[
                        Plus@@Table[
                            {##[[1]],##[[2]]*##[[2]],##[[3]],##[[4]]*##[[4]]}&[(ToExpression[QSafeGetWrapper[TaskPrefix, "out",ToString[i]<>"-"<>ToString[##,InputForm]<>"-R",ToStringInputForm[{0,0,0,0}]]]
                            * ToExpression[QGetWrapper[TaskPrefix, "out","EXTERNAL-"<>ToString[j-i]<>"E"]])]
                            ,{i,min,j+SHIFT}
                        ]   (* exact coefficients from j-min to -SHIFT  *)
                    ]
                ,
                    DoubleCutExtraDigits[
                        Plus@@Table[
                            {##[[1]],##[[2]]*##[[2]],##[[3]],##[[4]]*##[[4]]}&[
                                (ToExpression[QSafeGetWrapper[TaskPrefix, "out",ToString[i]<>"-"<>ToString[##,InputForm]<>"-R",ToStringInputForm[{0,0,0,0}]]]
                                * ToExpression[QGetWrapper[TaskPrefix, "out","EXTERNAL-"<>ToString[j-i]]])
                            ]
                            ,{i,min,j+SHIFT}
                        ]   (* coefficients from j-min to -SHIFT  *)
                        //{##[[1]],Sqrt[##[[2]]],##[[3]],Sqrt[##[[4]]]}&,
                        options
                    ]
                    //{##[[1]],##[[2]]*##[[2]],##[[3]],##[[4]]*##[[4]]}&
                ],
                ep^j*(If[OptionValue["RegVar"] =!= None, OptionValue["RegVar"], OptionValue["ExpandVar"]]^##[[1]])*
                (Log[If[OptionValue["RegVar"] =!= None, OptionValue["RegVar"], OptionValue["ExpandVar"]]]^##[[2]])
            }
        ,
            {j,min-SHIFT,runorderRequested-SHIFT}
        ];  (* coefficients from(back) -SHIFT or (order-min) to -SHIFT  *)
        temp={KillSmall[
                CutExtraDigits[##[[1]][[1]]+I*##[[1]][[3]],options]+
                CutExtraDigits[Power[##[[1]][[2]],1/2]+I*Power[##[[1]][[4]],1/2],options]*
                ToExpression[ToString[OptionValue["PMVar"]]<>ToString[PMCounter++]],
                options
            ],##[[2]]}&/@temp;
        temp
    )&/@degs,1];
    Return[result];
]

MyRank::usage = "MyRank[xx] is the matrix rank of differences of xx";
MyRank[xx_] := MatrixRank[(## - xx[[1]]) & /@ xx]

VNorm::usage = "VNorm[xx] is the special norm of a vector used for minimization";
VNorm[xx_] := Times @@ ((## + 1) & /@ xx);

PointOver::usage = "PointOver[xx, yy] checks if one point is strictly above another (comparisson of all coordinates)";
PointOver[xx_, yy_] := And @@ ((## >= 0) & /@ (xx - yy));

OnlyLowPoints::usage = "OnlyLowPoints[xx] leaves only points of a polytope that are not above another one";
OnlyLowPoints[xx_] := Module[{temp, i, j, Good},
    temp = {};
    For[i = 1, i <= Length[xx], i++,
        j = 1;
        Good = True;
        While[j <= Length[temp],
            If[PointOver[xx[[i]], temp[[j]]],
                Good = False;
                Break[]
            ];
            If[PointOver[temp[[j]], xx[[i]]],
                temp = Delete[temp, j];
                Continue[]
            ];
            j++
        ];
        If[Good,
            AppendTo[temp, xx[[i]]]
        ];
    ];
    temp
];

MyDegrees::usage = "MyDegrees[pol, var] makes a Newton polytope of a polynomial";
MyDegrees[pol_,var_] := Module[{rule, temp, degrees, i},
    rule = Rule[##, 1] & /@ var;
    temp = Expand[pol/.(0.->0)];
    If[Head[temp]===Plus,temp = List @@ temp,temp={temp}];
    temp = Union[(##/(## /. rule)) & /@ temp];
    degrees = Table[Exponent[##, var[[i]]], {i, Length[var]}] & /@ temp;
    degrees = OnlyLowPoints[degrees];
    degrees
]

SubSpace::usage = "SubSpace[xxx] finds a vector such that it's minimum scalar produce with the polytope points is a face but perhaps not a facet";
SubSpace[xxx_] := Module[{av, m, n, k, i, j, ind, facet, vector, temp, scalar, pr, newpoint, symplex, xx, sbs, km},
    If[Length[xxx[[1]]]===1,Return[{1}]];
    xx = Sort[xxx, (VNorm[#1] < VNorm[#2]) &];
    n = Length[xx[[1]]];
    k = n;
    m = MyRank[OnlyLowPoints[xx]];
    sbs = Subsets[Range[n], {2, Max[m + 1, n]}];
    sbs = Sort[sbs, (Plus @@ #1 > Plus @@ #2) &];
    For[i = 1, i <= Length[sbs], i++,
        av = Normal[SparseArray[Apply[Rule, (({##, 1}) & /@ sbs[[i]]), {1}],n]];
        If[MyRank[OnlyLowPoints[(##*av) & /@ xx]] >= Length[sbs[[i]]] - 1,
            Return[av];
        ];
    ];
    RawPrintLn[xxx];
    RawPrintLn["No subspace found"];
    Return[Table[0, {n}]];
];

Facet::usage = "Facet[xxx] finds a faces of the polytope visible from zero";
Facet[xxx_] := Module[{av, aa, bb, m, n, k, i, j, ind, pos, facet, vector, temp, scalar, pr, newpoint, symplex, xx, sbs, km, yy, avlabel},
    xx = xxx;
    n = Length[xx[[1]]];
    If[n===1,Return[{{1},{1}}]];
    av = SubSpace[xx];
    Label[avlabel];
    xx = Sort[xx, (VNorm[#1*av] < VNorm[#2*av]) &];
    yy = {};
    For[i = 1, i <= Length[xx], i++,
        If[And[Length[yy] > 0, MyRank[(##*av) & /@ yy] == MyRank[(##*av) & /@ Append[yy, xx[[i]]]]],
            Continue[]
        ];
        AppendTo[yy, xx[[i]]];
        yy = OnlyLowPoints[yy];
        If[MyRank[(##*av) & /@ yy] < Total[av],
            Continue[]
        ];
        aa = Table[0, {Length[yy]}];
        bb = Table[0, {Length[yy]}];
        If[Length[yy] == 1,
            Continue[]
        ];
        For[j = 1, j <= Length[yy], j++,
            facet = Delete[##, Position[av, 0]] & /@ yy;
            newpoint = facet[[j]];
            facet = Delete[facet, j];
            pr = MyNullSpace[(## - facet[[1]]) & /@ facet];
            If[Length[pr] === 1,
                vector = pr[[1]];
                If[vector.facet[[1]] < 0, vector = -vector];
                If[vector.newpoint >= vector.facet[[1]], aa[[j]] = 1];
                bb[[j]] = vector;
            ];
        ];
        pos = Flatten[Position[aa, 1]];
        If[Length[pos] == 0,
            pos = Flatten[Position[aa, 2]];
            If[Length[pos] == 0,
                RawPrintLn[xxx];
                Return[False];
            ];
        ];
        j=Last[pos];
        yy = Delete[yy, j];
    ];
    If[Length[yy] == 1, Return[False]];
    facet = Delete[##, Position[av, 0]] & /@ yy;
    pr = MyNullSpace[(## - facet[[1]]) & /@ facet];
    pr = Normal[SparseArray[Apply[Rule, Transpose[{Flatten[Position[av, 1]], ##}], {1}], n]] & /@ pr;
    pr = pr[[1]];
    pr = If[## < 0, 0, ##] & /@ pr;
    If[Total[pr] == 1,
        av[[Flatten[Position[pr, 1]]]] = 0;
        Goto[avlabel]
    ];
    pr
]

NormShift::usage = "NormShift[xx] shifts the polytope so that all minimums by coordinates are zero";
NormShift[xx_] := Module[{temp},
    temp = Apply[Min, Transpose[xx], {1}];
    (## - temp) & /@ xx
]

WMinVector::usage = "WMinVector[xx] is an internal function of the B strategy";
WMinVector[xx_] := Apply[Min, Transpose[xx], {1}];

WTilda::usage = "WTilda[xx] is an internal function of the B strategy";
WTilda[xx_] := Module[{temp = WMinVector[xx]}, (## - temp) & /@ xx];

WD::usage = "WD[ind, xx] is an internal function of the B strategy";
WD[ind_, xx_] := Min @@ (ind.## & /@ xx);

WFindA::usage = "WFindA[xx] is an internal function of the B strategy";
WBSequence[xx_] := Module[{n, result = {}, ind, delta, d, H, temp, indN},
    n = Length[xx[[1]]];
    ind = Table[1, {n}];
    delta = xx;
    result = {};
    While[True,
        AppendTo[result, {ind, delta}];
        If[delta == {}, Break[]];
        temp = WTilda[delta];
        d = WD[Table[1, {n}], temp];
        If[d == 0, Break[]];
        temp = Select[temp, ((ind.##) === d) &];
        H = If[##>0,1,0] & /@ Apply[Max, Transpose[temp], {1}];
        ind = Max[##, 0] & /@ (ind - H);
        temp = Union[(##/d) & /@ WTilda[delta], delta];
        temp = Select[temp, (H.## < 1) &];
        delta = Union[((##*ind)/(1 - ##.H)) & /@ temp];
    ];
    result
]

WFindA::usage = "WFindA[ind, delta] is an internal function of the B strategy";
WFindA[ind_, delta_] := Module[{temp, i, n, restart, temp2},
    n = Length[ind];
    temp = ind;
    Label[restart];
    For[i = 1, i <= n, i++,
        If[temp[[i]] == 1,
            temp2 = temp;
            temp2[[i]] = 0;
            If[And @@ ((temp2.## >= 1) & /@ delta),
                temp = temp2;
                Goto[restart];
            ]
        ]
    ];
    temp
]

WBSet::usage = "WBSet[xx] in terms of strategy B finds a vector for sector decomposition step";
WBSet[xx_] := Module[{n, temp, ind, delta},
    n = Length[xx[[1]]];
    temp = WBSequence[xx];
    {ind, delta} = Last[temp];
    If[delta === {}, Return[Table[1, {n}] - ind]];
    Return[Table[1, {n}] - ind + WFindA[ind,delta]]
]

MLN::usage = "MLN[xx] creates 3 numbers used for comparisson by the S strategy; they should decrease lezicographycally after each step for the strategy to work";
MLN[xx_] := Module[{n, vectors, i, j, ll, nn, l, v, m},
    n = Length[xx];
    If[n===1,Return[{0,0,0}]];
    vectors = DeleteCases[Flatten[Table[xx[[i]] - xx[[j]], {i, 1, n}, {j, 1, n}], 1], Evaluate[Table[0, {Length[xx[[1]]]}]]];
    ll = (Max @@ ## - Min @@ ##) & /@ vectors;
    nn = (Length[Cases[##, Max @@ ##]] + Length[Cases[##, Min @@ ##]]) & /@ vectors;
    l=Min @@ ll;
    m=Min @@ ((##[[2]])&/@Cases[Transpose[{ll,nn}],{l,a_}]);
    v=vectors[[Position[Transpose[{ll,nn}],{l,m}][[1]][[1]]]];
    {n, l, m, Position[v,Min@@v][[1]][[1]], Position[v,Max@@v][[1]][[1]]}
]

TriLower::usage = "TriLower[xx, yy] is a lexicographyc comparisson of sets of 3 numbers";
TriLower[xx_, yy_] :=
    If[xx[[1]] < yy[[1]], True,
        If[xx[[1]] > yy[[1]], False,
            If[xx[[2]] < yy[[2]], True,
                If[xx[[2]] > yy[[2]], False,
                    If[xx[[3]] < yy[[3]],
                        True,
                        False
                    ]
                ]
            ]
        ]
    ]

MakeOneStepFacet::usage = "MakeOneStepFacet[xxx, options] makes a step when a facet (=norm vector) was already chosen";
MakeOneStepFacet[xx_,facet_]:=MakeOneStepFacetExtra[xx,facet,Table[xx[[3]],{Length[facet]-Length[Position[facet,0]]}]]

MakeOneStepFacetExtra::usage = "MakeOneStepFacet[xxx, options] makes a step when a facet (=norm vector) was already chosen and has an extra parameter; in case of graph strategies that parameter contains graphs that are different for each path, otherwise there might be extra information that is copied for each path";
MakeOneStepFacetExtra[xx_,facet_,graphs_]:=Module[{res,n, active, Matrix},
    n=Length[xx[[1]][[1]]];
    active = Complement[Range[n], Flatten[Position[facet, 0]]];
    res = {};
    For[i = 1, i <= Length[active], i++,
        Matrix=ReplacePart[IdentityMatrix[n],facet,active[[i]]];
        AppendTo[res, {(Matrix.##) & /@ xx[[1]],Matrix.xx[[2]],graphs[[i]]}];
    ];
    res = {NormShift[#[[1]]],#[[2]],#[[3]]}&/@ res;
    res = {OnlyLowPoints[#[[1]]],#[[2]],#[[3]]}&/@ res;
    res
];

MakeOneStep::usage = "MakeOneStep[xxx, options] is the main function for all iterative sector decomposition strategies";
MakeOneStep[xxx_, options:OptionsPattern[FIESTA]] := Module[{xx,temp, active, n,m, sbs,i,j, res,Matrix,newfacet,restarted=False, m0,l0,n0,facet,vmax,vmin},
    xx={xxx[[1]][[1]],xxx[[2]],xxx[[3]]};
    If[OptionValue["Strategy"] === "SS",
        n=Length[xx[[1]][[1]]];
        temp=GraphStep[xxx[[3]], options];
        If[temp===$Failed,
            Print["ERROR: strategy failed"];
            Print[{{{{xx[[1]][[1]]}},xx[[2]],xx[[3]]}}];
            Print[Combinatorica`GetEdgeWeights[xxx[[3]]]];
            Abort[];
            Return[{{{{xx[[1]][[1]]}},xx[[2]],xx[[3]]}}];
        ];
        res=MakeOneStepFacetExtra[xx,Normal[SparseArray[Apply[Rule, (({##, 1}) & /@ temp[[1]]), {1}],n]],temp[[2]]];
        Return[({{##[[1]]},##[[2]],##[[3]]}&/@res)]
    ];
    If[OptionValue["Strategy"] === "B",
        res=MakeOneStepFacet[xx,WBSet[xx[[1]]]];
        Return[{{##[[1]]},##[[2]],##[[3]]}&/@res]
    ];
    If[Or[OptionValue["Strategy"] === "S",OptionValue["Strategy"] === "S2"],
        For[i=1,i<=Length[xxx[[1]]],i++,
            If[Length[xxx[[1,i]]] === 1, Continue[]]; (* this factor was already resolved *)
            n=Length[xxx[[1,i]][[1]]];
            facet = TimeConstrained[Facet[xxx[[1,i]]],1000,False];
            If[Not[facet===False],
                res=MakeOneStepFacet[##,facet]&/@({##,xxx[[2]],xxx[[3]]}&/@(xxx[[1]]));
                res=Transpose[res];
                res=Transpose/@res;
                res={##[[1]],##[[2]][[1]],##[[3]][[1]]}&/@res;
            ];
            If[Or[facet===False,Not[And@@((Length[##[[1,i]]]<Length[xxx[[1,i]]])&/@res)]],
                {m0,l0,n0,vmin,vmax}=MLN[xxx[[1,i]]];
                If[Or[facet===False,Not[And@@(TriLower[MLN[##],{m0,l0,n0}]&/@Flatten[First/@res,1])]],
                    facet=Normal[SparseArray[(##->1)&/@{vmin,vmax},n]];
                    res=MakeOneStepFacet[##,facet]&/@({##,xxx[[2]],xxx[[3]]}&/@(xxx[[1]]));
                    res=Transpose[res];
                    res=Transpose/@res;
                    res={##[[1]],##[[2]][[1]],##[[3]][[1]]}&/@res;
                    If[And@@(TriLower[MLN[##[[1,i]]],{m0,l0,n0}]&/@res),
                        Return[res];
                    ];
                    RawPrintLn["Failed to resolve"];
                    RawPrintLn[xx[[1]]];
                    Abort[];
                ];
            ];
            Return[res];
        ];
    ];
    If[OptionValue["Strategy"] === "A",
        n=Length[xx[[1]][[1]]];
        {m0,l0,n0,vmin,vmax}=MLN[xx[[1]]];
        facet=Normal[SparseArray[(##->1)&/@{vmin,vmax},n]];
        res=MakeOneStepFacet[xx,facet];
        If[And@@(TriLower[MLN[##[[1]]],{m0,l0,n0}]&/@res),
            Return[{{##[[1]]},##[[2]],##[[3]]}&/@res]
        ];
        RawPrintLn["Failed to resolve"];
        RawPrintLn[xx[[1]]];
        Abort[];
    ];
    If[OptionValue["Strategy"] === "X",
        n=Length[xx[[1]][[1]]];
        For[j=1,j<=Length[xxx[[1]]],j++,
            For[m=2,m<=n,m++,
                sbs=Subsets[Range[n],{m}];
                For[i=1,i<=Length[sbs],i++,
                    facet=Normal[SparseArray[(##->1)&/@sbs[[i]],n]];
                    If[And@@((Not[(facet.##)===0])&/@(xxx[[1]][[j]])),
                        res=MakeOneStepFacet[##,facet]&/@({##,xxx[[2]],xxx[[3]]}&/@(xxx[[1]]));
                        res=Transpose[res];
                        res=Transpose/@res;
                        res={##[[1]],##[[2]][[1]],##[[3]][[1]]}&/@res;
                        Return[res]
                    ];
                ]
            ];
        ];
    ];
]

MyNullSpace::usage = "MyNullSpace[xx] is a more convenient wrapper around Mathematica NullSpace";
MyNullSpace[xx_]:=Module[{temp},
    If[xx==={},Return[{{1}}]];
    temp=NullSpace[xx];
    If[Plus@@##>=0,##,-##]&/@temp
]

FindSD::usage = "FindSD[xxx, options] calls FindSDWithExtraInformation passing no extra information";
FindSD[xxx_, options:OptionsPattern[FIESTA]]:=FindSDWithExtraInformation[xxx,False,options];

FindSDWithExtraInformation::usage = "FindSDWithExtraInformation[xxx, Extra, options] performs sector decomposition while the meaning of Extra depends on the strategy choice";
FindSDWithExtraInformation[xxx_, Extra_, options:OptionsPattern[FIESTA]]:=Module[{temp, xx, yy}, yy = {};
    If[xxx==={{{}}},Return[{{}}]];
    xx = {{OnlyLowPoints/@xxx,IdentityMatrix[Length[xxx[[1]][[1]]]],Extra}};
    temp=xx;
    yy = Join[yy, Select[temp, (Times@@(Length/@(##[[1]])) === 1) &]];
    xx = Select[temp, Not[Times@@(Length/@(##[[1]])) === 1] &];
    While[True,
        temp = MakeOneStep[##, options] &/@ xx;
        temp = Flatten[temp, 1];
        yy = Join[yy, Select[temp, (Times@@(Length/@(##[[1]])) === 1) &]];
        xx = Select[temp, Not[Times@@(Length/@(##[[1]])) === 1] &];
        If[Length[xx] === 0,
            yy=#[[2]]&/@yy;
            Return[yy]
        ];
    ];
]

MyGraph::usage = "MyGraph[edges] creates a combinatorica graph object based on edges";
MyGraph[edges_] := Module[{temp},
    temp = Combinatorica`FromUnorderedPairs[edges];
    Combinatorica`Graph[Transpose[{Combinatorica`Edges[temp], (EdgeWeight -> ##) & /@
        Range[Length[Combinatorica`Edges[temp]]]}], {##} & /@ Combinatorica`Vertices[temp]
    ]
]

MyDeleteEdge::usage = "MyDeleteEdge[graph, weight] deletes an edge from a graph but leaves vertices";
MyDeleteEdge[graph_, weight_] := Combinatorica`Graph[DeleteCases[graph[[1]], {aa_, EdgeWeight -> weight}], graph[[2]]];

MyDeleteEdges::usage = "MyDeleteEdges[graph, weights] deletes edges from a graph but leaves vertices";
MyDeleteEdges[graph_, {}] := graph;
MyDeleteEdges[graph_, weights_] := Combinatorica`Graph[Intersection @@ (DeleteCases [graph[[1]], {aa_, EdgeWeight -> ##}] & /@ weights), graph[[2]]];

MyBiconnectedComponents::usage = "MyBiconnectedComponents[graph, options] finds biconnected componets taking care of the infinity vertex";
MyBiconnectedComponents[graph_, options:OptionsPattern[FIESTA]] := Combinatorica`InduceSubgraph[graph, DeleteCases[##,OptionValue["Graph"][[3]]]] & /@ Combinatorica`BiconnectedComponents[graph]

CleanGraph::usage = "CleanGraph[graph, options] cleans the graph of garbage --- self loops, alone vertices and such";
CleanGraph[graph_, options:OptionsPattern[FIESTA]] := Module[{g, temp},
    g = Combinatorica`RemoveSelfLoops[graph];
    temp = MyBiconnectedComponents[g, options];
    temp = Select[temp, (Combinatorica`M[##] <= 1) &];
    temp = Union @@ (Combinatorica`GetEdgeWeights /@ temp);
    MyDeleteEdges[g, temp]
]

ExternalNotConnected::usage = "ExternalNotConnected[graph] returns whether external vertices are connected after edges deletion or not";
ExternalNotConnected[graph_, external_] := Module[{temp},
    temp = Combinatorica`ConnectedComponents[graph];
    temp = Union[(##[[1]]) & /@ Position[temp, ##] & /@ external];
    Or[temp === {{}}, Length[temp] =!= 1]
]

GraphStep::usage = "GraphStep[graph, options] finds a sector decomposition step according to the current graph";
GraphStep[graph_, options:OptionsPattern[FIESTA]] := Module[{g, temp, InfinityVertex, ExternalVertices},
    InfinityVertex = OptionValue["Graph"][[3]];
    ExternalVertices = OptionValue["Graph"][[2]];
    g = CleanGraph[graph, options];
    If[Combinatorica`M[g] === Length[ExternalVertices], Return[$Failed]];
    If[Not[ExternalNotConnected[Combinatorica`DeleteVertex[graph,InfinityVertex],ExternalVertices]],
        temp = Sort[DeleteCases[##, InfinityVertex] & /@ Combinatorica`BiconnectedComponents[g], (Length[#1] > Length[#2]) &];
    ,
        temp = Sort[Combinatorica`BiconnectedComponents[Combinatorica`DeleteVertex[graph,InfinityVertex]], (Length[#1] > Length[#2]) &];
    ];
    temp = Combinatorica`GetEdgeWeights[Combinatorica`InduceSubgraph[g,temp[[1]]]];
    temp=Sort[temp];
    Return[{temp, (MyDeleteEdge[g, ##] & /@ temp)}];
]

ContractEdge::usage = "ContractEdge[graph, weight] contracts an edge (they are numbered by weights) which is needed when killing non-positive indices";
ContractEdge[graph_, weight_] := Module[{temp},
    temp = Rule @@ Cases [graph[[1]], {aa_, EdgeWeight -> weight}][[1]][[1]];
    If[temp[[1]] === temp[[2]], Return[False]];
    MyDeleteEdge[
        Combinatorica`Graph[ReplacePart[##, ##[[1]] /. temp, 1] & /@ graph[[1]],
        graph[[2]]],
        weight
    ]
]

MBshiftRules::usage = "MBshiftRules[dx] shfts gammas and polygammas how possible by multiplying by a factorial";
MBshiftRules[dx_] := Module[{i, m, n, a},
    {
        Gamma[m_.+a_.*dx] :> Gamma[1+a*dx]/Product[a*dx-i, {i,0,-m}] /; IntegerQ[m] && m <= 0,
        PolyGamma[n_, m_.+a_.*dx] :> (-a*dx)^(-n-1)*((-a*dx)^(n+1)*PolyGamma[n, 1+a*dx] + n!*Sum[(a*dx/(a*dx-i))^(n+1),{i,0,-m}]) /; IntegerQ[m] && m <= 0
    }
]

MBexpansionRules::series = "exhausted precomputed expansion of Gamma's (`1`)";
MBexpansionRules::usage = "MBexpansionRules[dx, order] provides rules for the expansion of gammas and polygammas up to order order by variable dx using precomputed expansion";
MBexpansionRules[dx_, order_] := Module[{i, a, n, m},
    {
        Gamma[m_+a_.*dx] :> If[order <= 20,
            Gamma[m]*Sum[(a*dx)^i/i!*MBexpGam[m, i], {i,0,order}]
        ,
            Message[MBexpansionRules::series, order];
            Normal[Series[Gamma[m+a*dx], {dx,0,order}]]
        ] /; !IntegerQ[m] || m > 0
    ,
        PolyGamma[n_, m_+a_.*dx] :> Sum[(a*dx)^i/i!*PolyGamma[n+i, m], {i,0,order}] /; !IntegerQ[m] || m > 0
    }
]

QHullRun::usage = "QHullRun[pts, dim, options] runs qhull providing path for temporary files and then cleans them";
QHullRun[pts_List, dim_Integer, options:OptionsPattern[FIESTA]]:=Module[{temp},
    temp=Asy`QHull[
        pts,
        dim,
        Asy`Executable->OptionValue["QHullPath"],
        Asy`InFile->ActualDataPath[options]<>"q"<>ToString[$KernelID]<>"i",
        Asy`OutFile->ActualDataPath[options]<>"q"<>ToString[$KernelID]<>"o",
        Mode->"Fv 2>/dev/null",
        Verbose->False
    ];
    DeleteFile[ActualDataPath[options]<>"q"<>ToString[$KernelID]<>"i"];
    DeleteFile[ActualDataPath[options]<>"q"<>ToString[$KernelID]<>"o"];
    temp
];

ZeroFaces::usage = "ZeroFaces[list, options] provides the list of facets containing the origin";
ZeroFaces[list_List, options:OptionsPattern[FIESTA]]:=Module[{n,pos,temp},
    If[Length[list]==0,Print["Zero list"];Abort[]];
    n=Length[list[[1]]];
    pos=Position[list,Table[0,{n}]];
    If[pos=={},Print["List does not contain zero"];Abort[]];
    pos=pos[[1]][[1]];
    temp=QHullRun[list,n,options];
    If[temp=={},Print["Something went wrong with QHull"];Print[list];Abort[]];
    temp=Select[temp,MemberQ[##,pos]&];
    If[temp=={},Return[{}]];
    temp=Map[list[[##]]&,temp,{2}];
    Return[temp];
]

NormVector::usage = "NormVector[list, direction] is a normal vector to a facet containing zero with a non-negative scalar product with direction";
NormVector[list_List,direction_List]:=Module[{n,pos,temp,det,vec,rank,i,j,temp2},
    If[Length[list]==0,Print["Empty facet"];Abort[]];
    n=Length[list[[1]]];
    pos=Position[list,Table[0,{n}]];
    If[pos=={},Print["Facet does not contain zero"];Abort[]];
    temp=DeleteCases[list,Table[0,{n}]];
    For[i=1,i<=Length[temp],i++,
        If[temp[[i]]==Table[0,{Length[temp[[i]]]}],Continue[]];
        pos=First[Complement[Range[Length[temp[[i]]]],Flatten[Position[temp[[i]],0]]]];
        For[j=i+1,j<=Length[temp],j++,
            If[temp[[j]][[pos]]!=0,temp[[j]]=(temp[[i]][[pos]])*temp[[j]]-(((temp[[j]][[pos]]))*temp[[i]])];
        ];
    ];
    temp=DeleteCases[temp,Table[0,{Length[temp[[1]]]}]];
    AppendTo[temp,Table[1,{n}]];
    det=Det[temp];
    vec=Table[0,{n}];
    vec[[n]]=det;
    vec=Inverse[temp].vec;
    det=GCD@@vec;
    vec=(##/det)&/@vec;
    If[vec.direction<0,vec=-vec];
    Return[vec];
]

DualCone::usage = "DualCone[list, options] is the dual cone to the original polytope";
DualCone[list_List, options:OptionsPattern[FIESTA]]:=Module[{n,pos,temp,c},
    temp=ZeroFaces[list,options];
    If[temp=={},Return[{}]];
    n=Length[list[[1]]];
    If[Length[temp]<n,Return[{}]]; (*zero is not a facet in fact*)
    (*If[Length[temp]<n,Print["The number of facets is too small"];Print[list];Abort[];];*)
    temp=NormVector[##,DeleteCases[DeleteCases[temp,##][[1]],Table[0,{n}]][[1]]]&/@temp;
    temp={Plus@@##,##}&/@temp;
    c=LCM@@((##[[1]])&/@temp);
    temp=((c/##[[1]])*(##[[2]]))&/@temp;
    Return[temp];
]

QHullWrapper::usage = "QHullWrapper[list, dim, options] is a call to qhull for a polytope of a smaller dimension";
QHullWrapper[list_List,dim_Integer, options:OptionsPattern[FIESTA]]:=Module[{n,temp,rank,i,ort,found,minor,positions,pos,j,newtemp},
    temp=(##-list[[1]])&/@list;
    temp=Drop[temp,1];
    n=Length[temp[[1]]];
    rank=dim;
    If[rank<n,
        temp=Transpose[temp];
        For[i=1,i<=n,i++,
            If[temp[[i]]==Table[0,{Length[temp[[i]]]}],Continue[]];
            pos=First[Complement[Range[Length[temp[[i]]]],Flatten[Position[temp[[i]],0]]]];
            For[j=i+1,j<=n,j++,
                If[temp[[j]][[pos]]!=0,temp[[j]]=temp[[j]]-(((temp[[j]][[pos]])/(temp[[i]][[pos]]))*temp[[i]])];
            ];
        ];
        temp=DeleteCases[temp,Table[0,{Length[temp[[1]]]}]];
        newtemp=Transpose[temp];
        newtemp=Prepend[newtemp,Table[0,{rank}]];
        newtemp=(LCM@@(Denominator/@Flatten[newtemp]))*newtemp;
        Return[QHullRun[newtemp,rank,options]];
    ,
        Return[QHullRun[list,n,options]];
    ]
]

Simplexify::usage = "Simplexify[list, dim, options] is a recursive simplexification of a cone";
Simplexify[list_List,dim_Integer, options:OptionsPattern[FIESTA]]:=Module[{temp},
    If[Length[list]<=dim,Print["Simplefify error"];Abort[]];
    If[Length[list]==dim+1,Return[{list}]];
    temp=QHullWrapper[list,dim,options];
    temp=Select[temp,Not[MemberQ[##,1]]&];
    temp=Map[list[[##]]&,temp,{2}];
    If[OptionValue["Strategy"] === "KU2",
        temp=(SimplexifyWrapper[##,dim-1,options])&/@temp;
    ,
        temp=(Simplexify[##,dim-1,options])&/@temp;
    ];
    temp=Flatten[temp,1];
    temp=Prepend[##,list[[1]]]&/@temp;
    Return[temp];
]

SimplexifyWrapper::usage = "SimplexifyWrapper[list, dim, options] is a wrapper over Simplexify that cycles the vertices in order to find the minimal simplexification";
SimplexifyWrapper[list_List,dim_Integer, options:OptionsPattern[FIESTA]]:=Module[{temp,min,i,tempres,res},
    min=Infinity;
    i=1;
    temp=list;
    While[And[i<=Length[temp],min>2],
        tempres=Simplexify[temp,dim,options];
        If[Length[tempres]<min,
            min=Length[tempres];
            res=tempres;
        ];
        i++;
        temp=RotateRight[temp];
    ];
    Return[res];
]

SimplexCones::usage = "SimplexCones[list, options] provides simplex cones corresponding to the original polytope";
SimplexCones[list_List, options:OptionsPattern[FIESTA]]:=Module[{n,pos,temp,facets,i,min,res,tempres},
    temp=DualCone[list, options];
    If[temp=={},Return[{}]];
    n=Length[temp[[1]]];
    If[Length[temp]<n,Print["Not enough edges in the dual cone"];Abort[]];
    If[MatrixRank[temp]<n,Return[{}]]; (*zero was not the vertex*)
    If[OptionValue["Strategy"] === "KU0",
        Return[Simplexify[temp,n-1,options]];
    ,
        Return[SimplexifyWrapper[temp,n-1,options]];
    ];
]

RawPrintLn["FIESTA loaded. Call ?FIESTA for information on the package."];

End[];
EndPackage[];
