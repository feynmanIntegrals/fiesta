source paths.inc 2>/dev/null
if ([ ! -d "examples/lin2/temp/" ]) then
    mkdir examples/lin2/temp;
fi;
if ([ "$#" -ne 0 ]) then
    cores="$1";links=$1;
else
    cores=0;links=1;
fi;
strategy="S2";
    { echo "Get[\"FIESTA5.m\"]; SetOptions[FIESTA, NumberOfSubkernels -> $cores, NumberOfLinks -> $links, Strategy -> \"$strategy\"]; MasterIndex = 1; "; cat examples/lin2/generate_db.m; } | ${MATH}

