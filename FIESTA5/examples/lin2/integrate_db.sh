    if ([ "$#" -ne 0 ]) then
        threads="$1";
    else
        threads=1;
    fi;
    if ([ "$#" -ne 0 ] && [ "$#" -ne 1 ]) then
        maxeval="$2";
    else
        maxeval=50000;
    fi;
    if ([ "$#" -ne 0 ] && [ "$#" -ne 1 ] && [ "$#" -ne 2 ]) then
        integrator="$3";
    else
        integrator="vegasCuba";
    fi;
    datapath="./examples/lin2/temp/db_in"
    ./bin/CIntegratePool --in $datapath --threads $threads -v --IntegratorOption maxeval:$maxeval --complex --printIntegrationCommand --Integrator ${integrator} --separateTerms --IntegratorOption epsrel:0.00000000001 --balanceSamplingPoints

