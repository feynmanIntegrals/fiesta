source paths.inc 2>/dev/null
if ([ ! -d "examples/pl2/temp/" ]) then
    mkdir examples/F1/temp;
fi;
if ([ "$#" -ne 1 ] && [ "$#" -ne 2 ]) then
    echo "Argument with a master integral number required";
else
    if ([ ! -f "examples/pl2/temp/db_$1_out.kch" ]) then
        echo "Output database does not exist";
    else
        { echo "MasterIndex = $1; Print[\"Checking result for master integral \" <> ToString[MasterIndex]];"; cat examples/pl2/check_result.m; } | ${MATH} -noprompt
    fi
fi;

