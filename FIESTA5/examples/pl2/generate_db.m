SetOptions[$Output,PageWidth->Infinity];

SetOptions[FIESTA,
    "DebugMemory" -> True,
    "DebugParallel" -> True,
    "SectorSymmetries" -> True,
    "ComplexMode" -> True,
    "ContourShiftShape" -> 4,
    "SeparateTerms" -> True,
    "OnlyPrepare" -> True
];

If[Not[TrueQ[ValueQ[MasterIndex]]], MasterIndex = 1];

SetOptions[FIESTA,
    DataPath -> "./examples/pl2/temp/db_" <> ToString[MasterIndex] <> "_"
];

Propagators = {
    -k1^2 + mm, -(k1 + p1 + p2)^2 + mm, -k2^2, -(k2 + p1 + p2)^2,
   -(k1 + p1)^2, -(k1 - k2)^2 + mm, -(k2 - p3)^2 + mm, -(k2 + p1)^2, -(k1 - p3)^2
};

Replacements = {p1^2 -> mm, p2^2 -> mm, p3^2 -> mm, p1 p2 -> 1/2 (-2 mm - S),
p1 p3 -> 1/2 (-2 mm - T), p2 p3 -> 1/2 (2 mm + S + T), mm -> 1, S -> -3/10, T -> 7/10};

masters = Get["./examples/pl2/pl2-mastersO.m"];

Print["Order is ", masters[[MasterIndex]][[2]]];

SDEvaluate[UF[{k1, k2}, Propagators, Replacements], masters[[MasterIndex]][[1,2]], masters[[MasterIndex]][[2]]];


