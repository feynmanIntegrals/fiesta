/** @file CIntegrate.cpp
 *
 *  This file is a part of the FIESTA package
 *
 *  The main file for the CIntegrate program.
 *  The routine Integrate() adds the rest and invokes the
 *  translator parseIntegrand(), file "Scanner.cpp".  After the incoming
 *  expression is translated into the tetrad array the routine Integrate()
 *  invokes the chosen integrator.
 */

#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include <fstream>

#include "constants.h"
#include "../sourcesCommon/handler.h"
#include "runtime.h"
#include "scanner.h"
#ifdef GPU
#include "kernel.h"
#endif
#include "common.h"
#include "integrators.h"

#ifdef GPU
#include <cuda_runtime.h>
#endif

#ifdef GPU
static int GPUCore = 0; /**<GPU mode. Used only in this file, can be set from input; If negative or not corresponding to a valid core, leads to a CPU-based evaluation*/
static int totalGPUCores; /**<GPU mode. The number of physicsl GPU cores detected at startup*/
#endif

static int outputPrecision = 15; /**<Precision with which the result is printed*/

/**
 * Prepares the Integration based on a string then runs it
 * @param s The integrand
 * @param onlyParse Whether to parse the expression only or to do the inegration
 */
void Integrate(const std::string& s, bool onlyParse) {
    Common::allTriads = 0;
    Common::usedTriads = 0;
    Common::memoryTetrads = 0;
#ifdef GPU
    if ((GPUCore < 0) || (GPUCore >= totalGPUCores)) {
        Common::GPUCores = 0;
        Common::CPUCores = 1;
    } else {
        Common::GPUCores = 1;
        Common::CPUCores = 0;

        if (cudaSetDevice(GPUCore) != cudaSuccess) {
            printf("Could not set Cuda device (Integrate, %d)!\n", GPUCore);
            abort();
        }
        SAFE_CALL((cudaDeviceSetCacheConfig(cudaFuncCachePreferL1)));

#ifdef __cplusplus
        cudaDeviceProp devProp;
#else
        struct cudaDeviceProp devProp;
#endif
        cudaGetDeviceProperties(&devProp, 0);
        Common::cudaSM = devProp.multiProcessorCount;
        size_t Mfree, Mtotal;
        cudaMemGetInfo(&Mfree, &Mtotal);
        Common::cudaMemoryLimit = Mfree;
        Common::GPUBatch = (cudaBlockSize * cudaBLocksPerSM * Common::cudaSM);
    }
#endif

    Common::batchSize = ((Common::GPUBatch * Common::GPUCores) + (Common::CPUBatch * Common::CPUCores));

    auto fscan = std::make_unique<Scanner>();

    fscan->ParseIntegrand(s);

    Integration::globalRuntime = std::make_unique<Runtime>(std::move(fscan));

    Integration::globalRuntime->BuildRuntime();

    if (onlyParse) {
#ifdef GPU
        Integration::globalRuntime->exitCuda();
#endif
        printf("{0,0,0,0}\n");
        return;
    }

    auto timeStart = std::chrono::steady_clock::now();

    IntegrationResult results = Integration::RunIntegrator(Integration::currentIntegrator);  // main Integration

    auto timeStop = std::chrono::steady_clock::now();

    char PrecisionFormat[128];
    sprintf(PrecisionFormat, "{%%.%df,%%.%df,%%.%df,%%.%df}%%s", outputPrecision, outputPrecision, outputPrecision, outputPrecision);

#ifndef COMPLEX
    printf(PrecisionFormat, results.resultReal, results.errorReal, 0.0, 0.0, "\n");
#else
    printf(PrecisionFormat, results.resultReal, results.errorReal, results.resultImaginary, results.errorImaginary, "\n");
#endif

    Common::timeStatistics[4] += std::chrono::duration_cast<std::chrono::microseconds>(timeStop - timeStart).count();
#ifdef GPU
    Integration::globalRuntime->exitCuda();
#endif
}

#ifdef GPU
#include <cuda_runtime_api.h>
/**
 * Finde the number of cuda cores on a GPU.
 * You must first call the cudaGetDeviceProperties() function, then pass
 * the devProp structure returned to this function
 * @param devProp cuda parameters
 * @return number of cuda cores
 */
int getSPcores(cudaDeviceProp devProp)
{
    int cores = 0;
    int mp = devProp.multiProcessorCount;
    switch (devProp.major){
        case 2: // Fermi
            if (devProp.minor == 1) cores = mp * 48;
            else cores = mp * 32;
            break;
        case 3: // Kepler
            cores = mp * 192;
            break;
        case 5: // Maxwell
            cores = mp * 128;
            break;
        case 6: // Pascal
            if ((devProp.minor == 1) || (devProp.minor == 2)) cores = mp * 128;
            else if (devProp.minor == 0) cores = mp * 64;
            else printf("Unknown device type\n");
            break;
        case 7: // Volta and Turing
            if ((devProp.minor == 0) || (devProp.minor == 5)) cores = mp * 64;
            else printf("Unknown device type\n");
            break;
        case 8: // Ampere
            if (devProp.minor == 0) cores = mp * 64;
            else if (devProp.minor == 6) cores = mp * 128;
            else printf("Unknown device type\n");
            break;
        default:
            printf("Unknown device type\n");
            break;
    }
    return cores;
}
#endif

/**
 * Main function of the process
 * Reads commands from stdin and writes answers to stdout
 * @param (argc > 1) number of arguments from the command line
 * @param argv the arguments. The single argument it can use is the number of the file replacing stdin
 * @return return code
 */
int main(int argc, char* argv[]) {

    AttachHandler();

    int argn = 0;
    std::fstream fs;
    if (argc  > 1) {
        fs.open(argv[1], std::fstream::in);
        ++argn;
    }
#ifdef WITH_DEBUG
    attachHandler();
#endif
    InitializePredefinedConstants();
    Integration::InitIntegrators();

#ifdef GPU
    cudaGetDeviceCount(&totalGPUCores);
#endif

    std::string buffer;
    while (1) {
        fflush(stdout);
        std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
        if (((argc > 1) ? fs : std::cin).eof()) {
            if (argn + 1 == argc) {
                break;
            }
            fs.close();
            ++argn;
            fs.open(argv[argn], std::fstream::in);
            continue;
        }
        if ((buffer == "exit") || (buffer == "Exit") || (buffer == "quit") || (buffer == "Quit")) {
            break;
        } else if (buffer == "//") {
            continue;
        } else if ((buffer == "ClearStatistics")) {
            Common::statistics[0] = 0;
            Common::statistics[1] = 0;
            Common::timeStatistics[0] = 0;
            Common::timeStatistics[1] = 0;
            Common::timeStatistics[2] = 0;
            Common::timeStatistics[3] = 0;
            Common::timeStatistics[4] = 0;
            Common::timeStatistics[5] = 0;
            Common::allTriads = 0;
            Common::usedTriads = 0;
            Common::memoryTetrads = 0;
            printf("Ok\n");
        } else if ((buffer == "Statistics")) {
            printf("Native points: %d, MPFR points: %d", Common::statistics[0], Common::statistics[1]);
            printf(", memory/used/all tetrads: %zu/%zu/%zu",
                    Common::memoryTetrads, Common::usedTriads, Common::allTriads);
#ifdef GPU
            printf(", cuda copy time: %f, cuda total time: %f",
                std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::microseconds(Common::timeStatistics[0])).count(),
                std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::microseconds(Common::timeStatistics[1])).count());
#else
            printf(", runlineNative time, %f",
                std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::microseconds(Common::timeStatistics[3])).count());
#endif
            printf(", MPFR time: %f",
                std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::microseconds(Common::timeStatistics[5])).count());

            printf(", runexpr time: %f",
                std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::microseconds(Common::timeStatistics[2])).count());

            printf(", call time: %f",
                std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::microseconds(Common::timeStatistics[4])).count());
            printf("\n");
            fflush(stdout);
        } else if (buffer == "Integrate") {
            std::getline(((argc > 1) ? fs : std::cin), buffer, '|');
            Integrate(buffer, false);
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
        } else if (buffer == "Parse") {
            std::getline(((argc > 1) ? fs : std::cin), buffer, '|');
            Integrate(buffer, true);
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
        } else if (buffer == "Precision") {
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
            if (!sscanf(buffer.c_str(), "%d", &outputPrecision)) {
                printf("Wrong input for Precision\n");
                return 1;
            }
            printf("Ok\n");
        } else if (buffer == "GPUCore") {
#ifdef GPU
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
            if (!sscanf(buffer.c_str(), "%d", &GPUCore)) {
                printf("Wrong input for GPUCore\n");
                return 1;
            }
#endif
            printf("Ok\n");
        } else if (buffer == "GPUMemoryPart") {
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
            if (!sscanf(buffer.c_str(), "%d", &Common::GPUMemoryPart)) {
                printf("Wrong input for GPUMemoryPart\n");
                return 1;
            }
            printf("Ok\n");
        }

        else if (buffer == "SetMath") {
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
            Common::mathBinary = std::string(buffer);
            printf("Ok\n");
        } else if (buffer == "SetIntegrator") {
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
            Integration::SetIntegrator(buffer);
            printf("Ok\n");
        } else if (buffer == "SetCurrentIntegratorParameter") {
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
            std::string buffer2;
            std::getline(((argc > 1) ? fs : std::cin), buffer2, '\n');
            Integration::SetParameter(Integration::currentIntegrator, buffer, buffer2);
            printf("Ok\n");
        } else if (buffer == "GetCurrentIntegratorParameters") {
            auto res = Integration::GetAllParameters(Integration::currentIntegrator);
            std::cout << res << std::endl;
        } else if (buffer == "SetMPPrecisionShift") {
            int s;
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
            if (!sscanf(buffer.c_str(), "%d", &s)) {
                printf("Wrong input for SetMPPrecisionShift\n");
            } else {
                if (s <= 0) {
                    fprintf(stderr, "SetMPPrecisionShift - Negative number.\n");
                } else {
                    Common::MPprecisionShift = s;
                    printf("Ok\n");
                }
            }
        } else if (buffer == "SetMPSmallX") {
            float ep;
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
            if (!sscanf(buffer.c_str(), "%f", &ep)) {
                printf("Wrong input for SetMPSmallX\n");
            } else {
                if (ep <= 0.0) {
                    fprintf(stderr, "SetMPSmallX - Ep <= 0!\n");
                } else {
                    if (ep > 1.0) ep = 1.0 / ep;
                    Common::MPsmallX = ep;
                    printf("Ok\n");
                }
            }
        } else if (buffer == "SetMPThreshold") {
            float ep;
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
            if (!sscanf(buffer.c_str(), "%f", &ep)) {
                printf("Wrong input for SetMPThreshold\n");
            } else {
                if (ep <= 0.0) {
                    printf("SetMPThreshold - Ep <= 0!\n");
                } else {
                    if (ep > 1.0) ep = 1.0 / ep;
                    Common::MPthreshold = ep;
                    printf("Ok\n");
                }
            }
        } else if (buffer == "SetMPMin") {
            float ep;
            std::getline(((argc > 1) ? fs : std::cin), buffer, '\n');
            if (!sscanf(buffer.c_str(), "%f", &ep)) {
                printf("Wrong input for SetMPMin\n");
                return 1;
            } else {
                if (ep > 1.0) ep = 1.0 / ep;
                Common::userSetMPMin = ep;
                printf("Ok\n");
            }
        } else if ((buffer == "help") || (buffer == "Help") || (buffer == "?")) {
            printf(
                "Possible commands: Quit, Integrate, Parse, SetMath, SetMPSmallX, SetMPPrecisionShift, SetMPThreshold, SetMPMin, SetIntegrator,\n"
                "GetCurrentIntegratorParameters, SetCurrentIntegratorParameter, TestF, Debug, MPFR, Native, Mixed, Statistics, GPUData,\n"
                "GPUCore, GPUMemoryPart, ClearStatistics, Precision, NoOptimization\n"
                "\n"
                "Some options require no parameter, some require a parameter to be entered after a new line, SetCurrentIntegratorParameter requires two\n"
                "\n"
                "exit, Exit, quit or Quit - exit immidiately\n"
                "\n"
                "help - this help\n"
                "\n"
                "MPFR, Native or Mixed (default) sets the evaluation mode\n"
                "\n"
                "Debug - turns all printing of all results\n"
                "TestF - tests the F function for being positive instead of integrating\n"
                "Statistics - prints timings\n"
                "ClearStatistics - clear stored timings\n"
                "NoOptimization - switch off triad optimization\n"
                "Precision - set precision for output printing\n"
                "\n"
                "SetIntegrator - set integrator equal to the next argument\n"
                "SetCurrentIntegratorParameter - set integrator parameter (two arguments)\n"
                "GetCurrentIntegratorParameters - print out actual parameters\n"
                "\n"
                "GPUData - priny information about existing GPU\n"
                "GPUForceCore - specifies the GPU core that will be used\n"
                "GPUMemoryPart - sets the GPU memory part that will be used by the process (default = 1 = full)\n"
                "\n"
                "The following options are related to multiple precision integration tuning\n"
                "SetMPPrecisionShift (integer argument)\n"
                "SetSmallX (float argument %%f)\n"
                "SetMPThreshold (float argument %%f)\n"
                "SetMPMin (float argument %%f)\n"
                "\n"
                "Integrate - the main integration command. The integrand format should be the following:\n"
                "<nvars>;\n"
                "<terms>;\n"
                "<terms> - lines with terms ending with ;\n"
                "<final expression>;\n"
                "\n"
                "<terms> can be 0, in this case the final expression comes right after the number of vars\n"
                "the terms or the final expression can refer preveous terms with f[n] (n starts from 1)\n"
                "integration variables are refered with x[n] (n starts from 1)\n"
                "power goes as p[_,_], logarithm as l[_], Pi as P, EulerGamma as G, PolyGamma is not shortened.\n"
                "\n"
                "Parse - same, but only check the input, do not integrate. Good answer is {0,0}\n"
                "\n"
                "SetMath - provides path to the Mathematica binary. If it is provided, CIntegrateMP can call Mathematica and evaluate some constants\n"
                "currently the only constant working this way is PolyGamma[_,_]\n"
                "some of the values of PolyGamma are stored in constants.h\n"
                "setting math is not recommended if you are using massive thread or mpi calculations (each slave will call math!)\n"
                "a better approach to be safe is to use OnlyPrepare in Mathematica, then run your job\n"
                "if some constants are missing you can add them to constants.h, recompile the binaries and launch the job again\n"
                "and since this version of FIESTA makes a complete parse check of all expressions before integrating, it won't take much time.\n"
                );
            fflush(stdout);
        } else if (buffer == "TestF") {
            Common::testF = true;
            printf("Ok\n");
            fflush(stdout);
        } else if (buffer == "Debug") {
            Common::debug = true;
            printf("Ok\n");
            fflush(stdout);
        } else if (buffer == "NoOptimization") {
            Common::withOptimization = false;
            printf("Ok\n");
            fflush(stdout);
        } else if (buffer == "MPFR") {
            Common::arithmetics = ArithmeticsMode::MP;
            printf("Ok\n");
            fflush(stdout);
        } else if (buffer == "Native") {
            Common::arithmetics = ArithmeticsMode::native;
            printf("Ok\n");
            fflush(stdout);
        } else if (buffer == "NoAVX"){
	    Common::avx_enabled = false;
            printf("Ok\n");
            fflush(stdout);
	} else if (buffer == "Mixed") {
            Common::arithmetics = ArithmeticsMode::mixed;
            printf("Ok\n");
            fflush(stdout);
        } else if (buffer == "GPUData") {
#ifdef GPU
            int deviceCountGPU = -1;
            cudaGetDeviceCount(&deviceCountGPU);
            fprintf(stderr, "Found %d devices\n", deviceCountGPU);
            // printing gpu options
            int device;
#ifdef __cplusplus
            cudaDeviceProp devProp;
#else
            struct cudaDeviceProp devProp;
#endif
            for (device = 0; device < deviceCountGPU; device++) {
                //получение параметров устройств
                cudaGetDeviceProperties(&devProp, device);
                fprintf(stderr, "Device number %d\n", device);
                fprintf(stderr, "Compute compatibility : %d.%d\n", devProp.major, devProp.minor);
                fprintf(stderr, "Device name : %s\n", devProp.name);
                fprintf(stderr, "Global memory size : %zu\n", devProp.totalGlobalMem);
                fprintf(stderr, "Memory bus width : %d\n", devProp.memoryBusWidth);
                fprintf(stderr, "Memory clock rate : %d\n", devProp.memoryClockRate);
                fprintf(stderr, "Number of multiprocessors : %d\n", devProp.multiProcessorCount);
                fprintf(stderr, "Number of cores : %d\n", getSPcores(devProp));
                fprintf(stderr, "Maximal number of threeds per multiprocessor : %d\n", devProp.maxThreadsPerMultiProcessor);
                fprintf(stderr, "Clock rate of CUDA cores : %d\n", devProp.clockRate);
                fprintf(stderr, "L2 cache size : %d\n", devProp.l2CacheSize);
                fprintf(stderr, "Shared memory per block : %zu\n", devProp.sharedMemPerBlock);
                fprintf(stderr, "Registers per block : %d\n", devProp.regsPerBlock);
                fprintf(stderr, "Warp size : %d\n", devProp.warpSize);
                fprintf(stderr, "Maximal number of threads per block : %d\n", devProp.maxThreadsPerBlock);
                fprintf(stderr, "Constant memory size: %zu\n", devProp.totalConstMem);
                fprintf(stderr, "Texture alignment : %zu\n", devProp.textureAlignment);
                fprintf(stderr, "Device overlap possibility : %d\n", devProp.deviceOverlap);
                fprintf(stderr, "Memory mapping possibility : %d\n", devProp.canMapHostMemory);
                fprintf(stderr, "Unified addressing possibility : %d\n", devProp.unifiedAddressing);
                fprintf(stderr, "Maximal thread dimension : %d %d %d\n", devProp.maxThreadsDim[0], devProp.maxThreadsDim[1], devProp.maxThreadsDim[2]);
                fprintf(stderr, "Maximal grid dimension : %d %d %d\n", devProp.maxGridSize[0], devProp.maxGridSize[1], devProp.maxGridSize[2]);
                fprintf(stderr, "Concurent kernels possibility : %d\n", devProp.concurrentKernels);
                fprintf(stderr, "PCI bus ID : %d\n", devProp.pciBusID);
                fprintf(stderr, "PCI device ID : %d\n", devProp.pciDeviceID);
            }
#endif
            printf("Ok\n");
            fflush(stdout);
        } else if (buffer != "") {
            printf("$Error - Undefined command: %s\n", buffer.c_str());
        }
    }

    return 0;
}
