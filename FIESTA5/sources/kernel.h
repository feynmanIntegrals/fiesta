/** @file kernel.h
 *
 *  This file is a part of the FIESTA package
 *
 *  This file is a header for GPU-mode functions
 */

#pragma once

#include "common.h"

constexpr int cudaBlockSize = 128; /**<The size of a block on a cuda multiprocessor*/
constexpr int cudaBLocksPerSM = 8; /**<The number of blocks requested a time on a cuda multiprocessor*/

/**
 * Safe wrapper for a Cuda call checking success
 */
#define SAFE_CALL(CallInstruction)                                                                  \
    {                                                                                               \
        cudaError_t cuerr = CallInstruction;                                                        \
        if (cuerr != cudaSuccess) {                                                                 \
            printf("CUDA error: %s at call \"" #CallInstruction "\"\n", cudaGetErrorString(cuerr)); \
            exit(0);                                                                                \
        }                                                                                           \
    }

/**
 * Safe wrapper for a Cuda call with synchronization checking success
 */
#define SAFE_KERNEL_CALL(KernelCallInstruction)                                                                                 \
    {                                                                                                                           \
        cudaError_t cuerr;                                                                                                      \
        cuerr = cudaDeviceSynchronize();                                                                                        \
        if (cuerr != cudaSuccess) {                                                                                             \
            printf("CUDA error in kernel execution: %s at kernel \"" #KernelCallInstruction "\"\n", cudaGetErrorString(cuerr)); \
            exit(0);                                                                                                            \
        }                                                                                                                       \
        KernelCallInstruction;                                                                                                  \
        cuerr = cudaGetLastError();                                                                                             \
        if (cuerr != cudaSuccess) {                                                                                             \
            printf("CUDA error in kernel launch: %s at kernel \"" #KernelCallInstruction "\"\n", cudaGetErrorString(cuerr));    \
            exit(0);                                                                                                            \
        }                                                                                                                       \
        cuerr = cudaDeviceSynchronize();                                                                                        \
        if (cuerr != cudaSuccess) {                                                                                             \
            printf("CUDA error in kernel execution: %s at kernel \"" #KernelCallInstruction "\"\n", cudaGetErrorString(cuerr)); \
            exit(0);                                                                                                            \
        }                                                                                                                       \
    }

/**
 * Cuda call with time measurement
 */
#define MEASURE_GPU_TIME(Function, number)                                              \
    {                                                                                   \
        cudaEventRecord(cudaStart, 0);                                                 \
        Function;                                                                       \
        cudaEventRecord(cudaStop, 0);                                                  \
        cudaEventSynchronize(cudaStop);                                                \
        float temp;                                                                     \
        cudaEventElapsedTime(&temp, cudaStart, cudaStop);                             \
        Common::timeStatistics[number] += round(temp * 1000); \
    }

/**
 * Evaluation of the integrand on a GPU
 * @param bunch the number of sampling points evaluated a time now
 * @param batchSize the maximal number of a batch, needed to find the shift for the imaginary part
 * @param length the length of the triad cycle
 * @param firstOperandGPU address in GPU memory of first operand addresses
 * @param secondOperandGPU address in GPU memory of second operand addresses
 * @param secondOperandIntegerGPU address in GPU memory of integer powers
 * @param resultGPU address in GPU memory of triad result addresses
 * @param operationGPU address in GPU memory of operations
 */
void RunlineNativeGPU(unsigned int bunch, unsigned int batchSize, unsigned int length, double** firstOperandGPU, double** secondOperandGPU, unsigned int* secondOperandIntegerGPU,
        double** resultGPU, Operation* operationGPU);
