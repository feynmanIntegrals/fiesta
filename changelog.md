# Changelog
All notable changes to this project will be documented in this file.
(mostly starting from changes brought by version 4.3)
This changelog uses following notations for sections:
- Features – for new features.
- Improved – for general improvements.
- Changed – for changes in existing functionality.
- Deprecated – for soon-to-be removed features.
- Removed – for removed features.
- Maintenance – for tidying code and internal minor changes.
- Fixed – for any bug fixes.
## FIESTA5.0 - 2021-10-25
### Features
- balancing mode for integration, so that integrals are first estimated and then a final run is done
- tt integrator now works in complex mode and can produce error estimates
- avx instrinstic instructions used for code speedup
- now working with icc
- now working on MAC OS X
### Fixed
- fixed sector splitting mode for simple integrals with no variables
- fixed rare crashes when the number of sampling points is not divisible by 4
- fixed a bug that prevented parallel sector decomposition
- fixed crashes in case of powers of complex numbers in functions
- fixed rare pissibility of completely incorrect answers due to an acient bug in mpfr setup
### Improved
- the running time was improved for MPFR usage
### Maintanance
- upgraded to c++17
- switched to original qmc (out pull request was merged)
- added some examples
## FIESTA4.5 - 2021-06-17
### Features
- --noOutputDatabase option for Pool not to save results in a database
### Maintanance
- The codebase was totally changed with the removal of legacy code and upgrade to the c++14 standart as much as possible
- Doxygen documentation added everywhere
### Fixed
- A rare case when the expression from mathematica was saved incorrectly was fixed
- An error fixed in lambda transformation search that resulted in lambda always smaller than needed
- MPI version was fixed to properly utilize CPU
- Fixing build for older gcc (but we need at least gcc 5)
## FIESTA4.4 - 2021-04-27
### Features
- DataPath option, which can be automatic resulting in not repeating database paths for different jobs
- Options for CIntegrateTool that can produce an integral file with additional integration options. Printing the corresponding command in case of crash
- --math= option in ./configure providing path to the math binary to be used for building or running. MathematicaBinary->Automatic uses this binary setting to pass to CIntegrate for evaluation of polygammas
- --cpp= option in ./configure providing the compiler to be used. FIESTA can be compiled with clang (but performance is worse)
- --enable-mimalloc option in ./configure changing memory allocator and improving performance
### Maintanance
- FIESTA no longer makes changes to SeriesCoefficient
- code passes checks with cppcheck, improving code style
- code passes checks with scan-build, fixing small bugs
- unifying some MPFR option names, cleaning some code
### Fixed
- Fixing strategy S and separating it from S2
- Fixing linking on opensuse, not linking statically with global libraries
- Fixing possible crash due to long complex numbers and errors in answer
- Fixing completely broken results in complex mode
- Fixing broken contour transformation search in some cases in complex mode
- Fixing irregular crash of make dep -j
## FIESTA4.3 - 2021-04-10
### Features
- new Tensor train integrator (currently in beta)
- QMC integrator plugged in
- MemoryDatabase option for speed
- ChunkSize option for better parallelization
- KUS strategy for large dimensions where KU fails
- SectorSplitting option for locating x[i]->1 problems after sector decomposition
- SectorSymmetries to identify equivalent sectors
- CIntegrateTool binary for conveniently getting integrals out of the database
### Improved
- radically improved performance in ComplexMode
- improved parallelization in Mathematica
### Changed
- the syntax was changed a lot; No more setting of global variables, everything is set via FIESTA options
- FIESTA is finally a Mathematica package
- code upgraded to c++, standart c++11
### Removed
- the UsingQLink = False option is removed in Mathematica
- BisectionVariables and related options, see SectorSplitting instead
### Maintanance
- cleaning up code style
- adding documentation to Mathematica code
- adding examples that can be run from command line
- adding a few tests
- making the code work at modern compilers and GPUs
## FIESTA4.2 - 2020-04-14
### Features
- Lee-Pomeransky option for [region search](https://arxiv.org/abs/1809.04325)
## FIESTA4.1 - 2016-05-18
### Fixed
- SmallX option (older versions could produce wrong results) and other small fixes
## FIESTA4.0 - 2015-11-14
- [gpu and other improvements](https://arxiv.org/abs/1511.03614)
## FIESTA3.0 - 2013-12-11
- [physical regions, databases, mpi](https://arxiv.org/abs/1312.3186)
## FIESTA2.0 - 2009-12-01
- [mpfr](https://arxiv.org/abs/0912.0158)
## FIESTA1.0 - 2008-07-25
- [basic version](https://arxiv.org/abs/0807.4129)
