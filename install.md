# Installation instructions #

The directory bin contains relative sim links to binaries placed in other directories.

The full package has the following binaries:

- CIntegrateMP: multiple-precision command-line integrator
- CIntegrateMPC: multiple-precision command-line integrator with complex numbers support
- CIntegrateMPG: multiple-precision command-line integrator with GPU suppurt
- CIntegrateMPCG: multiple-precision command-line integrator with complex numbers support and GPU support

- CIntegratePool: used command-line option, opens database, integrates (communicating with integrators), writes results in other database
- CIntegratePoolMPI: MPI version of CIntegratePool
- KLink: mathlink program called from Mathematica and used to create the database
- CIntegrateTool: helps to debug some cases when CIntegrateMP crashes

You will require some libraries to be installed to build FIESTA.

On Ubuntu it would be

- apt-get install git g++ cmake zlib1g-dev libmpfr-dev gfortran libgsl0-dev

You will need also

- apt-get install mpich

for the MPI version

- apt-get install doxygen

to build documentation

- apt-get install qhull-bin

for testmath

- apt-get install uuid-dev

to build the project and

- apt-get install nvidia-cuda-toolkit nvidia-cuda-dev

for the GPU version.

To build FIESTA first run
- ./configure

You will see possible options and their explanations. You can rerun ./configure with those options.
It is not a classical configure script but a bash script made specifically for FIESTA
The list of options used to run ./configure is saved in previous\_options file

Then run
- make dep
(as usual you can add -j to make it parallel).

This builds libraries shipped with FIESTA:
- cuba (default integrator)
- kyotocabinet (database engine)
- qmc (integrator, if turned on)
- tt (integrator, if turned on)
- mimalloc (memory allocator from Facebook, if turned on).

Please note that we use modified versions of Cuba and Kyotocabinet.
Other libraries are downloaded from github and bitbucket by request.

If the mpfr and gmp libraries are not on system paths, one needs to edit paths.inc and provide proper paths to those libraries.
Please do not forget to add those paths with -I and -L for include and link paths correspondingly.
Please keep in mind that changes to those files are overwritten on the ./configure run

Then run

- make"

in order to build everything but the MPI version of IntegratePool
The MPI version is build with

- make mpi

or together with other packages with

- make all

The GPU version is build with

- make gpu

There is no "make install" in package. Just provide the paths to files in the bin directory.

In order to build KLink you need to have Mathematica installed on your system. The paths should be detected automatically.
If you do not want to build KLink (the database will be prepared elsewhere) and wish to avoid compilation errors, use

- make noklink

You build results might be verified with

- make test

or

- make testall

(to test also MPI).

To go through more tests with Mathematica (not everything can be tested without math on your computer) run

- make testmath

You can create documentation with

- make doc

and open it with

- make showdoc

(or open the index.html in documentaion folder with your browser)
